<?php
/**
 * 
 * The proposed structure of the project.
 * 
 * @package apflow
 * @version 1.0
 * 
 */


	// Own error/exceptions handling.
	try{
		// Load apflow
		require APP_PATH.DS.'bootloader.php';

		// Imports AJAX into the project.
		if (load('request')->isAjax()) {			
			header('Content-Type: application/json; charset=utf-8');
			import(PROJECT_PATH.'/ajax/ajax.php');
		}
		
		// Imports web into the project.
		else {
			header('Content-Type: text/html; charset=utf-8');
			import(PROJECT_PATH.'/project.php');
		}
	} catch(\Throwable $t){
		// Displays the message of the exception being thrown.
		report($t);
	}