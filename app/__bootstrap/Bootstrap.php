<?php
/**
 *
 * This file contains instances of public 
 * methods of the Bootstrap, Import and Reportable classes.
 *
 * @package apflow
 * @subpackage Bootstrap
 * @version 1.0
 *
 */


    /**
     * 
     * Instance of the load method.
     * 
     * @param string $class - The name of the class used
     * @return object
     * 
     */
    function load(string $class){
        $boot = new apflow\Bootstrap;
        return $boot->load($class);
    }


    /**
     * 
     * Instance of the import method.
     * 
     * @param mixed $path       - Filename or path to file
     * @param bool $return	    - Is there anything to return for?
     * @param string $omission  - Files that will be omitted in the glob
     * 
     * @return mixed
     * 
     */
    function import($path, bool $return = false, string $omission = null){
        $file = new apflow\Components\Import;
        return $file->import($path, $return, $omission);
    }


    /**
     *
     * Filters the path to the file given as the method argument.
     *
     * @param string $path - Filename or path to file
     * @return string
     *
     */
    function filterPath(string $path) : string{
        $file = new apflow\Components\Import;
        return $file->filterPath($path);
    }


    /**
     * 
     * Instance of the Reportable object.
     * 
     * @param mixed $get        - Throwable object or Message string
     * @param string $file      - The name of the file in which the error occurred
     * @param string $line      - Line number in the file in which the error occurred
     * @param string $sql       - SQL Query
     * @param string $db_error  - Error DB
     * @param string $errid     - Own ERROR ID
     * 
     * @return object
     * 
     */
    function report($get = null, ?string $file = null, ?int $line = null, ?string $sql = null, ?string $db_error = null, ?string $errid = null){
        return new apflow\Components\Reportable($get, $file, $line, $sql, $db_error, $errid);
    }