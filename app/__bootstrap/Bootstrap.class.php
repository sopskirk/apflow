<?php
/**
 * 
 * This is the base class that 
 * connects all apflow components.
 * 
 * @package apflow
 * @subpackage Bootstrap
 * @version 1.0
 * 
 */


	namespace apflow;

	/** @package COMPONENTS */
	use apflow\Components\{
		API\API,
		Facades\File, Facades\Dir,
		Http\Cookie, Http\Session,
		Mailer\Mailer,
		Requests\Request,
		Security\CSP, Security\Security,
		Convert,
		IsType,
		DateTime,
		DotNotation,
		Import,
		Controller,
		Randval,
		Localization,
		SQL
	};

	/** @package TOOLS */
	use apflow\Tools\{
		Access,
		Detect,
		ArrayDB
	};

	use Exception;

	class Bootstrap extends Import{
		/**
		 *
		 * Sets the new object to the holder as needed.
		 *
		 * @param string $component - The name of the component used
		 * @return object
		 *
		 */
		public function load(string $component) : object{
			$component = strtolower($component);

			switch ($component) {
				/** @package COMPONENTS */

				case 'api'			: return new API;
				case 'file'			: return new File;
				case 'dir'			: return new Dir;
				case 'cookie'		: return new Cookie;
				case 'session'		: return new Session;
				case 'request'		: return new Request;
				case 'security'		: return new Security;
				case 'convert'		: return new Convert;
				case 'type'			: return new IsType;
				case 'date'			: return new DateTime;
				case 'dot'			: return new DotNotation;
				case 'controller'	: return new Controller;
				case 'randval'		: return new Randval;
				case 'localization'	: return new Localization;

				case 'mailer' :
					if (LOAD_COMPOSER == true) return new Mailer;
					else throw new Exception('To use the <b>Mailer</b> class, set the <q>LOAD_COMPOSER</q> constant to <b>true</b>.');
				break;

				case 'csp' : 
					if (LOAD_CSP == true && LOAD_COMPOSER == true) return new CSP;
					else throw new Exception('To use the <b>CSP</b> class, set the <q>LOAD_CSP</q> and <q>LOAD_COMPOSER</q> constant to <b>true</b>.');
				break;

				case 'sql' :
					if (RUN_SQL == true && LOAD_COMPOSER == true) return new SQL;
					else throw new Exception('To use the <b>SQL</b> class, set the <q>RUN_SQL</q> and <q>LOAD_COMPOSER</q> constant to <b>true</b>.');
				break;


				/** @package TOOLS */

				case 'access' 	: return new Access;
				case 'array-db'	: return new ArrayDB;
				case 'detect' 	: 
					$detect = new Detect;

					$detect->init();
					return $detect
				;

				// Error
				default : throw new Exception('The given parameter <b>'.$component.'</b> for the method <a href="https://github.com/sopskirk/apflow/Bootstrap#load">Bootstrap::load</a> has not been recognized!');
			}
		}
	}
