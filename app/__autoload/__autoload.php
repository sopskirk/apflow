<?php
/**
 *
 * This function is used to load components classes
 * located under a given namespace prefix.
 *
 * @package apflow
 * @subpackage Autoload
 * @version 1.0
 *
 */

	
	spl_autoload_register(function(string $class){
		// Load apflow
		$dirname = APP_PATH.DIRECTORY_SEPARATOR;
		
		$components = $dirname.'__components';
		$prefix 	= 'apflow\Components';

		// What prefixes should be recognized?
		$prefixes = [
			'apflow\Tools' => [$dirname.'__tools'],

			$prefix.'\API'			=> [$components.'/api'],
			$prefix.'\Facades'		=> [$components.'/facades'],
			$prefix.'\Http'			=> [$components.'/http'],
			$prefix.'\Mailer'		=> [$components.'/mailer'],
			$prefix.'\Requests'		=> [$components.'/requests'],
			$prefix.'\Security'		=> [$components.'/security'],
			$prefix => [$components],
			
			'apflow' => [$dirname.'__bootstrap']
		];

		// Go through the prefixes.
		foreach ($prefixes as $prefix => $dirs) {
			// Does the requested class match the namespace prefix?
			$prefix_len = strlen($prefix);
			if (substr($class, 0, $prefix_len) !== $prefix) continue;

			// Strip the prefix off the class.
			$class = substr($class, $prefix_len);

			// A partial filename.
			$part = str_replace([ '/', '\\' ], DIRECTORY_SEPARATOR, preg_replace('#/+#', '/', trim($class))).'.class.php';

			// Go through the directories to find classes.
			foreach ($dirs as $dir) {
				$dir = str_replace([ '/', '\\' ], DIRECTORY_SEPARATOR, preg_replace('#/+#', '/', trim($dir)));
				$file = $dir.DIRECTORY_SEPARATOR.$part;

				if (is_readable($file)) {
					require $file;
					return;
				}
			}
		}
	});
