<?php
/**
 * 
 * This class is used to construct 
 * an e-mail message.
 * 
 * @package apflow
 * @subpackage Mailer
 * @version 1.0
 * 
 */


    namespace apflow\Components\Mailer;
    use apflow\Bootstrap;
    use Exception;

    if (class_exists('PHPMailer\PHPMailer\PHPMailer')) {
        class MessageBuilder extends Bootstrap{
            /**
             *
             * Email subject and message.
             *
             * @var string
             *
             */
            public $subject;
            private $message;


            //------------------------------------------------------------------
            // BUILD METHOD
            //------------------------------------------------------------------


            /**
             *
             * Sets the content of the e-mail message.
             *
             * @param array $args       - Email message content
             * @param object $phpmailer - Instance of PHPMailer class
             * @param string $template  - Default Template
             * 
             * @return void
             *
             */
            public function buildEmail(array $args, object $phpmailer, string $template) : void{
                $template = $args['template'] ?? $template;
                
                if (!empty($template)) {
                    include APP_PATH.DS.'__templates'.DS.'PHPMailer'.DS.$template.'.php';

                    $exec = $template($args);
                    
                    $this->subject = $exec['subject'];
                    $this->message = $exec['message'];

                    $phpmailer->MsgHTML('<!doctype html><html><head><title>'.$this->subject.'</title></head><body style="width:100%;height:100%;margin:0;padding:0;font:normal 14px Helvetica;color:#555">'.$this->message.'</body></html>');
                }

                else throw new Exception('The e-mail <q>template</q> <span>must be</span> given!');
            }
        }
    }