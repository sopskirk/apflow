<?php
/**
 *
 * This class of management of sending e-mails. 
 * In addition, he also creates them.
 *
 * @package apflow
 * @subpackage Mailer
 * @version 1.0
 * 
 * @uses https://github.com/PHPMailer/PHPMailer
 *
 */


    namespace apflow\Components\Mailer;
    use PHPMailer\PHPMailer\PHPMailer;

    if (class_exists('PHPMailer\PHPMailer\PHPMailer') && LOAD_COMPOSER == true) {
		class Mailer{
			/**
			 *
			 * Stores instances of
			 * the PHPMailer class.
			 *
			 * @var object
			 *
			 */
			private $_phpmailer;
			
			/**
			 *
			 * Stores instances of
			 * the MessageMailer class.
			 *
			 * @var object
			 *
			 */
			private $_builder;
			
			/**
			 *
			 * Email address of the user.
			 *
			 * @var string
			 *
			 */
			private $_recipient;
		
			/**
			 * 
			 * Email address of the admin.
			 * 
			 * @var string
			 * 
			 */
			private $_sender;

			/**
			 * 
			 * Signature of the admin
			 * 
			 * @var string
			 * 
			 */
			private $_signature;

			/**
			 * 
			 * Default template
			 * 
			 * @var string
			 * 
			 */
			private $_template;
			

			//------------------------------------------------------------------
			// CONFIG METHOD
			//------------------------------------------------------------------


			/**
			 *
			 * Sets basic SMTP configuration for PHPMailer class.
			 *
			 * @return void
			 *
			 */
			private function _mailerConfig() : void{
				$config = include APP_PATH.DIRECTORY_SEPARATOR.'__config'.DIRECTORY_SEPARATOR.'config.mailer.php';
				
				$this->_phpmailer 	= new PHPMailer(true);
				$this->_builder 	= new MessageBuilder;

				$this->_phpmailer->CharSet = 'utf-8';
				$this->_phpmailer->isSMTP();
				$this->_phpmailer->SMTPAuth = true;
				$this->_phpmailer->SMTPSecure = 'tls';
				$this->_phpmailer->Port = $config['PORT'];
				$this->_phpmailer->Host = $config['HOST'];
				$this->_phpmailer->Username = $config['USER'];
				$this->_phpmailer->Password = $config['PASS'];
				
				$this->_sender 		= $config['SENDER'];
				$this->_recipient	= $config['RECIPIENT'];
				$this->_signature	= $config['SIGNATURE'];

				$this->_template	= $config['DEFAULT_TEMPLATE'];
			}
			

			//------------------------------------------------------------------
			// SEND METHOD
			//------------------------------------------------------------------


			/**
			 *
			 * Sets the recipient's address (user) and sender's
			 * address (admin).
			 *
			 * @return void
			 *
			 */
			private function _sendToUser() : void{
				if (!is_null($this->_recipient)) {
					$this->_phpmailer->Sender = $this->_sender;
					$this->_phpmailer->addReplyTo($this->_sender, $this->_signature);
					$this->_phpmailer->setFrom($this->_sender, $this->_signature);
					$this->_phpmailer->addAddress($this->_recipient);
					$this->_phpmailer->Subject = $this->_builder->subject;
				}
			}


			/**
			 *
			 * It sends a properly set message to the user, and if
			 * there is any problem sending, it will display an error.
			 *
			 * @param array $args - Email message content
			 * @return bool
			 * 
			 * @uses _sendToUser(), mailerConfig()
			 *
			 */
			public function send(array $args) : bool{
				$this->_mailerConfig();

				$this->_recipient 	= $args['recipient'] ?? $this->_recipient;
				$this->_sender 		= $args['sender'] ?? $this->_sender;
				$this->_signature 	= $args['signature'] ?? $this->_signature;

				$this->_builder->buildEmail($args, $this->_phpmailer, $this->_template);
				$this->_sendToUser();
				return $this->_phpmailer->send();
			}
		}
	}