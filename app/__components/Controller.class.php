<?php
/**
 *
 * This class manages the import of relevant files 
 * depending on the subpage, adds the allowed 
 * pages and changes the page title.
 *
 * @package apflow
 * @version 1.0
 *
 */


    namespace apflow\Components;
    use apflow\Bootstrap;
    use Exception;

    class Controller extends Bootstrap{
        /**
		 *
		 * Current active options (view + model).
		 *
		 * @var string
		 *
		 */
        private static $_options;
        private static $_view;

        /**
		 *
		 * Current active page.
		 *
		 * @var string
		 *
		 */
        private static $_page;

        /**
         * 
         * Was the view name taken 
         * directly from the URL?
         * 
         * @var bool
         * 
         */
        private $_fromURL = false;


        //------------------------------------------------------------------
        // IMPORT METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Attaches the page header to
         * the project.
         * 
         * @return void
         * 
         */
        public function header() : void{
            $this->import(LAYOUT_PATH.'/header.php');
        }

        
        /**
         * 
         * Attaches the page footer to 
         * the project.
         * 
         * @return void
         * 
         */
        public function footer() : void{
            $this->import(LAYOUT_PATH.'/footer.php');
        }


        /**
         * 
         * Attaches the view 404 to
         * the project.
         * 
         * @return void
         * 
         */
        public function view404() : void{
            $this->import(LAYOUT_PATH.'/404.php');
        }


        //------------------------------------------------------------------
        // CHECK METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Checks whether the given view 
         * has been found.
         * 
         * @return bool
         * 
         */
        public function viewFound() : bool{
            return http_response_code() == 200 ? true : false;
        }


        /**
         * 
         * Checks or displays the views 
         * group name.
         * 
         * @param string $group - Views Group Name
         * @return mixed
         * 
         */
        public function groupView(?string $group = null){
            if (empty(self::$_view)) $current = dirname(self::$_options[self::$_page]['view']);
            else $current = str_replace('views', '', pathinfo(dirname(self::$_view), PATHINFO_BASENAME));

            if (!is_null($group)) return $group === $current ? true : false;
            else return empty($current) ? 'global' : $current;
        }


        //------------------------------------------------------------------
        // LAZY LOADIND SQL METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Loads data from the database 
         * on demand.
         * 
         * @param mixed $methods    - Method names and their arguments
         * @param object $instance  - Class instance
         * 
         * @return void
         * 
         */
        protected function useLazyLoadingSQL($methods, object $instance) : void{
			if (is_string($methods)) $instance->$methods = $instance->$methods();
			else {
				foreach ($methods as $key => $method) {
					if (is_string($method)) $instance->$method = $instance->$method();
					else $instance->$key = $instance->$key($method);
				}
			}
		}


        //------------------------------------------------------------------
        // ADD METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Loads files from a given assets:
         * - views,
         * - models
         * 
         * @param string $asset - Asset MVC name
         * @param string $path  - Path to asset
         * @param bool $bool    - Import file?
         * 
         * @return mixed
         * 
         */
        private function _loadAssetMVC(string $asset, string $path, bool $import = true){
            $asset = strtolower($asset);

            $list   = $this->load('dir')->list($path);
            $files  = array_map(function($e){
                $e = strtolower($e);

                $basename = str_replace([ 'views', 'models' ], '', pathinfo(dirname($e), PATHINFO_BASENAME));
                $filename = pathinfo($e, PATHINFO_FILENAME);

                return !empty($basename) ? $basename.'/'.$filename : $filename;
            }, $list);

            $key = array_search($asset, $files);

            if ($key !== false) {
                $path = $list[$key];
                
                if ($import) {
                    if (file_exists($path)) {
                        $this->import($path);
                        return true;
                    }
                }
                
                else return $path;
            }

            return false;
        }


        /**
         * 
         * Init controller to the project.
         * 
         * @param array $pages - Subpages that will be used
         * @return void
         * 
         * @uses viewFound(), __traitInitMethod()
         * 
         */
        public function init(array $pages = null) : void{
            $is = false;
            $location = $this->load('request')->location();

            /* Call to array $pages. */
            if (is_array($pages)) {
                foreach ($pages as $page => $data) {
                    $page = is_int($page) ? $data : $page;
    
                    if ($location->is($page, $data['subpages'] ?? false) == true) {
                        if (!is_array($data)) self::$_view = $data;
                        else self::$_options = $pages;
    
                        self::$_page = $page;
                        http_response_code(200);
                        
                        $is = true;
                        break;
                    }
                }
            }

            /* Call to null $pages. */
            if (is_null($pages) || $is == false) {
                // Check exists view
                $view = $this->_loadAssetMVC($location->pathname, MVC_VIEWS_PATH, false);
                $this->_fromURL = true;

                if ($view !== false) {
                    self::$_view = $view;
                    
                    http_response_code(200);
                    $is = true;
                }
            }

            /* Error: Diffrent type of argument. */
			elseif (!is_array($pages) && !is_null($pages)) throw new Exception('Incompatible type <b>'.gettype($pages).'</b> of the <a href="https://github.com/sopskirk/apflow/Controller#init">Controller::init</a> method argument!');

            // When is "page not found".
            if ($is === false) http_response_code(404);
        }


        //------------------------------------------------------------------
        // TITLE METHOD
        //------------------------------------------------------------------

        /**
         * 
         * Sets the page title according to the page.
         * 
         * @param string $default - Default page title
         * @return string
         * 
         * @uses viewFound()
         * 
         */
        public function title(string $default = 'New project in apflow!') : string{
            $title = $this->load('localization')->translate()['TITLE'];
            
            if ($this->viewFound()) return $title[self::$_page] ?? $default;
            else return $title['404'] ?? '404 - Page not found!';
        }


        //------------------------------------------------------------------
        // TEMPLATE METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Load specific template.
         * 
         * @param string $template - Template name
         * @return object
         * 
         */
        public function loadTemplate(string $template){
            $load = $this->_loadAssetMVC($template, MVC_TEMPLATES_PATH);
            if ($load === false) throw new Exception('The `<q>'.$template.'</q>` template cannot be loaded because *<span>it does not exist!</span>*');
        }


        //------------------------------------------------------------------
        // MODEL METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Load specific model.
         * 
         * @param string $model - Model name
         * @return object
         * 
         */
        public function loadModel(string $model){
            $load = $this->_loadAssetMVC($model, MVC_MODELS_PATH);
            
            if ($load === false) throw new Exception('The `<q>'.$model.'</q>` model cannot be loaded because *<span>it does not exist!</span>*');
            else {
                $className = ucfirst(basename($model));
                return new $className;
            }
        }


        //------------------------------------------------------------------
        // VIEWS METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Loads views
         * 
         * @return void
         * 
         */
        private function _loadViews() : void{
            $options    = self::$_options[self::$_page] ?? [];
            $view       = self::$_view ?? self::$_page;
            $model      = $this->load('request')->location()->pathname;

            if (empty(self::$_view)) {
                $model  = $options['model'] ?? false;
                $view   = $options['view'];
            }

            // Check exists view
            if ($this->_fromURL == false) {
                $load = $this->_loadAssetMVC($view, MVC_VIEWS_PATH);
                if ($load === false) throw new Exception('The given `<q>'.$view.'</q>` model was not found!');
            }

            else $this->import($view);

            // Check exist model
            if ($model !== false) {
                $load = $this->_loadAssetMVC($model, MVC_MODELS_PATH, false);

                if ($load === false) {
                    $parent_model = explode('/', $model);
                    $parent_model = $parent_model[0];

                    $load = $this->_loadAssetMVC($parent_model, MVC_MODELS_PATH, false);
                    if ($load === false) throw new Exception('The given `<q>'.$model.'</q>` model was not found!');
                }
            }
        }


        /**
         * 
         * It imports both a single view.
         * 
         * @param bool $inside_404  - Attaching an 404 page
         * @param bool $checkView   - Check if the view exists
         * 
         * @return void
         * @uses viewFound(), view404(), _loadViews()
         * 
         */
        public function loadViews(bool $inside_404 = false, bool $checkView = true) : void{
            // Check if the view exists
            if ($checkView) {
                if ($this->viewFound()) $this->_loadViews($inside_404);

                // Displays the error page inside the sections container.
                else if ($inside_404) $this->view404();
            }

            else $this->_loadViews(false, false);
        }
    }