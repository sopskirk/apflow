<?php
/**
 * 
 * This class is used to handle 
 * the response of the request.
 * 
 * @package apflow
 * @subpackage Requests
 * @version 1.0
 * 
 */


    namespace apflow\Components\Requests;
    use apflow\Bootstrap;

    class Response extends Bootstrap{
        /**
         * 
         * Retrieves the response code.
         * 
         * @param int $code - Response code
         * @return void
         * 
         */
        public function __construct(?int $code = null){
            if (!is_null($code)) $this->_response($code);
        }


        //------------------------------------------------------------------
        // RESPONSE METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Sets the response code and sets 
         * it in a cookie file.
         * 
         * @param int $code - Response code
         * @return void
         * 
         */
        private function _response(int $code) : void{
            $this->load('session')->flash(['response' => $code]);
        }


        /**
         * 
         * Redirecting the page after sending the request.
         * 
         * @param string $path - Redirect to PATH URL
         * @return void
         * 
         */
        public function redirect(?string $path = null) : void{
            $path = !empty($path) ? $path : '/'.$this->load('request')->location()->pathname;

            ob_start();
		    header('Location:'.$path);
		    exit;
        }


        //------------------------------------------------------------------
        // MESSAGE METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Retrieves the response code and creates 
         * a message view based on the code.
         * 
         * @param string $html  - The HTML code where the message should be placed
         * @param array $msg    - Messages for validation statuses
         * 
         * @return string
         * 
         */
        public function message(string $html, array $msg) : string{
            $message = '';
            $session = $this->load('session');

            if ($session->has('response')){
                $code       = $session->id('flash.response')->data;
                $code       = is_int($code) && strlen($code) <= 3 && strlen($code) > 0 ? $code : 0;
                $message    = str_replace('message', $msg[$code], $html);
            }

            return $message;
        }
    }