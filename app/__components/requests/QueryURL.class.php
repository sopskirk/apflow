<?php
/**
 * 
 * This class gets the query from 
 * the URL and modifies it accordingly.
 * 
 * @package apflow
 * @subpackage Requests
 * @version 1.0
 * 
 */


    namespace apflow\Components\Requests;
    use Exception;

    class QueryURL extends Location{
        /**
         * 
         * It stores query data 
         * from the URL.
         * 
         * @var array
         * 
         */
        private $_queries = [];

        /**
         * 
         * Stores the value or name of
         * a specific query URL.
         * 
         * @var string
         * 
         */
        public $value;
        public $name;

        /**
         * 
         * It stores the validation results 
         * of a specific query URL.
         * 
         * @var bool
         * 
         */
        public $valid = false;

        /**
         * 
         * Stores the validation 
         * results of all queries URL.
         * 
         * @var array
         * 
         */
        private $_validation = [];


        //------------------------------------------------------------------
        // CONSTRUCT METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Instance of the method _query().
         * 
         * @return void
         * @uses _query()
         * 
         */
        public function __construct(){
            $this->_query();
        }


        //------------------------------------------------------------------
        // GET METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Retrieves the URL with the query and
         * converts it to an associative array.
         * 
         * @return object
         * 
         */
        private function _query() : object{
            $url    = parse_url($this->url(), PHP_URL_QUERY);
            $parts  = parse_url('?'.$url); // Simple trick to make it key value

            if (isset($parts['query'])) {
                parse_str(html_entity_decode($parts['query']), $query);
                $this->_queries = $query;
                
                return $this;
            } else throw new Exception('There is <span>no Query</span> in the URL!');
        }

        
        /**
         * 
         * Returns the filtered query 
         * value from the URL.
         * 
         * @return string
         * 
         */
        private function _value(string $value) : string{
            return $this->load('security')->filter($value, true);
        }


        /**
         * 
         * It retrieves all of the query from the URL 
         * along with their values ​​and returns it in 
         * the form of an associative array.
         * 
         * @param bool $valid - Checking all query value
         * @return mixed
         * 
         */
        public function all(bool $valid = false){
            if (!$valid) {
                $data = array();

                foreach ($this->_queries as $name => $value){
                    $data[$name]['value'] = $value;
                    if (!empty($this->_validation) && array_key_exists($name, $this->_validation)) $data[$name]['valid'] = $this->_validation[$name];
                }

                return $data;
            }

            // Check all data, if somewhere is false, 
            // it breaks the loops.
            foreach ($this->_validation as $name => $result) {
                $this->valid = $result;
                if (!$result) break;
            }
            
            return $this;
        }


        /**
         * 
         * Gets data about a particular 
         * query given as a parameter.
         * 
         * @return object
         * @uses _value()
         * 
         */
        public function get(string $name) : object{
            if (array_key_exists($name, $this->_queries) !== false) {
                $this->value    = $this->_value($this->_queries[$name]);
                $this->name     = $name;
                $this->valid    = $this->_validation[$name] ?? $this->valid;
            }

            return $this;
        }


        /**
         * 
         * Returns the validated value of
         * a specific query.
         * 
         * @param mixed $data       - A set of patterns or a collective pattern.
         * @param string $default   - Collective pattern
         *
         * @return void
         * @uses _value()
         * 
         */
        public function validation($data = 'http', string $default = 'http') : void{
            $validator = $this->load('security');

            /* Call to array $data */
            if (is_array($data)) {
                foreach ($data as $name => $pattern) {
                    if (array_search($name, $this->_queries) !== false) {
                        $value = $this->_value($this->_queries[$name]);
                        $this->_validation[$name] = $validator->validation($value, $pattern);
                    }
                }
                
                // When the field name was not provided in this method.
                if (count($this->_queries) > count($this->_validation)) {
                    foreach ($this->_queries as $name => $value) {
                        if (array_search($name, $this->_validation) === false) {
                            $value = $this->_value($this->_queries[$name]);
                            $this->_validation[$name] = $validator->validation($value, $default);
                        }
                    }
                }
            }

            /* Call to string $data */
            elseif (is_string($data)) {
                foreach ($this->_queries as $name => $value) {
                    $validation = $validator->validation($value, $data);
                    $this->_validation[$name] = $validation;

                    if (!$validation) break;
                }
            }

            /* Error: Diffrent type of argument. */
			else throw new Exception('Incompatible type </b>'.gettype($data).'<b> of the <a href="https://github.com/sopskirk/apflow/QueryURL#validation">QueryURL::validation</a> method argument!');
        }


        //------------------------------------------------------------------
        // BUILD METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Generate URL-encoded query string.
         * 
         * @link http://php.net/manual/en/function.http-build-query.php
         * @return string
         * 
         */
        public function build($query_data, string $numeric_prefix, string $arg_separator, int $enc_type = PHP_QUERY_RFC1738) : string{
            return http_build_query($query_data, $numeric_prefix, $arg_separator, $enc_type);
        }
    }