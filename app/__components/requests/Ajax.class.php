<?php
/**
 * 
 * This class is used to AJAX request.
 * 
 * @package apflow
 * @subpackage Requests
 * @version 1.0
 * 
 */


	namespace apflow\Components\Requests;

	class Ajax {
		/**
		 *
		 * Code status message
		 *
		 * @var int
		 *
		 */
		private $_code = 200;

		/**
		 *
		 * Content Type
		 *
		 * @var string
		 *
		 */
		private $_contentType = 'application/json; charset=UTF-8';


		/**
		 * 
		 * This method retrieves all headers 
		 * sent by the server.
		 * 
		 * @return array
		 * 
		 */
		private function _getAllHeaders() : array{
			$headers = [];
			
			foreach ($_SERVER as $name => $value) {
				if (substr($name, 0, 5) == 'HTTP_') $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}

			return $headers;
		}
		

		/**
		 * 
		 * This method returns the status 
		 * of the AJAX request.
		 * 
		 * @return string
		 * 
		 */
		private function _getStatusMessage() : string{
			$status = [
				100 => 'Continue',  
				101 => 'Switching Protocols',  
				200 => 'OK',
				201 => 'Created',  
				202 => 'Accepted',  
				203 => 'Non-Authoritative Information',  
				204 => 'No Content',  
				205 => 'Reset Content',  
				206 => 'Partial Content',  
				300 => 'Multiple Choices',  
				301 => 'Moved Permanently',  
				302 => 'Found',  
				303 => 'See Other',  
				304 => 'Not Modified',  
				305 => 'Use Proxy',  
				306 => '(Unused)',  
				307 => 'Temporary Redirect',  
				400 => 'Bad Request',  
				401 => 'Unauthorized',  
				402 => 'Payment Required',  
				403 => 'Forbidden',  
				404 => 'Not Found',  
				405 => 'Method Not Allowed',  
				406 => 'Not Acceptable',  
				407 => 'Proxy Authentication Required',  
				408 => 'Request Timeout',  
				409 => 'Conflict',  
				410 => 'Gone',  
				411 => 'Length Required',  
				412 => 'Precondition Failed',  
				413 => 'Request Entity Too Large',  
				414 => 'Request-URI Too Long',  
				415 => 'Unsupported Media Type',  
				416 => 'Requested Range Not Satisfiable',  
				417 => 'Expectation Failed',  
				500 => 'Internal Server Error',  
				501 => 'Not Implemented',  
				502 => 'Bad Gateway',  
				503 => 'Service Unavailable',  
				504 => 'Gateway Timeout',  
				505 => 'HTTP Version Not Supported'
			];
			
			return $status[$this->_code] ?? $status[500];
		}
		

		/**
		 * 
		 * This method sets the headers
		 * 
		 * @return void
		 * @uses _getStatusMessage()
		 * 
		 */
		public function _setHeaders() : void{
			header('HTTP/1.1 '.$this->_code.' '.$this->_getStatusMessage());
			header('Content-Type:'.$this->_contentType);
			header('Content-Type:'.$this->_contentType);
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0', false);
			header('Pragma: no-cache');
		}
		

		/**
		 * 
		 * This method is used to send data via AJAX.
		 * 
		 * @param mixed $data - json_encode data
		 * @param int $status - Status Message
		 * 
		 * @return void
		 * @uses _setHeaders()
		 * 
		 */
		public function response($data, ?int $status = null) : void{
			$this->_code = !is_null($status) ? $status : 200;
			$this->_setHeaders();
			
			echo json_encode($data);
			exit;
		}
	}