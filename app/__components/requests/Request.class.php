<?php
/**
 * 
 * This class is used to handle everything 
 * that is related to the request:
 * - Methods,
 * - Submit button,
 * - Inputs,
 * - Responses,
 * - Location,
 * - Query
 * 
 * @package apflow
 * @subpackage Requests
 * @version 1.0
 * 
 */


    namespace apflow\Components\Requests;
    use apflow\Bootstrap;

    class Request extends Bootstrap{
        /**
         * 
         * Checks whether the request was sent 
         * using the POST method.
         * 
         * @param bool $only_data_sent - Check only if the data has been sent
         * @return bool
         * 
         */
        public function isPost(bool $only_data_sent = false) : bool{
            return ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)) || ($only_data_sent && !empty($_POST)) ? true : false;
        }


        /**
         * 
         * Checks whether the request was sent 
         * using the GET method.
         * 
         * @param bool $only_data_sent - Check only if the data has been sent
         * @return bool
         * 
         */
        public function isGet(bool $only_data_sent = false) : bool{
            return ($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET)) || ($only_data_sent && !empty($_GET)) ? true : false;
        }


        /**
         * 
         * Checks whether the request was sent 
         * using the PUT method.
         * 
         * @param bool $only_data_sent - Check only if the data has been sent
         * @return bool
         * 
         */
        public function isPut(bool $only_data_sent = false) : bool{
            return ($_SERVER['REQUEST_METHOD'] === 'PUT' && !empty($_PUT))  || ($only_data_sent && !empty($_PUT)) ? true : false;
        }


        /**
         * 
         * Checks whether the request was sent 
         * using the AJAX method.
         * 
         * @return bool
         * 
         */
        public function isAjax() : bool{
            if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
                define('AJAX', true);
                return true;
            }

            return false;
        }


        /**
         * 
         * Checks whether the request was sent 
         * using the ALL methods.
         * 
         * @return string
         * 
         */
        public function isMethod() : string{
            $method = $this->load('security')->filter($_SERVER['REQUEST_METHOD']);
            return strtoupper($method);
        }


        //------------------------------------------------------------------
        // SUBMIT METHOD
        //------------------------------------------------------------------
        

        /**
         * 
         * Bind an event handler to the submit 
         * event form.
         * 
         * @param string $button - Button name (dot notation)
         * @param function $function - Function that is to be executed after calling the request.
         *
         * @return mixed
         * @uses isPost()
         * 
         */
        public function submit(string $button, $function){
            // Request sent by method ...
            $method = $this->isPost() ? $_POST : $_GET;

            // Dot notation use
            $data = $this->load('dot')->set($button, $method);
            return !is_null($data) ? $function : false;
        }


        //------------------------------------------------------------------
        // CALLS METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Calls the Input{} class methods.
         * 
         * @param string $input - Name(s) of input(s) (dot notation)
         * @return object
         * 
         */
        public function input(?string $input = null) : object{
            return new Input($input);
        }


        /**
         * 
         * Calls the Response{} class methods.
         * 
         * @param int $code - Response code
         * @return object
         * 
         */
        public function response(?int $code = null) : object{
            return new Response($code);
        }


        /**
         * 
         * Calls the Location{} class methods.
         * 
         * @return object
         * 
         */
        public function location() : object{
            return new Location;
        }


        /**
         * 
         * Calls the Query{} class methods.
         * 
         * @return object
         * 
         */
        public function query() : object{
            return new QueryURL;
        }


        /**
         * 
         * Calls the Ajax{} class methods.
         * 
         * @return object
         * 
         */
        public function ajax() : object{
            return new Ajax;
        }
    }