<?php
/**
 * 
 * This class displays basic data 
 * from the URL:
 * - full URL,
 * - host,
 * - protocol,
 * - hash,
 * - path
 * 
 * @package apflow
 * @subpackage Request
 * @version 1.0
 * 
 */


    namespace apflow\Components\Requests;
    use apflow\Bootstrap;

    class Location extends Bootstrap{
        /**
         * 
         * Stores parts of the full URL.
         * 
         * @var string
         * 
         */
        public $url;
        public $pathame;
        public $host;
        public $protocol;
        public $hash;


        //------------------------------------------------------------------
        // CONSTRUCT METHOD
        //------------------------------------------------------------------
        

        /**
         * 
         * Assigns the appropriate methods 
         * to variables.
         * 
         * @return void
         * @uses url(), _pathname(), _protocol(), _host(), _history(), _protocol(), _hash()
         * 
         */
        public function __construct(){
            $this->url      = $this->url();
            $this->pathname = $this->_pathname();
            $this->host     = $this->_host();
            $this->protocol = $this->_protocol();
            $this->hash     = $this->_hash();
        }


        //------------------------------------------------------------------
        // GET METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Retrieves the entire current URL.
         * 
         * @param bool $strip - Strip HTML and PHP tags from a string
         * @return string
         * 
         * @uses _protocol()
         * 
         */
        public function url(bool $strip = true) : string{
            static $filter, $scheme, $host;
            
            if (!$filter) {
                $filter = $this->load('security')->filterURL($_SERVER['REQUEST_URI'], $strip);
                $host   = $_SERVER['HTTP_HOST'];
                $scheme = $this->_protocol();
            }
	
		    return sprintf('%s://%s%s', $scheme, $host, $filter);
        }


        /**
         * 
         * Attaches a subpage to the URL.
         * 
         * @param string $path - Path to the subpage
         * @return string
         * 
         * @uses _protocol()
         * 
         */
        public function joinTo(string $path) : string{
            return $this->_protocol().'://'.$_SERVER['HTTP_HOST'].'/'.$path;
        }


        /**
         * 
         * Retrieves only the URI part 
         * of the URL.
         * 
         * @return string
         * @uses $_protocol()
         * 
         */
        private function _pathname() : string{
            $pathname = str_replace($this->_protocol().'://'.$_SERVER['HTTP_HOST'].'/', '', $this->url);
            if ($_SERVER['SERVER_SOFTWARE'] == 'Apache') $pathname = str_replace(substr(ROOT_PATH, 1), '', $this->pathname);

            if (strpos($pathname, '?') !== false) $pathname = substr($pathname, 0, strpos($pathname, '?'));
            return empty($pathname) || $pathname == '/' ? 'home' : trim($pathname, '/');
        }


        /**
         * 
         * Retrieves the current host 
         * of the page.
         * 
         * @return string
         * 
         */
        private function _host() : string{
            return $_SERVER['HTTP_HOST'];
        }


        /**
         * 
         * Retrieves the current protocol 
         * from the URL.
         * 
         * @return string
         * 
         */
        private function _protocol() : string{
            $protocol = (!isset($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS']) == 'off') ? 'http' : 'https';
            return $protocol;
        }


        /**
         * 
         * Retrieves everything after the # 
         * sign from the URL.
         * 
         * @return string
         * @uses $url
         * 
         */
        private function _hash() : ?string{
            return parse_url($this->url, PHP_URL_FRAGMENT);
        }


        //------------------------------------------------------------------
        // IS METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Checks whether the provided 
         * URL is allowed.
         * 
         * @param string $path      - Address URL
         * @param mixed $subpages   - Adresss URL Subpages
         * 
         * @return bool
         * 
         */
        public function is(string $path, $subpages = false) : bool{
            $_path = null;
            $is = $isSubpage = false;
            $path = str_replace('/', '\/', $path);

            // For many dirs
            if (strpos($path, '\/*') !== false) {
                $_path = substr($path, 0, -3);
                $pattern = !empty($_path) ? '/^('.$_path.'[^\w])[\/\w\S]+$/i' : '/^[\/\w\S]+$/i';

                $isSubpage = (bool)preg_match($pattern, $this->pathname);
            }

            // Only for one dir.
            if (strpos($path, '\/*') === false || $isSubpage === false) {
                $_path = $isSubpage === false ? $path : $_path;
                $pattern = !empty($_path) ? '/^('.$_path.'\/|'.$_path.'|'.$_path.'(\?|#)[\w\S\=\.&%-]+)$/i' : '/^((\?|#)[\w\S\=\.&%-]+)$/i';
            }
            
            $is = (bool)preg_match($pattern, $this->pathname);

            if ($isSubpage && $is) {
                if ($subpages !== false) {
                    $subpage = str_replace(substr($path, 0, -3), '', $this->pathname);
                    $subpage = trim($subpage, '/');

                    $is = in_array($subpage, $subpages);
                }
            }

            return $is;
        }
    }