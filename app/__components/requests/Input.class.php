<?php
/**
 * 
 * This class is used to get
 * information about input fields.
 * 
 * @package apflow
 * @subpackage Requests
 * @version 1.0
 * 
 */


    namespace apflow\Components\Requests;
    use apflow\Bootstrap;
    use apflow\Components\Facades\File;

    class Input extends Bootstrap{
        /**
         * 
         * It stores data of form fields.
         * 
         * @var array
         * 
         */
        private $_inputs = [];

        /**
         * 
         * It stores input data.
         * 
         * @var string
         * 
         */
        public $value;
        public $name;
        public $autocomplete;

        /**
         * 
         * It stores the validation results 
         * of a specific form field
         * 
         * @var bool
         * 
         */
        public $valid = true;

        /**
         * 
         * Stores the validation 
         * results of all form fields
         * 
         * @var array
         * 
         */
        private $_validation = [];

        /**
         * 
         * It stores the group name
         * of form fields.
         * 
         * @var string
         * 
         */
        private $_group_name;

        //------------------------------------------------------------------
        // STORAGE FILE VAR
        //------------------------------------------------------------------

        /**
         * 
         * Stores the path to 
         * the autocomplete storage file.
         * 
         * @var string
         * 
         */
        private const AUTOCOMPLETE_FILE = 'app/__storage/autocomplete.storage';


        //------------------------------------------------------------------
        // CONSTRUCT METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Instance of the methods _input() 
         * and _prefix().
         * 
         * @param string $input - Name(s) of input(s) (dot notation)
         * @return void
         * 
         * @uses _input()
         * 
         */
        public function __construct(?string $input = null){
            if (!is_null($input)) $this->_input($input);
        }


        //------------------------------------------------------------------
        // CHECK METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Checks whether the name method 
         * has been called.
         * 
         * @return void
         * 
         */
        private function _checkUNameMethod() : void{
            if (empty($this->value)) throw new Exception('The <a href="https://github.com/sopskirk/apflow/Input#name">Input::name</a> method <span>must be</span> called for proper operation this method!');
        }


        //------------------------------------------------------------------
        // AUTOCOMPLETE METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Saves the form field data that 
         * has been sent.
         * 
         * @return void
         * @uses autoclear()
         * 
         */
        private function _save() : void{
            $content = null;
            $this->autoclear();

            // Save to file...
            foreach ($this->_inputs as $name => $value) $content .= '['.strtoupper($name).': '.$value.'] ';
            file_put_contents(self::AUTOCOMPLETE_FILE, '['.$this->load('detect')->ip().'] '.$this->_group_name.' '.trim($content).PHP_EOL, FILE_APPEND);
        }


        /**
         * 
         * Automatic cleaning of the autocomplete file.
         * 
         * @param object $file - Object of the File class
         * @return void
         * 
         */
        private function _autoclear(File $file) : void{
            // Get current date.
            $current_date = date('Y:m:d H:i:s');

            // Get forward date.
            $forward_date = date('Y:m:d H:i:s', strtotime($current_date.'+ 24hours'));

            // Gets the first line from the log 
            // file - expiry date of the logs.
            $expiry = !$file->empty() ? $file->content(true)[0] : '';

            // Extracts the expiration date of logs.
            $extract_date = str_replace(array('[', ']'), '', $expiry);

            if (
                // Checks whether the ruler with the
                // creation date of the log file is correct.
                // If it is not, then clean the file and
                // add the correct line.
                preg_match('/\[[0-9]{4}:[0-9]{2}:[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\]/', $expiry) != 1 ||

                //Cleans the file every 12 hours.
                strtotime($extract_date) <= strtotime($current_date))
            {
                if (!$file->empty()) $file->clear();
                file_put_contents(self::AUTOCOMPLETE_FILE, '['.$forward_date.']'.PHP_EOL, FILE_APPEND);
            }
        }


        /**
         * 
         * Removes the autocomplete form fields 
         * from the storage file.
         * 
         * @param bool $all - Removes all form data from a specific IP
         * @return bool
         * 
         * @uses _autoclear()
         * 
         */
        public function autoclear(bool $all = false) : bool{
            $file = $this->load('file')->name(self::AUTOCOMPLETE_FILE);

            // Autoclear
            $this->_autoclear($file);

            // Search for...
            $find = $all ? $this->load('detect')->ip() : $this->_group_name;

            // Removes data
            return $all ? $file->deleteDuplicatesLine($find, true) : $file->deleteLine($find);
        }


        /**
         * 
         * Retrieves the saved data about the form
         * field in the form of a string or 
         * an associative array.
         * 
         * @return string
         * 
         */
        private function _autocomplete() : string{
            $inputs = [];
            $file = $this->load('file')->name(self::AUTOCOMPLETE_FILE);
            
            $content    = $file->content(true, false);
            $index      = $file->search($this->_group_name, true);

           // die(var_export($index));

            if ($index !== false) {
                if (preg_match_all('~\[(\w+):\s*([^][]*)]~', $content[$index], $matches)) {
                    array_shift($matches);
                    $inputs = array_combine($matches[0], $matches[1]);
                }

                if (!empty($this->name)) {
                    $name = strtoupper($this->name);
                    return array_key_exists($name, $inputs) ? $inputs[$name] : '';
                }

                else return $inputs;
            } return '';
        }


        //------------------------------------------------------------------
        // GET METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Extracts from the dot notation 
         * the group name of the form fields.
         * 
         * @param string $input - Name(s) of input(s) (dot notation)
         * @return void
         * 
         */
        private function _pullGroup(string $input) : void{
            $pos = stripos($input, '.');
            
            $this->_group_name = $pos !== false ? substr($string, -$pos + 1) : $input;
            $this->_group_name = '[GROUP: '.$this->_group_name.']';
        }


        /**
         * 
         * Retrieves input data from 
         * a specific request.
         * 
         * @param string $input         - Input(s) name(s) in dot notation
         * @param bool $only_data_sent  - Check only if the data has been sent
         * 
         * @return mixed
         * 
         */
        private function _input(string $input, bool $only_data_sent = false){
            $this->_pullGroup($input);

            // Ajax version
            if ($this->load('request')->isAjax()) {
                if (isset($_REQUEST[$input])) {
                    $request = $_REQUEST[$input];

                    foreach ($request as $key => $data) {
                        if (preg_match_all('~\[(\w+)]~', $request[$key]['name'], $matches)) {
                            $this->_inputs[$matches[1][0]] = $request[$key]['value'];
                        }
                    }

                    // Save data as an autocomplete form.
                    $this->_save();
                }
            } 
            
            // Normal version
            else {
                // Dot notation use
                $data = $this->load('dot')->set($input, $_REQUEST, null, true);
                
                if (!empty($data)) {
                    // Value assignment to input.
                    foreach ($data as $name => $_data) {
                        foreach ($_data as $_name => $value) $this->_inputs[$_name] = $value;
                    }

                    // Save data as an autocomplete form.
                    $this->_save();
                }
            }

            return $this;
        }


        /**
         * 
         * Returns the filtered value 
         * of a specific form field.
         * 
         * @return string
         * 
         */
        private function _value(string $value) : string{
            return $this->load('security')->filter($value, true);
        }


        /**
         * 
         * It retrieves all of the form fields 
         * and their data and returns them in 
         * the form of an associative array.
         * 
         * @param bool $valid - Checking all form data
         * @return mixed
         * 
         * @uses _value(), _autocomplete()
         * 
         */
        public function all(bool $valid = false){
            if (!$valid) {
                $data = [];

                foreach ($this->_inputs as $name => $value) {
                    $this->name = $name;

                    $data[$name]['value']           = $this->_value($value);
                    $data[$name]['autocomplete']    = $this->_autocomplete();

                    if (!empty($this->_validation) && array_key_exists($name, $this->_validation)) $data[$name]['valid'] = $this->_validation[$name];
                }

                return $data;
            }

            // Check all data, if somewhere is false, 
            // it breaks the loops.
            foreach ($this->_validation as $name => $result) {
                $this->valid = $result;
                if (!$result) break;
            }
            
            return $this;
        }


        /**
         * 
         * Gets data about a specific form 
         * field given as a parameter.
         * 
         * @param string $input - Input name
         * @param bool $autocomplete - Whether to show the saved input value?
         * 
         * @return object
         * @uses _value(), _autocomplete()
         * 
         */
        public function name(string $input, bool $autocomplete = false) : object{
            $this->name = $input;
            
            if (array_key_exists($input, $this->_inputs) !== false) {
                $this->value = $this->_value($this->_inputs[$input]);
                $this->valid = $this->_validation[$input] ?? $this->valid;
            }

            if ($autocomplete) $this->autocomplete = $this->_autocomplete();
            return $this;
        }


        /**
         * 
         * Returns the validated value of
         * a specific form field.
         * 
         * @param mixed $data       - A set of patterns or a collective pattern
         * @param bool $all 		- Validation for all input value
         * @param string $default   - Collective pattern
         *
         * @return void
         * @uses _value()
         * 
         */
        public function validation($data = 'text', bool $all = false, string $default = 'text') : void{
            $validator = $this->load('security');

            /* Call to string $data */
            if (is_string($data)) {
                foreach ($this->_inputs as $name => $value) {
                    $validation = $validator->validation($value, $name == 'email' ? 'email' : $data);
                    $this->_validation[$name] = $validation;
                }
            }

            /* Call to array $data */
            elseif (is_array($data)) {
                foreach ($data as $name => $pattern) {
                    if (array_key_exists($name, $this->_inputs)) {
                        $value = $this->_value($this->_inputs[$name]);
                        $this->_validation[$name] = $validator->validation($value, $pattern);
                    }
                }
                
                // When the field name was not provided in this method.
                if (count($this->_inputs) > count($this->_validation)) {
                    foreach ($this->_inputs as $name => $value) {
                        if (array_key_exists($name, $this->_validation) === false) {
                            $value = $this->_value($this->_inputs[$name]);
                            $this->_validation[$name] = $validator->validation($value, $default);
                        }
                    }
                }
            }

            /* Error: Diffrent type of argument. */
            else throw new Exception('Incompatible type </b>'.gettype($data).'<b> of the <a href="https://github.com/sopskirk/apflow/Input#validation">Input::validation</a> method argument!');
                
            // The overall result of validation for input value.
            if ($all) {
                foreach ($this->_validation as $name => $valid) {
                    $this->valid = $valid;
                    if (!$valid) break;
                }
            }
        }
    }