<?php
 /**
  *
  * Modified and improved function to import any files embedded
  * on the server is a central class to import resources.
  *
  * @package apflow
  * @version 1.0
  *
  */


	namespace apflow\Components;
	use Exception;

	class Import{
		//------------------------------------------------------------------
        // FILTER METHOD
        //------------------------------------------------------------------


		/**
		 *
		 * Filters the path to the file given as the method argument.
		 *
		 * @param string $path - Filename or path to file
		 * @return string
		 *
		 */
		public function filterPath(string $path) : string{
			$path = str_replace(array('./', '.\/'), '', $path);
			$path = strip_tags($path);
			$path = preg_replace('#(/|\/)+#', DIRECTORY_SEPARATOR, trim($path));
			$path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);

			return $path;
		}


		//------------------------------------------------------------------
        // CHECK METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Checks whether a file exists.
		 *
		 * @param string $path 	- Path to file
		 * @param bool $error 	- Display error
		 * 
		 * @return mixed
		 *
		 */
		private function _isFile(string $path, bool $error){
			$path = $this->filterPath($path);

			if (!file_exists($path)) {
				if ($error) throw new Exception('No such file <q>'.$path.'</q>!');
				else return false;
			}
			
			else return $path;
		}


		/**
		 *
		 * Checks whether a path exists.
		 * 
		 * @param string $path 	- Only path
		 * @param bool $error 	- Display error
		 *
		 * @return mixed
		 *
		 */
		private function _isPath(string $path, bool $error){
			$path = $this->filterPath($path);

			if (!is_dir($path)) {
				if ($error) throw new Exception('The given path <q>'.$path.'</q> does not exist!');
				else return false;
			}
			
			else return $path;
		}


		/**
		 *
		 * Checks whether a path or file exists.
		 * 
		 * @param string $path 	- Filename or path to file
		 * @param bool $error 	- Display error
		 * @param bool $check	- It only checks the path to the file
		 * 
		 * @return mixed
		 * @uses _isFile(), _isPath()
		 *
		 */
		protected function path(string $path, $error = null, bool $check = false){
			$error = !is_null($error) ? $error : DISPLAY_HAS_ERROR;

			// When will glob be used or
			// When to check only the dirname path.
			if (strpos($path, '*') !== false || $check) return $this->_isPath($path, $error);

			// When when checking the file or the path.
			else return strpos($path, '.') !== false ? $this->_isFile($path, $error) : $this->_isPath($path, $error);
		}


		//------------------------------------------------------------------
        // INCLUDE METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Creates an absolute path to the file and
		 * modifies it accordingly.
		 * 
		 * @param string $path 	- Filename or path to file
		 * @param bool $return	- Is there anything to return for?
		 * 
		 * @return mixed
		 * @uses path(), _importConfig()
		 *
		 */
		private function _include(string $path, bool $return = false){
			$path = $this->path($path);
				
			if ($return) return require $path;
			else require $path;
		}


		/**
		 *
		 * Imports a specific file or group of files to the project.
		 * 
		 * @param mixed $path 		- Filename or path to file
		 * @param bool $return		- Is there anything to return for?
		 * @param string $omission	- Files that will be omitted in the glob
		 * 
		 * @return mixed
		 * @uses _include(), path()
		 * 
		 */
		public function import($path, bool $return = false, string $omission = null){
			/* Call to array $path argument. */
			if (is_array($path)) foreach ($path as $file) $this->_include($file);

			/* Call to string $path argument. */
			elseif (is_string($path)) {
				/* Call to normal structure for $path argument. */
                if (strpos($path, '*') === false) {
					if ($return) return $this->_include($path, $return);
					else $this->_include($path);
				} 
				
				/* Call to glob structure for $path argument. */
				else {
					$path = $this->path($path).DIRECTORY_SEPARATOR.basename($path);
					
					if (!empty($omission)) {
						foreach (glob($path) as $file) {
							if (strpos($file, $omission) === false) $this->_include(basename($file));
						}
					} else foreach (glob($path) as $file) $this->_include(basename($file));
				}
            }

			/* Error: Diffrent type of argument(s). */
			else throw new Exception('Incompatible type <b>'.gettype($path).'</b> of the <a href="https://github.com/sopskirk/apflow/Import#import">Import::import</a> method argument(s)!');
		}
	}