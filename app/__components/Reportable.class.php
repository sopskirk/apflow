<?php
/**
 *
 * Supports reporting of returned 
 * exceptions.
 *
 * @package apflow
 * @version 1.0
 *
 */


	namespace apflow\Components;
	use apflow\Components\Requests\Ajax;
	use apflow\Bootstrap;

	class Reportable extends Bootstrap{
        /**
         * 
         * It stores the unique identifier 
         * of the report.
         * 
         * @var mixed
         * 
         */
		public $errid 		= null;
        private $_isErrid 	= false;
        
        /**
         * 
         * It stores information if 
         * there is an object.
         * 
         * @var bool
         * 
         */
        private $_isObject = false;

        /**
         * 
         * It stores information
         * about the error.
         * 
         * @var string
         * 
         */
		private $_message 	= null;
		private $_file 		= null;
		private $_line 		= null;
		private $_code 		= null;
		private $_trace 	= null;
		private $_sql 		= null;
		private $_db_error	= null;
		
		/**
		 * 
		 * Report file
		 * 
		 * @var string
		 * 
		 */
		private $_reportFile = null;
        

        //------------------------------------------------------------------
        // CONSTRUCT METHOD
        //------------------------------------------------------------------


        /**
		 * 
		 * Initializes the error data.
		 * 
		 * @param mixed $get        - Throwable object or Message string
		 * @param string $file      - The name of the file in which the error occurred
		 * @param string $line      - Line number in the file in which the error occurred
		 * @param string $sql       - SQL Query
		 * @param string $db_error  - Error DB
		 * @param string $errid     - Own ERROR ID
		 * 
		 * @return object
		 * @uses _view()
		 * 
		 */
		public function __construct($get = null, ?string $file = null, ?int $line = null, ?string $sql = null, ?string $db_error = null, ?string $errid = null) {
			if (is_string($get)) {
				$this->_message 	= $get;
				$this->_file 		= $file;
				$this->_line 		= $line;
				$this->_sql 		= $sql;
				$this->_db_error 	= $db_error;
			} else {
				$this->_message 	= $get->getMessage();
				$this->_file 		= $get->getFile();
				$this->_line 		= $get->getLine();
				$this->_code 		= $get->getCode();
				$this->_trace 		= $get;

				$this->_isObject = true;
			}

			$this->errid 		= $errid;
			$this->_isErrid		= !is_null($errid);
			$this->_reportFile	= APP_PATH.'/__log/reports_'.date('m').'_'.date('Y').'.log';
			
			$this->_view();
        }
        

        //------------------------------------------------------------------
        // OTHERS METHODS
        //------------------------------------------------------------------


        /**
         * 
         * Checks whether the request was sent 
         * using the AJAX method.
         * 
         * @return bool
         * 
         */
		private function _isAjax(){
			return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
		}


        /**
         *
         * Rand a string from the specified range.
         * 
         * @param int $quantity - How many numbers are to be drawn
         * @return mixed
         *
         */
		private function _generateID(int $quantity){
            $rand = '';
            $chars = array_flip(array_merge(range('a', 'z'), range('A', 'Z'), range(1, 9)));

            for ($i = 0; $i < $quantity; ++$i) $rand .= array_rand($chars);
            return $rand;
		}


		/**
		 * 
		 * Clears the message from HTML 
		 * tags and special characters.
		 * 
		 */
		private function _cleanMessage(bool $strip = false) : string{
			$message = str_replace([ '`', '*' ], '', $this->_message);
			return $strip ? strip_tags($message) : $message;
		}


		//------------------------------------------------------------------
        // REPORT METHODS
        //------------------------------------------------------------------
		

		/**
		 *
         * Gets a specific report from the log file.
		 * 
         * @param string $phrase - A phrase that is included in the search query.
		 * @return array
		 *
         */
        public function get(string $phrase){
            $file = $this->load('file');
			$result = [];

			if ($file->has($this->_reportFile)) {
				$file = $file->name($this->_reportFile);

				if (($file->search($phrase)) !== false) {
					$content = $file->content(true)[$file->search($phrase, true)];

					// Creates an array of the search report.
					if (preg_match_all('~\[(\w+):\s*([^][]*)]~', $content, $matches)) {
						array_shift($matches);
						$result = array_combine($matches[0], $matches[1]);
					}
				}
			}

            return $result;
        }


        /**
         * 
         * Add new report to log file.
         * 
         * @return void
         * 
         */
        private function _addReport() : void{
            // Error view
			$view = $this->_isAjax() ? 'AJAX' : 'HTML';

			// Get Report Data
			$report = $this->get('[ERROR_MSG: '.$this->_cleanMessage(true).']');

			// Data of the found report,
			$rmsg 	= $report['ERROR_MSG'] ?? null;
			$rfile 	= $report['FILE'] ?? null;
			$rline  = $report['LINE'] ?? null; 
			$rid    = $report['ERROR_ID'] ?? 0;

			// Preventing duplication of reports.
			if ($this->_cleanMessage(true) != $rmsg && $this->_file != $rfile && $this->_line != $rline) {
				// Unique Report Identifier
				$this->errid = $this->_generateID(15);

				// Adds a new liner except for the log file.
				$content = '[ERROR_ID: '.$this->errid.'] [ERROR_MSG: '.$this->_cleanMessage(true).'] [ERROR_SQL_QUERY: '.$this->_sql.'] [ERROR_DB: '.$this->_db_error.'] [FILE: '.$this->_file.'] [LINE: '.$this->_line.'] [DATE: '.date('Y:m:d H:i:s').'] [VIEW: '.$view.']';
				file_put_contents($this->_reportFile, $content.PHP_EOL, FILE_APPEND);

				if (SLACK_WEBHOOK_RUN == true) {
                    $this->load('api')->Slack([
                        'attachments' => [
							'title'	=> 'Błąd serwera',
                        	'text' => strip_tags($this->_message)."\n".$this->_db_error."\n\n".'Kod błędu: `'.$this->errid.'`'
                        ]
                    ]);
                }
			}

			// Places the same error in 
            // errid Unique Report Identifier.
            else $this->errid = $rid;
        }


        //------------------------------------------------------------------
        // SET METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Sets the look of the message of
         * the generated exception.
		 * 
		 * @return mixed
         * 
         */
        private function _set(){
			$table = $trace = null;
			
			if (!$this->_isAjax()) {
				// The displayed Report message depending on
				// the visibility version of the report.
				if (ERROR_VISIBLE == 'private') {
					$message 	= $this->_cleanMessage();
					$file 		= $this->_file;
					$line 		= $this->_line;
					
					if (!$this->_isObject) {
						$sql 		= !empty($this->_sql) ? '<tr><th><h1>sql query</h1></th><th class="sql-query"><span>'.$this->_sql.'</span></th></tr>' : '';
						$db_error 	= !empty($this->_db_error) ? '<tr><th><h1>db error</h1></th><th><span>'.$this->_db_error.'</span></th></tr>' : '';
						$trace 		= null;
					} else {
						$sql 		= null;
						$db_error	= null;
						$trace 		= nl2br($this->_trace->getTraceAsString());
					}

					// More information about error.
					$table = '<table class="table-container"><tbody><tr><th><h1>file</h1></th><th><span>'.$file.'</span></th></tr><tr><th><h1>line</h1></th><th><span>'.$line.'</span></th></tr>'.$sql.$db_error.'<tr><th title="Unique Report Identifier"><h1>id</h1></th><th><span>'.$this->errid.'</span></th></tr></tbody></table>';

					// Trace exception
					if (!is_null($trace)) $trace = '<div class="btn-trace"><button><span>Preview error trace</span></button></div><div class="trace-container" style="display:none" id="trace"><div class="btn-close"><button><span>Close preview</span></button></div><div class="trace"><p>'.$trace.'</p></div></div>';
				} 
				
				// Message exception
				else $message = 'Unique Error Identifier: <u>'.$this->errid.'</u>';

				// CSP Nonce
				$nonce = (LOAD_CSP == true && LOAD_COMPOSER == true) ? $this->load('csp')->nonce() : 'nonce';

				// Returns the complete message of the report.
				return '<!doctype html><html lang="en"><head><title>apflow - Exception report</title><meta charset="utf-8"><meta name="robots" content="noindex"><link href="https://fonts.googleapis.com/css?family=Comfortaa:700|Roboto:400,700" rel="stylesheet"><link rel="stylesheet" href="/app/__errors/assets/styles/style.css"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"><script src="/app/__errors/assets/scripts/jquery-3.3.1.min.js" nonce="'.$nonce.'"></script><script src="/app/__errors/assets/scripts/index.min.js" nonce="'.$nonce.'"></script></head><body><div class="global-container"><div class="content-container" id="reportable"><div class="logo"><a href="/" title="apflow" target="_blank">apflow</a><p>error engine</p></div><div class="message" id="message"><p>'.$message.'</p></div>'.$table.$trace.'<footer class="footer-container"><span>An exception report generated by apflow</span></footer></div></div></body></html>';
			} else {
				$report = [];
				
				// The displayed Report message depending on
				// the visibility version of the report.
				if (ERROR_VISIBLE == 'private') {
					$report['error'] = [
						'ID'		=> $this->errid,
						'Message'	=> strip_tags($this->_message),
						'FILE'		=> $this->_file,
						'LINE'		=> $this->_line,
						'SQL'		=> $this->_sql,
						'ERROR DB'	=> $this->_db_error,
						'TRACE'		=> !is_null($this->_trace) ? $this->_trace->getTrace() : $this->_trace
					];
				} else $report['error']['ID'] = $this->errid;

				$report['status'] = false;
				(new Ajax)->response($report, 400);
			}
        }


        //------------------------------------------------------------------
        // VIEW METHOD
        //------------------------------------------------------------------


        /**
         * 
         * Displays the appropriate report and saves 
         * the error information to the log file.
		 * 
		 * @return void
         * 
         */
        private function _view() : void{
			@ob_end_clean();

            // Displays the error report and places
            // it in the log file.
            $this->_addReport();
            if (!$this->_isErrid) {
				echo $this->_set();
				exit;
			}
        }
    }