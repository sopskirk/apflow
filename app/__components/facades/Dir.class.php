<?php
/**
 *
 * This class manages the folders on the server,
 * creates, deletes, clears their contents.
 *
 * @package apflow
 * @subpackage Facades
 * @version 1.0
 *
 */


	namespace apflow\Components\Facades;

	use apflow\Components\Import;
	use DirectoryIterator;
	use Exception;
	use RecursiveDirectoryIterator;
	use RecursiveIteratorIterator;

	class Dir extends Import{
		/**
		 *
		 * Sets the absolute path to the dir and checks
		 * if the specified dir exists.
		 *
		 * @param $path 	- The relative path under which the folder should be located.
		 * @param $create	-
		 * 
		 * @return bool
		 *
		 */
		private function has(string $path, bool $create = false) : bool{
			$dir = $create ? dirname($this->path($path)) : $this->path($path);
			if (!is_dir($dir)) throw new Exception('The specified <q>'.$dir.'</q> folder does not exist under the specified path!');
		}


		/**
		 *
		 * Creates a new folder under 
		 * the specified path.
		 *
		 * @param string $path 		-
		 * @param int $access 		- Folder access rights
		 * @param int $recursive 	-
		 * 
		 * @return bool
		 *
		 */
		public function set(string $path, int $access, int $recursive) : bool{
			$this->exists($path, true);
			return !is_dir($this->path) ? mkdir($this->path, $access, $cursive) : false;
		}


		/**
		 *
		 * Creates folder/s at once.
		 *
		 * @param array $arg -
		 * @return bool
		 * 
		 * @uses set()
		 *
		 */
		public function create(array $arg) : bool{
			if (is_multi_array($arg)){
				foreach ($arg as $key => $item){
					$access 	= findKey($item, 1) ? $item[1] : 0600;
					$recursive 	= findKey($item, 2) ? $item[2] : false;

					return $this->set($item[0], $access, $recursive);
				}
			} else {
				$access 	= array_key_exists(1, $arg) ? $arg[1] : 0600;
				$recursive	= array_key_exists(2, $arg) ? $arg[2] : false;
				
				return $this->set($arg[0], $access, $recursive);
			}
		}


		/**
		 *
		 * Deletes the folder and all its contents from
		 * the path specified.
		 *
		 * @return delete folder
		 *
		 * @see rmdir()
		 * @method clear()
		 *
		 */
		private function delete(string $path){
			$this->clear($path);
			return rmdir($this->path);
	 	}


		/**
		 *
		 * Removes (cleans) all the contents (files and dir) of
		 * a folder under the specified path.
		 *
		 * @return void
		 * @see unlink()
		 *
		 */
		public function clear(string $path){
			$this->exists($path);

			foreach(new DirectoryIterator($this->path) as $fileinfo){
				if ($fileinfo->isFile() || $fileinfo->isLink()) unlink($fileinfo->getPathName());
				elseif (!$fileinfo->isDot() && $fileinfo->isDir()) $this->delete(str_replace('\\', DS, $path.DS.basename($fileinfo->getPathName())));
			}
		}


		/**
		 * 
		 * Listing all the folders subfolders 
		 * and files in a directory.
		 * 
		 * @param string $dir - Main dir
		 * @return array
		 * 
		 */
		public function list(string $path) : array{
			$rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
			$files = [];

			foreach ($rii as $file) {
				if ($file->isDir()) continue;
				$files[] = $file->getPathname(); 
			}

			return $files;
		}
	}
