<?php
/**
 *
 * This class manages the file stream, 
 * creates, deletes, updates and reads 
 * the file from the file.
 *
 * @package apflow
 * @subpackage Facades
 * @version 1.0
 *
 */


	namespace apflow\Components\Facades;
	
	use apflow\Components\Import;
	use RecursiveIteratorIterator;
	use RecursiveDirectoryIterator;
	use Exception;

	class File extends Import{
		/**
		 * 
		 * Stores paths to files.
		 * 
		 * @var array
		 * 
		 */
		private $_pathes = [];

		/**
		 *
		 * It stores input data.
		 *
		 * @var string
		 *
		 */
		public $content;
		public $length;
		public $size;
		public $lastModified;
		public $mode;
		public $files;
		public $path;

		/**
		 *
		 * Stores the decision to add a new line.
		 * Enabled by default.
		 *
		 * @var bool
		 *
		 */
		private $_new_line = true;

		public function __construct(){
			clearstatcache();
		}


		//------------------------------------------------------------------
        // CHECK METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Checks whether the method has not
		 * been called (name).
         * 
		 * @param bool $pathes - Using the array type for name method
		 * @return void
		 *
		 */
		private function _checkNameMethod(bool $pathes = false){
			$error = 'The <a href="https://github.com/sopskirk/apflow/File#name">File::name</a> method must be called for proper operation this method!';
			
			if (!$pathes) {
				if (empty($this->path)) throw new Exception($error);
			} else {		
				if (!empty($this->path) || !empty($this->_pathes)) return null;
				else throw new Exception($error);
			}
		}


		/**
		 *
		 * Checks whether the file exists at all.
         * 
		 * @param mixed $file 	- Filename(s) or path(s) to file(s)
		 * @param bool $error 	- Display error
		 * @param bool $bool 	- Return only bool
		 * 
		 * @return mixed
		 *
		 */
		public function has($file, $error = null, $bool = null){
			$error 	= !is_null($error) ? $error : DISPLAY_HAS_ERROR;
			$bool 	= !is_null($bool) ? $bool : DISPLAY_AS_BOOL;

			/* @call to string $file argument. */
			if (is_string($file)) return $this->path($file, $error) != false ? true : false;

			/* @call to array $file argument. */
			if (is_array($file)) {
				if (!$error) {
					$result = [];

					foreach ($file as $_file) {
						$exists = $this->path($file, false);
						
						if (!$bool) $result[basename($_file)] = $exists;
						else if (!$exists) return false;
					}

					return !$bool ? $result : true;
				} else throw new Exception('If the first argument of the <a href="https://github.com/sopskirk/apflow/File#has">File::has</a> method has been called as an <uarray</u>, then the second argument <span>can not be</span> <b>true</b>');
			}

			/* @error: Diffrent type $file argument. */
			else throw new Exception('Incompatible type <b>'.gettype($file).'</b> of the <a href="https://github.com/sopskirk/apflow/File#has">File::has</a> method argument(s)!');
		}


		/**
		 *
		 * Checks whether the file is empty.
         * 
		 * @param bool $bool - Return only bool
		 * @return mixed
		 * 
		 * @uses _checkNameMethod()
		 *
		 */
		public function empty($bool = null){
			$bool = !is_null($bool) ? $bool : DISPLAY_AS_BOOL;

			// Has the name method been called?
			$this->_checkNameMethod(true);

			/* @call to array $_pathes var. */
			if (!empty($this->_pathes)) {
				$result = [];
				
				foreach ($this->_pathes as $file) {
					$empty = empty(trim(file_get_contents($file)));
					
					if (!$bool) $result[basename($file)] = $empty;
					else if (!$empty) return false;
				}
	
				return !$bool ? $result : true;
			}

			/* @call to string $path var. */
			return empty(trim(file_get_contents($this->path)));
		}


		//------------------------------------------------------------------
        // CREATE METHODS
		//------------------------------------------------------------------
		

		/**
		 *
		 * Manages the addition of a new line
		 * at the beginning or end of the line.
		 * Default sets true.
		 *
		 * @param bool $new_line - Is there a new line
		 * @return object
		 *
		 */
		public function newLine(bool $new_line = true){
			$this->_new_line = $new_line;
			return $this;
		}
		

		/**
		 *
		 * This is the trait for methods from
		 * the create and update section.
		 *
		 * @param string $path		- Filename or path to file
		 * @param string $content 	- New content added to the file
		 * @param string $prepend 	- Prepend the data to the file?
		 * @param bool $append 		- Append the data to the file?
		 * @param bool $put 		- Create a new file?
		 * 
		 * @return bool
		 *
		 */
		private function __traitFileMethods(?string $path = null, string $content, bool $prepend = false, bool $append = false, bool $put = false) : bool{
			// Designation of adopted extensions.
			if (is_null($this->path)) $this->path = $this->path($path, false, true);

			// Create a prepend content.
			if ($prepend) $content = $this->_new_line  && !$this->empty() ? $content.PHP_EOL.$this->content(false, false).PHP_EOL : $content.$this->content(false, false);
			
			// Create a append content.
			elseif ($append) $content = $this->_new_line && !$this->empty() ? $this->content(false, false).PHP_EOL.$content.PHP_EOL : $this->content(false, false).$content;
			
			// Create a normal content.
			else $content = $this->_new_line ? $content.PHP_EOL : $content;

			// Adds the content to the file.
			return (bool)file_put_contents($this->path, $content);
		}


		/**
		 * 
		 * Creates a file and overwrites its contents
		 * each time the method is called.
		 *
		 * @param mixed $data - File data
		 * 
		 * @return void
		 * @uses __traitFileMethods
		 *
		 */
		public function put($data) : bool{
			/* @call to string $data argument. */
			if (is_string($data)) return $this->__traitFileMethods($data, '', false, false, true);

			/* @call to array $data argument. */
			if (is_array($data)) {
				foreach ($data as $filename => $content) {
					$created = $this->__traitFileMethods($filename, $content, false, false, true);
					if (!$created) return false;
				}

				return true;
			}

			/* @error: Diffrent type $data argument. */
			else throw new Exception('Incompatible type <b>'.gettype($data).'</b> of the <a href="https://github.com/sopskirk/apflow/File#put">File::put</a> method argument!');
		}


		//------------------------------------------------------------------
        // UPDATE METHODS
        //------------------------------------------------------------------


		/**
		 * 
		 * At the beginning of the file, add the content provided
		 * as the method argument.
		 *
		 * @param string $content - New content added to the file
		 * @return bool
		 * 
		 * @uses _checkNameMethod()
		 *
		 */
		public function prepend(string $content) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			// Update file
			return $this->__traitFileMethods(null, $content, true);
		}


		/**
		 * 
		 * At the end of the file, add the content provided
		 * as the method argument.
		 *
		 * @param string $content - New content added to the file
		 * @return bool
		 * 
		 * @uses _checkNameMethod()
		 *
		 */
		public function append(string $content) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			// Update file
			return $this->__traitFileMethods(null, $content, false, true);
		}


		/**
		 * 
		 * It allows you to change the mode file:
		 * - private (0666),
		 * - public (0755),
		 * - protected (0750)
		 * 
		 * @param string $modifier - Modifier for accessing the file
		 * @return bool
		 * 
		 * @uses _checkNameMethod()
		 * 
		 */
		public function access(string $modifier) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			switch ($modifier) {
				case 'private' 		: $modifier = 0666;break;
				case 'public'		: $modifier = 0755;break;
				case 'protected'	: $modifier = 0750;break;
				default 			: $modifier = 0755;
			}

			// Sets a new file permissions.
			chmod($this->path, $modifier);
		}


		/**
		 *
		 * Changes the file name to the new name.
		 *
		 * @param string $filename - New name for the file
		 * @return bool
		 * 
		 * @uses _checkNameMethod()
		 *
		 */
		public function rename(string $filename) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			// Gets the name of the directory.
			$dir = dirname($this->path);

			// Modify filename
			return rename($this->path, $dir.DS.$filename);
		}


		//------------------------------------------------------------------
        // TRANSFER METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Performs the operation of moving
		 * or copying the selected file
		 * to a new location.
		 *
		 * @param string $place - Place of file transfer
		 * @param bool $moved 	- Is the file moved?
		 * 
		 * @return bool
		 * @uses content(), mode(), put(), trash(), isExists()
		 *
		 */
		private function __traitTransferMethods(string $place, bool $moved) :bool{
			// New path to file.
			$place 	= str_replace('.', '', $place);
			$path 	= $place.DS.basename($this->path);

			// Until the file exists.
			if (!$this->isExists($path) || preg_match('/^(\.|\.\|\.\/)+/', $path)) {
				$use = $this->name($this->path);

				// Get content file
				$content = $moved ? trim($use->content(false, false)) : $use->content(false, false);

				// Get mode file
				$mode = $use->mode(true);

				// Remove old file.
				// Only when the file transfer operation is performed.
				if ($moved) $use->trash();

				// Create new file
				$this->put($path, $content);

				// Transfer of file rights.
				chmod($this->path, '0'.$mode);

				// Have you succeeded?
				return $this->isExists($path);
			} else return true;
		}


		/**
		 *
		 * Moves the selected file or files to
		 * the specified new path.
		 *
		 * @param string $place - Place of file transfer
		 * @return bool
		 * 
		 * @uses __traitChangeMethods()
		 *
		 */
		public function move(string $place) : bool{
			return $this->__traitChangeMethods($place, true);
		}


		/**
		 *
		 * Copy the selected file or files to
		 * the specified new path.
		 *
		 * @param string $place - Place of file transfer
		 * @return bool
		 * 
		 * @uses __traitChangeMethods()
		 *
		 */
		public function copy($place) : bool{
			return $this->__traitChangeMethods($place, false);
		}


		//------------------------------------------------------------------
        // GET METHODS
		//------------------------------------------------------------------
		

		/**
		 * 
		 * Places the file data from the function 
		 * (default values) to the variable.
		 * 
		 * @return object
		 * @uses _checkNameMethod()
		 * 
		 */
		public function get(){
			// Has the name method been called?
			$this->_checkNameMethod();

			$this->content 		= $this->content();
			$this->length 		= $this->length();
			$this->size 		= $this->size();
			$this->lastModified	= $this->lastModified();
			$this->mode 		= $this->mode();
			$this->files 		= $this->files();

			return $this;
		}


		/**
		 *
		 * Reads the contents of the file given as 
		 * the method argument.
		 *
		 * @param bool $array 		- Is he to return the boards
		 * @param bool $new_line 	- Whether to display a new line
		 * 
		 * @return mixed
		 * @uses _checkNameMethod()
		 *
		 */
		public function content(bool $array = false, bool $new_line = true){
			// Has the name method been called?
			$this->_checkNameMethod();

			// Is there a new line?
			if (!$array) return $new_line ? nl2br(file_get_contents($this->path)) : file_get_contents($this->path);

			// Return content as array.
			else return file($this->path, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
		}


		/**
		 *
		 * Gets the number of lines from
		 * the file bypassing empty lines.
		 *
		 * @return int
		 * @uses _checkNameMethod(), content()
		 *
		 */
		public function length() : int{
			// Has the name method been called?
			$this->_checkNameMethod();
			return count($this->content(true, false));
		}


		/**
		 *
		 * Gets the size of the file given as an argument in the get() method.
		 * By default units using KB and MB are enabled.
		 *
		 * @param bool $units - Size display with units
		 * @return mixed
		 * 
		 * @uses _checkNameMethod()
		 *
		 */
		public function size(bool $units = true){
			// Has the name method been called?
			$this->_checkNameMethod();

			// What size is the file?
			$size = filesize($this->path);
			
			if ($units) {
				if ($size >= 1048576) 	$size = number_format($size / 1048576, 2).' MB';
				elseif ($size >= 1024) 	$size = number_format($size / 1024, 2).' KB';
				elseif ($size > 1) 		$size = $size.' bytes';
				elseif ($size == 1) 	$size = $size.' byte';
				else 					$size = '0 bytes';
			}

			return $size;
		}


		/**
		 *
		 * Gets the last modified time of the file given.
		 *
		 * @param string $formt - formate Date
		 * @return string
		 * 
		 * @uses _checkNameMethod()
		 *
		 */
		public function lastModified(string $format = 'Y:m:d H:i:s') : string{
			// Has the name method been called?
			$this->_checkNameMethod();

			// Returns the full date.
			return date($format, filemtime($this->path));
		}


		/**
		 *
		 * Gets the open/write mode of the file given.
		 *
		 * @param bool $retInt - Is it to return the rights in the number format?
		 * @return mixed
		 * 
		 * @uses _checkNameMethod()
		 * @link http://php.net/manual/pl/function.fileperms.php
		 *
		 */
		public function mode(bool $retInt = true){
			// Has the name method been called?
			$this->_checkNameMethod();
			
			// What are the file permissions?
			$perms = fileperms($this->path);

			if (!$retInt) {
				// Socket
				if (($perms & 0xC000) == 0xC000) $stats = 's';

				// Symbolic link
				elseif (($perms & 0xA000) == 0xA000) $stats = 'l';

				// Simple link
				elseif (($perms & 0xA000) == 0xA000) $stats = 'l';

				// Block device
				elseif (($perms & 0x6000) == 0x6000) $stats = 'b';

				// Catalog
				elseif (($perms & 0x4000) == 0x4000) $stats = 'd';

				// Character device
				elseif (($perms & 0x2000) == 0x2000) $stats = 'c';

				// FIFO
				elseif (($perms & 0x1000) == 0x1000) $stats = 'p';

				// Unknow
				else $stats = 'u';
				
				// Owner
				$stats .= (($perms & 0x0100) ? 'r' : '-');
				$stats .= (($perms & 0x0080) ? 'w' : '-');
				$stats .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x' ) : (($perms & 0x0800) ? 'S' : '-'));
				
				// Group
				$stats .= (($perms & 0x0020) ? 'r' : '-');
				$stats .= (($perms & 0x0010) ? 'w' : '-');
				$stats .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x' ) : (($perms & 0x0400) ? 'S' : '-'));
				
				// Others
				$stats .= (($perms & 0x0004) ? 'r' : '-');
				$stats .= (($perms & 0x0002) ? 'w' : '-');
				$stats .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x' ) : (($perms & 0x0200) ? 'T' : '-'));

				// Returns the rights in the file in 
				// the format -rw-r--r--.
				return $stats.' ('.decoct($perms & 0777).')';
			}

			// Only number
			return decoct($perms & 0777);
		}


		/**
		 *
		 * Gets the all files from the catalog/s.
		 *
		 * @return array|string
		 * @uses _checkNameMethod()
		 *
		 */
		public function files(){
			// Has the name method been called?
			$this->_checkNameMethod();
			$files = null;
			
			// Is this a path
			if (preg_match('/\.([a-z0-9]+)$/i', $this->path) != 1) {
				$rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->path));

				foreach ($rii as $file) {
					if ($file->isDir()) continue;
					$files[] = $file->getPathname(); 
				}

				return $files;
			} 
			
			// Is this file
			if (!empty($this->path)) $files = $this->path;
			return $files;
		}


		//------------------------------------------------------------------
        // SEARCH METHODS
		//------------------------------------------------------------------


		/**
		 *
		 * This trait searches for the phrase or number
		 * of line file given as the method parameter.
		 *
		 * @param mixed $search 	- Search phrase or number line
		 * @param bool $multi 		- Should it show all matching?
		 * @param bool $first 		- Should it display the first found?
		 * @param bool $search_line - Search number of line
		 * @param bool $line		- Return only number of lines
		 * @param bool $bool		- Return only bool
		 * 
		 * @return mixed
		 *
		 */
		private function __traitSearchMethods($search, bool $multi, bool $first = false, bool $search_line = false, bool $line = false, $bool = null){
			$bool = !is_null($bool) ? $bool : DISPLAY_AS_BOOL;

			// Has the name method been called?
			$this->_checkNameMethod();

			/* @call to string or int $search argument. */
			if (is_string($search) || is_int($search)) {
				$exists = false;
				
				// Gets the content of the file.
				$content = $this->content(true, false);
				
				// Searching for values
				if (!$search_line) {
					$found 	= [];
					$lines	= [];

					// Searches the indicated file.
					$i = 0;
					foreach ($content as $_line) {
						if (strpos($_line, $search) !== false) {
							$found[] = $_line;
							$lines[] = $i;

							$exists = true;
						}
						
						$i++;
					}

					// Returns the entire line(s) or error message.
					// Possibly returns boolean.
					if ($bool && !$multi) return $exists;
					else {
						if ($exists) {
							if (!$multi) {
								if (!$line) return $first ? $found[0] : $found[count($found) - 1];
								else return $first ? $lines[0] : $lines[count($lines - 1)];
							} else {
								if (!$bool) {
									if (!$line) return count($found) > 1 ? $found : $found[0];
									else return count($lines) > 1 ? $lines : $lines[0];
								}
								else return count($found) > 1 ? true : false;
							}
						} else return false;
					}
				} 
				
				// Line search
				else return array_key_exists($search, $content);
			}
		}


		/**
		 *
		 * This method returns bool or int type. 
		 * If the second argument is set, the method 
		 * returns true if it finds more than one phrase.
		 *
		 * @param string $phrase 	- Search phrase
		 * @param bool $line 		- Return only number of lines
		 * @param bool $multi 		- Should it show all matching?
		 * 
		 * 
		 * @return mixed
		 * @uses __traitSearchMethods()
		 *
		 */
		public function search(string $phrase, bool $line = false, bool $multi = false){
			return $this->__traitSearchMethods($phrase, $multi, true, false, $line, !$line);
		}

		/**
		 *
		 * Displays the last line in which the searched 
		 * phrase is located.
		 *
		 * @param string $phrase 	- Search phrase
		 * @param bool $line 		- Return only number of lines
		 * 
		 * @return mixed
		 * @uses __traitSearchMethods()
		 *
		 */
		public function searchLast(string $phrase, bool $line = false){
			return $this->__traitSearchMethods($phrase, false, false, false, $line);
		}


		/**
		 *
		 * Displays all lines in which the searched 
		 * phrase is located.
		 * 
		 * @param string $phrase 	- Search phrase
		 * @param bool $line 		- Return only number of lines
		 * 
		 * @return mixed
		 * @uses __traitSearchMethods()
		 *
		 */
		public function searchAll(string $phrase, bool $line = false){
			return $this->__traitSearchMethods($phrase, true, false, false, $line);
		}


		/**
		 *
		 * Checks whether the given line is in the file.
		 * 
		 * @param int $number_line - Search number line
		 * @return bool
		 * 
		 * @uses __traitSearchMethods()
		 *
		 */
		public function searchLine(int $number_line) : bool{
			return $this->__traitSearchMethods($number_line, false, false, true, false, true);
		}


		//------------------------------------------------------------------
        // DELETE METHODS
		//------------------------------------------------------------------


		/**
		 *
		 * Removes the specified file.
		 *
		 * @param string $path - Filename or path to file
		 * @return bool
		 *
		 */
		private function unlink(string $path) : bool{
			return unlink($this->path);
		}


		/**
		 *
		 * Removes the specified file or all file.
		 *
		 * @return bool
		 * @uses unlink(), _checkNameMethod(), _checkBeforeOpen()
		 *
		 */
		public function delete() : bool{
			// Has the name method been called?
			$this->_checkNameMethod(true);

			/* @call to array $_pathes var. */
			if (!empty($this->_pathes)) {
				foreach	($this->_pathes as $file) $this->unlink($path);
				return true;
			}

			/* @call to string $path var. */
			return $this->unlink($this->path);
		}


		/**
		 *
		 * Removes the specified line from file.
		 *
		 * @param mixed $search - Search phrase or line number
		 * @return bool
		 * 
		 * @uses _checkNameMethod(), content(), searchLine(), search(), newLine(), put()
		 *
		 */
		public function deleteLine($search) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			/* @call to int or string $search argument. */
			if (is_int($search) || is_string($search)) {
				$findLine 	= $this->search($search, true);
				$search	 	= is_int($search) ? $search >= 0 ? $search : $this->length() - abs($search) : $findLine;

				// Modification of the file.
				if (($this->searchLine($search) && is_int($search)) || ($findLine !== false && is_string($search))){
					$content = $this->content(true, false);
					unset($content[$search]);
					
				 	// Update content file
					$content = implode(PHP_EOL, $content);
					return $this->newLine(false)->put([$this->path => trim($content)]);
				}
				
				return false;
			}

			/* @error: Diffrent type $search arguement. */
			else throw new Exception('Incompatible type <b>'.gettype($search).'</b> of the <a href="https://github.com/sopskirk/apflow/File#deleteLine">File::deleteLine</a> method argument!');
		}


		/**
		 *
		 * Removes the indicated duplicates from 
		 * the file except the last one or all.
		 *
		 * @param string $search 	- Search phrase
		 * @param bool $all 		- Removes all identical
		 * 
		 * @return bool
		 * @uses _checkNameMethod(), content(), searchAll(), newLine(), put()
		 *
		 */
		public function deleteDuplicatesLine(string $search, bool $all = false) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			/* @call to string or int $search argument. */
			if (is_string($search)){
				$lines = $this->searchAll($search, true);

				if ($lines != false) {
					$content = $this->content(true, false);

					// Removes duplicates except the last one.
					if (!$all) unset($lines[count($lines) - 1]);
					else foreach ($lines as $line) unset($content[$line]);

					// Update content file
					$content = implode(PHP_EOL, $content);
					return $this->newLine(false)->put($this->path, trim($content));
				}

				return false;
			}

			/* @error: Diffrent type $search arguement. */
			else throw new Exception('Incompatible type <b>'.gettype($search).'</b> of the <a href="https://github.com/sopskirk/apflow/File#deleteDuplicatesLine">File::deleteDuplicatesLine</a> method argument!');
		}


		/**
		 *
		 * It deletes the entire contents of the file,
		 * but does not delete it.
		 *
		 * @return bool
		 * @uses _checkNameMethod(), _checkBeforeOpen()
		 *
		 */
		public function clear() : bool{
			// Has the name method been called?
			$this->_checkNameMethod(true);

			/* @call to array $_pathes var. */
			if (!empty($this->_pathes)) {
				foreach ($this->_pathes as $file) file_put_contents($file, '');
				return true;
			}

			/* @call to string $path var. */
			file_put_contents($this->path, '');
			return $this->empty();
		}


		//------------------------------------------------------------------
        // NAME METHOD
		//------------------------------------------------------------------


		/**
		 *
		 * The method used to refer to a specific file 
		 * or a specific group of files. With its help 
		 * you can get the contents of the file, 
		 * update it, delete it, etc.
		 *
		 * @param mixed $path 	- Filename or path to file
		 * @param bool $bool 	- Return only bool
		 * 
		 * @return object
		 *
		 */
		public function name($path, $bool = null) : object{
			$bool = !is_null($bool) ? $bool : DISPLAY_AS_BOOL;

			/* @call to string $path argument. */
			if (is_string($path)) {
				$this->path = $this->path($path);

				if (strpos($path, '*') !== false) {
					$path = $this->path.DS.basename($path);
					foreach (glob($path) as $file) $this->_pathes[] = $file;
				}
			}

			/* @call to array $path argument. */
			elseif (is_array($path)) {
				// Checks whether the file exists before operations.
				foreach ($this->_pathes as $file) $this->has($file, true);

				// Gets and sets filenames.
				foreach ($path as $file) $this->_pathes[] = $this->path($file, true);
			}

			/* @error: Diffrent type $path argument. */
			else throw new Exception('Incompatible type <b>'.gettype($path).'</b> of the <a href="https://github.com/sopskirk/apflow/File#name">File::name</a> method argument(s)!');
			return $this;
		}
	}