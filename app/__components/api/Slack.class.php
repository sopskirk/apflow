<?php
/**
 * 
 * This class is connects to Webhook Slack.
 * 
 * @package apflow
 * @subpackage APi
 * @version 1.0
 * 
 */


    namespace apflow\Components\API;

    if (SLACK_WEBHOOK_RUN == true) {
        class Slack{
            /**
             * 
             * This method loaded message for Weebhook Slack.
             * 
             * @param string $template  - Template name for Webhook Slack
             * @param array $data       - Data send
             * 
             * @return string
             * 
             */
            private function _createMessage(string $template, array $data) {
                include APP_PATH.DIRECTORY_SEPARATOR.'__templates'.DIRECTORY_SEPARATOR.'Slack'.DIRECTORY_SEPARATOR.$template.'.php';
                return [ 'payload' => json_encode($template($data)) ];
            }


            /**
             * 
             * This method send Weebhook Slack.
             * 
             * @param string $webhook   - Webhook URL Slack
             * @param string $template  - Template name for Webhook Slack
             * @param array $data       - Data send
             * @return string
             * 
             */
            public function send(string $webhook, string $template, array $data){
                $c = curl_init($webhook);
                
                curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($c, CURLOPT_POST, true);
                curl_setopt($c, CURLOPT_POSTFIELDS, $this->_createMessage($template, $data));
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_exec($c);
                curl_close($c);
            }
        }
    }