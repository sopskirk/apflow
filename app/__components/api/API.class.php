<?php
/**
 * 
 * This class is used to handle everything 
 * that is related to the API classes:
 * - Webhook Slack
 * 
 * @package apflow
 * @subpackage APi
 * @version 1.0
 * 
 */


    namespace apflow\Components\API;
    use Exception;

    class API {
        //------------------------------------------------------------------
        // CALLS METHODS
        //------------------------------------------------------------------
        

        /**
         * 
         * Calls the Slack{} class methods.
         * 
         * @param array $data       - Data send
         * @param string $webhook   - Webhook URL Slack
         * @param string $template  - Template name for Webhook Slack
         *
         * @return mixed
         * 
         */
        public function Slack(array $data = [], string $webhook = SLACK_WEBHOOK_URL, string $template = SLACK_WEBHOOK_TEMPLATE){
            if (SLACK_WEBHOOK_RUN == true) return (new Slack)->send($webhook, $template, $data);
            else throw new Exception('To use the <b>Slack</b> class, set the <q>SLACK_WEBHOOK_SQL</q> constant to <b>true</b>.');
        }


        /**
         * 
         * Calls the MailGun{} class methods.
         * 
         * @param array $options - Options to MainGun API
         * @return mixed
         * 
         */
        public function MailGun($options = []){
            return true;
        }
    }