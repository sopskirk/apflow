<?php
/**
 *
 * Central cookie control, delete, create and get.
 *
 * @package apflow
 * @subpackage HTTP
 * @version 1.0
 *
 */


	namespace apflow\Components\Http;
	use apflow\Bootstrap;
	use Exception;

	class Cookie extends Bootstrap{
		/**
		 *
		 * Did you call the save method?
		 *
		 * @var bool
		 *
		 */
		private $_save = false;

		/**
		 * 
		 * Stores the cookie(s) name(s).
		 * 
		 * @var mixed
		 * 
		 * 
		 */
		private $_name;
		private $_names;

		/**
         * 
         * Stores the validation 
         * results of all cookies.
         * 
         * @var mixed
         * 
         */
		private $_validation = [];
		public $valid;
		
		/**
		 * 
		 * Stores paths to cookie storage files.
		 * 
		 * @var string
		 * 
		 */
		private const STORAGE_FILE = APP_PATH.'/__storage/cookie/cookie.storage';
		private const STORAGE_VISITORS_FILE = APP_PATH.'/__storage/cookie/visitors.storage';


		//------------------------------------------------------------------
        // CONFIG METHOD
        //------------------------------------------------------------------


		/**
		 *
         * Gets the settings from the config file.
		 * 
		 * @return array
		 *
         */
		private function _cookieConfig() : array{
			return include APP_PATH.DIRECTORY_SEPARATOR.'__config'.DIRECTORY_SEPARATOR.'config.cookie.php';
		}


		//------------------------------------------------------------------
        // CHECK METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Checks whether the method has not
		 * been called (use).
         * 
		 * @return void
		 *
		 */
		private function _checkNameMethod() : void{
			if (empty($this->_name)) throw new Exception('The <a href="https://github.com/sopskirk/apflow/Cookie#name">Cookie::name</a> method must be called for proper operation this method!');
		}


		/**
		 *
         * Checks whether a cookie file exists.
		 *
		 * @param mixed $name 	- Cookie(s) name(s)
		 * @param bool $error 	- Display error
		 * @param bool $bool 	- Return only bool
         * 
		 * @return mixed
		 *
         */
		public function has($name, bool $error = false, bool $bool = false){
			/* @call to string $name argument. */
			if (is_string($name)) {
				$exists = isset($_COOKIE[$name]);
				if (!$error) return $exists;

				/* @error: Cookie name does not exists. */
				elseif (!$exists) throw new Exception('The cookie about name <q>'.$name.'</q> does not exist!');
				return true;
			}

			/* @call to array $name argument. */
			elseif (is_array($name)) {
				if (!$error) {
					$cookies = [];
					
					foreach ($name as $_name) {
						$exists = isset($_COOKIE[$_name]);

						if (!$bool) $cookies[$_name] = $exists;
						else if (!$exists) return false;
					}
					
					return !$bool ? $cookies : true;
				} else throw new Exception('If the first argument of the <a href="https://github.com/sopskirk/apflow/Cookie#has">Cookie::has</a> method has been called as an <u>array</u>, then the second argument <span>can not be</span> <b>false</b>');
			}

			/* @error: Diffrent type $name argument. */
			else throw new Exception('Incompatible type <b>'.gettype($name).'</b> of the <a href="https://github.com/sopskirk/apflow/Cookie#has">Cookie::has</a> method argument(s)!');
		}


		//------------------------------------------------------------------
        // VALID METHODS
        //------------------------------------------------------------------


		/**
		 *
         * Checks the entered cookie name.
		 *
		 * @param string $name - Cookie name
		 * @return void
		 *
         */
		private function _validName(string $name) : void{
			if (!preg_match('/^[a-z0-9_-]+$/i', $name) || preg_match('/[=,; \\t\\r\\n\\013\\014]/', $name)) 
				throw new Exception('Incorrect cookie name: <q>'.$name.'</q>!');
		}


		/**
		 *
         * Checks the entered cookie expire.
		 *
		 * @param string $expire - Verbal time
		 * @return void
		 *
         */
		private function _validExpire(string $expire) : void{
			if (!preg_match('/^[0-9 ]*(second|seconds|minute|minutes|hour|hours|day|days|month|months|year|years)$/', $expire)) 
				throw new Exception('Incorrect cookie expire time: <q>'.$expire.'</q>!');
		}
		 
		
		//------------------------------------------------------------------
        // CREATE METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Creates a specific one cookie file.
		 *
		 * @param string $name 			- Cookie name
		 * @param string $value 		- Cookie value
		 * @param string $expire		- Cookie expire
		 * @param string $httponly		- Is httponly?
		 * @param string $custom_date	- Custom date (other than current)
		 * @param bool $checked			- Have the data been checked yet?
		 * 
		 * @return bool
		 * @uses _validName(), _validExpire(), _cookieConfig()
		 *
		 */
		private function _build(string $name, string $value = 'hello world', ?string $expire = null, ?bool $httponly = null, ?string $custom_date = null, bool $checked = false) : bool{
			// Validation cookie data
			if (!$checked) $this->_validName($name);

			// Cookie config
			$config = $this->_cookieConfig();
			$timestamp = 0;

			// Validation cookie expire
			if (!is_null($expire)) {
				if (!$checked) $this->_validExpire($expire);
				$timestamp = $this->load('convert')->toTimestamp($expire, $custom_date);
			}

			// Custom settings httponly
			$httponly = !is_bool($httponly) ? $config['HTTPONLY'] : $httponly;

			// Did you call the save method?
			if ($this->_save) {
				$file = $this->load('file');

				// The date when the cookie was created.
				$created = !empty($custom_date) ? $custom_date : date('Y:m:d H:i:s');

				// It adds the cookie file name to the store.
				// If it has not been added before.
				$storage = $file->name(self::STORAGE_FILE);
				if (!$storage->search($name)) $storage->append($name);

				// Gets the cookie information,
				$cookie = '['.$this->load('user')->ip().'] [NAME: '.$name.'] [VALUE: '.$value.'] [EXPIRE: '.$expire.'] [HTTPONLY: '.(bool)$httponly.'] [CREATED: '.$created.'] [STORAGE: true]';
				
				// Adds cookie data to the storage file.
				if ($file->has(self::STORAGE_VISITORS_FILE)) {
					$visitors = $file->name(self::STORAGE_VISITORS_FILE);
					if (!$visitors->search($name)) file_put_contents(self::STORAGE_VISITORS_FILE, $cookie.PHP_EOL, FILE_APPEND);
				}

				// Create a visitors storage file.
				else file_put_contents(self::STORAGE_VISITORS_FILE, $cookie.PHP_EOL, FILE_APPEND);
			}

			// Build cookie file
			return setcookie($name, $value, $timestamp, $config['PATH'], $config['DOMAIN'], $config['SECURE'], $httponly);
		}


		/**
		 * 
		 * Create a new cookie(s).
		 *
		 * @param mixed $data - Data of the created cookie(s)
		 * @return bool
		 * 
		 * @uses _build(), _cookieConfig()
		 *
		 */
		public function create($data) : bool{
			/* @call to string $data argument. */
			if (is_string($data)) {
				if ($this->has($data)) throw new Exception('The cookie about name <q>'.$data.'</q> you want to create already exists! Cookies <span>will not be</span> created until you correct the problem!');
				return $this->_build($data);
			}

			/* @call to array $data argument. */
			elseif (is_array($data)) {
				foreach ($data as $name => $_data) {
					$_name = is_int($name) ? $_data : $name;
					if ($this->has($_name)) throw new Exception('The cookie about name <q>'.$_name.'</q> you want to create already exists! Cookies <span>will not be</span> created until you correct the problem!');
				}

				$created = false;
				foreach ($data as $name => $_data){
					/* @call to string $name var. */
					if (is_string($name)) {
						/* @call to string $_data var. */
						if (is_string($_data)) return $this->_build($name, 'hello world', $_data);

						/* @call to array $_data var. */
						elseif (is_array($_data)) {
							$value 		= $_data[0] ?? 'hello world!';
							$expire		= $_data[1] ?? null;
							$httponly	= $_data[2] ?? null;

							$created = $this->_build($name, $value, $expire, $httponly);
						}

						// When it will not be created.
						if (!$created) return false;
					} 
					
					/* @call to int $name var. */
					else $created = $this->_build($_data);

					// When it will not be created.
					if (!$created) return false;
				}

				return true;
			}

			/* @error diffrent type $data argument. */
			else throw new Exception('Incompatible type <b>'.gettype($data).'</b> of the <a href="https://github.com/sopskirk/apflow/Cookie#create">Cookie::create</a> method argument!');
		}


		//------------------------------------------------------------------
        // GET METHODS
		//------------------------------------------------------------------
		

		/**
		 * 
		 * Gets the specific cookie value.
		 * 
		 * @param string $value - Cookie value
		 * @return mixed
		 * 
		 */
		private function _value(string $value){
			if (strpos($value, '.') !== false) {
				$values = [];

				if (preg_match_all('[(\w+).(\w+)]', $value, $matches)) {
					array_shift($matches);

					$values = $this->load('security')->filter($matches[1], true);
					$values = array_combine($matches[0], $values);
				}

				return $values;
			}

			return $this->load('security')->filter($value, true);
		}


		/**
		 *
		 * An auxiliary method for the get method.
		 *
		 * @param string $name 	- Cookie name
		 * @param string $key 	- The key from which we get data
		 * 
		 * @return mixed
		 * @uses _value()
		 *
		 */
		private function _get(string $name, string $key){
			if (strpos('expire countdown httponly created', $key) !== false) {
				[$name, $value, $expire, $httponly, $created] = $this->storage()->name($name)->get();

				switch ($key) {
					case 'value' 		: return $this->_value($value);
					case 'expire' 		: return $expire;
					case 'countdown' 	: 
						$end = $this->load('convert')->toTimestamp($expire, $created);
						return $this->load('date')->countdown(date('Y:m:d H:i:s', $end));
					case 'httponly'	: return $httponly;
					case 'created'	: return $created;
				}
			} else {
				if ($key == 'value') return $this->_value($_COOKIE[$name]);
				elseif ($key == 'valid') return $this->_validation[$name] ?? $this->_valid;
			}
		}


		/**
		 * 
		 * Display of cookie data:
		 * - value,
		 * - expire,
		 * - countdown,
		 * - httponly,
		 * - created,
		 * - valid
		 * 
		 * @param string $key - The key from which we get data
		 * @return mixed
		 * 
		 * @uses _checkNameMethod(), _get()
		 * 
		 */
		public function get(string $key){
			// Has the name method been called?
			$this->_checkNameMethod();

			// Returns cookie data.
			return $this->_get($this->_name, $key);
		}


		//------------------------------------------------------------------
        // UPDATE METHOD
		//------------------------------------------------------------------ 


		/**
		 * 
		 * Updates selected cookie stirage data:
		 * - name,
		 * - value,
		 * - expire,
		 * - httponly
		 *
		 * @param array $data - Cookie update data
		 * @return mixed
		 * 
		 * @uses _build(), save(), _checkNameMethod()
		 *
		 */
		public function update(array $data){
			// Has the name method been called?
			$this->_checkNameMethod();

			// Gets old data for the cookie storage.
			[$name, $value, $expire, $httponly, $created] = $this->storage()->name($this->_name)->get();

			// Gets new data for the cookie.
			foreach ($data as $key => $new_data) {
				if (match('/(name|value|expire|httponly)/', $key)) {
					$$key = $new_data;

					// Validation cookie data.
					if ($key == 'name') $this->_validName($name);

					// Validation cookie expire.
					if ($key == 'expire') $this->_validExpire($expire);
				} else return false;
			}

			// Deletes cookie
			$this->delete();

			// Update data cookie
			$exists = $this->save()->_build($name, $value, $expire, $httponly, $created, true);
			return $exists;
		}


		//------------------------------------------------------------------
        // DELETE METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Deletes a specific cookie.
		 *
		 * @param string $name 	- Cookie name
		 * @return bool
		 * 
		 * @uses _cookieConfig()
		 *
		 */
		private function _delete(string $name) : bool{			
			// Deletes cookie data in storage.
			$this->storage()->name($name, false)->delete();

			// Deletes the cookie.
			$config = $this->_cookieConfig();
			return setcookie($name, null, $this->load('convert')->toTimestamp('-1 day'), $config['PATH'], $config['DOMAIN'], $config['SECURE'], $config['HTTPONLY']);
		}


		/**
		 *
		 * It removes all existing cookies in addition
		 * to the session cookie.
		 *
		 * @param bool $session - Remove the session cookie?
		 * @return bool
		 * 
		 * @uses _delete()
		 *
		 */
		private function _deleteAll(bool $session = false) : bool{
			if ($_COOKIE) {
				foreach ($_COOKIE as $name) {
					if ((session_name() != $name && !$session) || $session) $this->_delete($name);
				}

				return true;
			}

			return false;
		}


		/**
		 *
		 * Deletes a specific cookie or all existing
		 * cookies except session cookies.
		 *
		 * @param bool $session - Remove the session cookie?
		 * @return bool
		 * 
		 * @uses _delete(), _deleteAll(), _checkNameMethod()
		 *
		 */
		public function delete(bool $session = false) : bool{
			// Has the name method been called?
			$this->_checkNameMethod();

			/* @call to string $_name var. */
			if (!empty($this->_name)) return $this->_delete($this->_name);
			else {
				if (empty($this->_names)) return $this->_deleteAll($session);

				/* @call to array $_names var. */
				else {
					$deleted = false;
					foreach ($this->_names as $name) {
						$deleted = $this->_delete($name, false);
						if (!$deleted) return false;
					}

					return true;
				}
			}

			return false;
		}


		//------------------------------------------------------------------
        // NAME METHOD
		//------------------------------------------------------------------


		/**
		 *
		 * A method used to refer to a specific or a certain
		 * group of cookies. With its help you can gets cookie
		 * data, update it and delete it.
		 *
		 * @param string $names - Cookie(s) name(s)
		 * @param bool $error 	- Display error
		 * 
		 * @return object
		 * @uses has()
		 *
		 */
		public function name($names, bool $error = false) : object{
			/* @call to string $names argument. */
			if (is_string($names)) {
				$this->has($names, $error);
				$this->_name = $names;
			}

			/* @call to array $names argument. */
			elseif (is_array($names)) {
				// Checks whether the cookie name exists before operation.
				foreach ($this->_names as $name) $this->has($name, $error);
				
				// Gets and sets cookie names.
				foreach ($this->_names as $name) $this->_names[] = $name;
			}

			/* @error diffrent type $data argument. */
			else throw new Exception('Incompatible type <b>'.gettype($names).'</b> of the <a href="https://github.com/sopskirk/apflow/Cookie#name">Cookie::name</a> method argument!');
			return $this;
		}


		/**
         * 
         * Returns the validated data of
         * a specific cookie.
         * 
         * @param mixed $data       - A set of patterns or a collective pattern
		 * @param bool $all 		- Validation for all cookie values
         * @param string $default   - Collective pattern
         *
         * @return void
         * @uses _data()
         * 
         */
        public function validation($data = 'http', bool $all = false, string $default = 'http') : void{
			$cookie = $_COOKIE;
			unset($cookie[session_name()]);

			if (isset($cookie)) {
				$validator = $this->load('security');

				/* @call to string $data argument. */
				if (is_string($data)) {
					foreach ($cookie as $name => $value) {
						$validation 	= $validator->validation($value, $data);
						$this->valid 	= $validation;

						if (!$validation) return;
					}
				}

				/* @call to array $data argument. */
				elseif (is_array($data)) {
					foreach ($data as $name => $pattern) {
						if (array_key_exists($name, $cookie)) {
							$value = $this->_get($name, 'value');
							$this->_validation[$name] = $validator->validation($value, $pattern);
						}
					}
					
					// When the cookie name was not provided in this method.
					if (count($cookie) > count($this->_validation)) {
						foreach ($cookie as $name => $value) {
							if (array_key_exists($name, $this->_validation) === false) {
								$value = $this->_get($name, 'value');
								$this->_validation[$name] = $validator->validation($value, $default);
							}
						}
					}

					// The overall result of validation for cookie values.
					if ($all) {
						foreach ($this->_validation as $name => $valid) {
							$this->valid = $valid;
							if (!$valid) return;
						}
					}
				}

				/* @error: Diffrent type of argument. */
				else throw new Exception('Incompatible type </b>'.gettype($data).'<b> of the <a href="https://github.com/sopskirk/apflow/Cookie#validation">Cookie::validation</a> method argument!');
			}
		}


		//------------------------------------------------------------------
        // PREVIEW METHOD
		//------------------------------------------------------------------


		/**
		 * 
		 * Displays a preview of all active cookies.
		 * 
		 * @return array
		 * 
		 */
		public function preview() : array{
			return $this->storage()->preview();
		}


		//------------------------------------------------------------------
        // STORAGE METHODS
		//------------------------------------------------------------------


		/**
		 * 
		 * If the CookieStorage function is turned off, 
		 * it returns an exception.
		 * 
		 * @return void
		 * 
		 */
		private function _isOnCookieStorage(){
			if (LOAD_COOKIE_STORAGE == false) throw new Exception('To use the <b>CookieStorage</b> class, set the <q>LOAD_COOKIE_STORAGE</q> constant to <b>true</b>.');
		}
		

		/**
         * 
         * Calls the CookieStorage{} class methods.
         * 
         * @return object
         * 
         */
        public function storage() : object{
			$this->_isOnCookieStorage();
			return new CookieStorage;
        }


		/**
         *
         * Saves the cookie file in the storage.
		 * 
         * @return object
 		 *
         */
		public function save() : object{
			$this->_isOnCookieStorage();

			$this->_save = true;
			return $this;
		}
	}