<?php
/**
 *
 * Abstract class for storing the cookie files.
 * Part of the Cookie class.
 *
 * @package apflow
 * @subpackage HTTP
 * @source 1.0
 *
 */


	namespace apflow\Components\Http;
	use apflow\Bootstrap;
	use Exception;
	
	class CookieStorage extends Bootstrap{
		/**
		 * 
		 * It stores the name
		 * of cookie storage.
		 * 
		 * @var mixed
		 * 
		 */
		private $_name;
		
		/**
		 * 
		 * Stores paths to cookie storage files.
		 * 
		 * @var string
		 * 
		 */
		private const STORAGE_FILE = 'app/__storage/cookie/cookie.storage';
		private const STORAGE_VISITORS_FILE = 'app/__storage/cookie/visitors.storage';


		//------------------------------------------------------------------
        // CHECK METHODS
		//------------------------------------------------------------------
		

		/**
		 *
		 * Checks whether the method has not
		 * been called (name).
         * 
		 * @return void
		 *
		 */
		private function _checkNameMethod() : void{
			if (empty($this->_name)) throw new Exception('The <a href="https://github.com/sopskirk/apflow/CookieStorage#name">CookieStorage::name</a> method must be called for proper operation this method!');
		}


		/**
		 *
		 * Checks whether the cookie file 
		 * has been saved to storage.
		 *
		 * @param mixed $name 	- Cookie(s) name(s)
		 * @param bool $error 	- Display error
		 * @param bool $bool 	- Return only bool
         * 
		 * @return mixed
		 *
		 */
		public function has($name, bool $error = true, bool $bool = false){
			$file = $this->load('file')->name(self::STORAGE_VISITORS_FILE);

			/* @call to string $name argument. */
			if (is_string($name)) {
				$exists = $file->search('[NAME: '.$name.']');
				if (!$error) return $exists;

				/* @error: Cookie storage name does not exists. */
				elseif (!$exists) throw new Exception('The cookie storage about name <q>'.$name.'</q> does not exist!');
				return true;
			}

			/* @call to array $name argument. */
			elseif (is_array($name)) {
				if (!$error) {
					$cookies = [];
					
					foreach ($name as $_name) {
						$exists = $file->search('[NAME: '.$_name.']');

						if (!$bool) $cookies[$_name] = $exists;
						else if (!$exists) return false;
					}
					
					return !$bool ? $cookies : true;
				} else throw new Exception('If the first argument of the <a href="https://github.com/sopskirk/apflow/CookieStorage#has">CookieStorage::has</a> method has been called as an <uarray</u>, then the second argument <span>can not be</span> <b>false</b>');
			}

			/* @error: Diffrent type $name argument. */
			else throw new Exception('Incompatible type <b>'.gettype($name).'</b> of the <a href="https://github.com/sopskirk/apflow/CookieStorage#has">CookieStorage::has</a> method argument(s)!');
		}


		//------------------------------------------------------------------
        // GET METHOD
        //------------------------------------------------------------------


		/**
		 *
		 * Gets the list of cookies data that 
		 * have been saved to storage.
		 *
		 * @param bool $all - All cookies storage
		 * @return array
		 *
		 */
		private function _get(bool $all = false) : array{
			$cookies = [];
			$content = $this->load('file')->extensions('storage')->name(self::STORAGE_VISITORS_FILE)->content();
			
			if (preg_match_all('~\G\s*\[(\d+)]|\s*\[([A-Z]+):\s*(.*?)]~', $content, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $item) {
					if (!isset($item[2])) $id = $item[1];
					else $cookies[$id][$item[2]][] = $item[3];
				}
			}
	
			return !$all ? $cookies[$this->load('user')->ip()] ?? [] : $cookies;
		}


		//------------------------------------------------------------------
        // STORAGE METHODS
		//------------------------------------------------------------------
		

		/**
		 * 
		 * Gets data about a specific cookie.
		 * 
		 * @param bool $all - All cookies storage
		 * @return array
		 * 
		 * @uses has(), _get(), _checkNameMethod()
		 * 
		 */
		public function get(bool $all = false) : array{
			// Has the use method been called?
			$this->_checkNameMethod();
			$cookies = [];
			
			$cookie_data = $this->_get($all);
			$pos = array_search($this->_name, $cookie_data['NAME']);

			foreach ($cookie_data as $key => $val) $cookies[] = $cookie_data[$key][$pos];
			return $cookies;
		}


		/**
		 * 
		 * Removes a specific cookie file 
		 * from the storage file.
		 * 
		 * @return bool
		 * @uses has(), _checkNameMethod()
		 * 
		 */
		public function delete() : bool{
			// Has the use method been called?
			$this->_checkNameMethod();

			if ($this->has($this->_name, false)) return $this->load('file')->name(self::STORAGE_VISITORS_FILE)->deleteLine('[NAME: '.$this->_name.']');
			return false;
		}


		/**
		 * 
		 * Displays a preview of all active cookies.
		 * 
		 * @return array
		 * @uses _get(), has()
		 * 
		 */
		public function preview() : array{
			$cookies = [];
			$cookie_data = $this->_get();

			if (!empty($cookie_data)) {
				$cookie_names = $cookie_data['NAME'];

				// Preview of a storage cookie.
				foreach ($cookie_data as $key => $value) {
					foreach ($cookie_data[$key] as $k => $v) $cookies['COOKIE_'.$cookie_names[$k]][$key] = $value[$k];
				}
			}

			// Preview of a standard cookie.
			foreach ($_COOKIE as $name => $value) {
				$cookies['COOKIE_'.$name]['NAME'] 		= $name;
				$cookies['COOKIE_'.$name]['VALUE'] 		= $value;
				$cookies['COOKIE_'.$name]['STORAGE']	= false;
			}

			return $cookies;
		}


		/**
		 *
		 * Clears the data file of cookie 
		 * in storage file.
		 *
		 * @param bool $all - It also cleans the main storage file
		 * @return bool
		 * 
		 */
		public function clear(bool $all = false) : bool{
			$file 	= $this->load('file');
			$cookie = $this->load('cookie');

			$cookies = $_COOKIE;
			unset($cookies[session_name()]);
			
			if (!empty($cookies)) {
				// Get names cookie storage.
				$data = $file->name(self::STORAGE_FILE)->content(true, false);

				// Removes all cookie storage.
				if (!empty($data)) {
					foreach ($data as $name) $cookie->name($name, false)->delete();
				}
			}

			// Cleans all cookie storage files.
			if ($all) {
				$all = $file->name([self::STORAGE_FILE, self::STORAGE_VISITORS_FILE]);
				$all->clear();

				return $all->empty(true);
			} 
			
			// Cleans only the storage file with visits.
			else return $file->name(self::STORAGE_VISITORS_FILE)->clear();
		}


		//------------------------------------------------------------------
        // CONTROLLER METHOD
		//------------------------------------------------------------------


		/**
		 * 
		 * Control cookie storage.
		 * 
		 * @return bool
		 * @uses _data(), _names(), delete()
		 * 
		 */
		public function controller() : bool{
			$active = false;
			$exists = false;

			$cookies = $_COOKIE;
			unset($cookies[session_name()]);

			$file 	= $this->load('file')->name(self::STORAGE_FILE);
			$cookie = $this->load('cookie');

			// Get names cookie storage.
			$data = $file->content(true, false);

			// Conducting an inspection.
			if (!empty($data) && !empty($cookies)) {
				foreach ($data as $name) {
					$active = array_key_exists($name, $_COOKIE);
					$exists = $file->search($name);

					if (!$active || !$exists) {
						if ($active) $cookie->name($name)->delete();
						$this->name($name)->delete();
					}
				}
			}

			return $active && $exists;
		}


		//------------------------------------------------------------------
        // NAME METHOD
		//------------------------------------------------------------------


		/**
		 *
		 * A method used to refer to a specific or a certain
		 * group of cookie storage. With its help you can gets cookie storage
		 * and delete it.
		 *
		 * @param string $name	- Cookie name
		 * @param bool $error 	- Display error
		 * @param bool $bool 	- Return only bool
		 * 
		 * @return object
		 * @uses has()
		 *
		 */
		public function name(string $name, bool $error = true, bool $bool = false) : object{
			// Checks whether a cookie exists.
			$this->has($name, $error, $bool);

			// Add session id to var.
			$this->_name = $name;
			return $this;
		}
	}