<?php
/**
 *
 * Central session control, delete, create and get.
 *
 * @package apflow
 * @subpackage HTTP
 * @version 1.0
 *
 */


	namespace apflow\Components\Http;
	use apflow\Bootstrap;
	use Exception;

	class Session extends Bootstrap{
		/**
		 * 
		 * It store(s) the id(s) 
		 * of session(s) and group name.
		 * 
		 * @var mixed
		 * 
		 */
		private $_group = [];
		private $_temp_group;
		public $group_name;

		/**
		 * 
		 * Stores the session(s) ID(s).
		 * 
		 * @var mixed
		 * 
		 */
		public $id;
		private $_ids = [];

		/**
         * 
         * Stores the data or exists of
         * a specific session,
         * 
         * @var mixed
         * 
         */
		public $data;
		public $countdown;

		/**
         * 
         * It stores the validation results 
         * of a specific session data.
         * 
         * @var bool
         * 
         */
		public $valid = false;

		/**
         * 
         * Stores the validation 
         * results of all sessions data.
         * 
         * @var array
         * 
         */
		private $_validation = [];

		/**
		 * 
		 * Stores information that a flash 
		 * Session is being created.
		 * 
		 * @var false
		 * 
		 */
		private $_flash = false;

		/**
		 * 
		 * Stores config
		 * 
		 * @var array
		 * 
		 */
		private $_config = [];


		//------------------------------------------------------------------
        // CONFIG METHOD
		//------------------------------------------------------------------
		

		/**
		 *
         * Gets the settings from the config file.
		 * 
		 * @return void
		 *
         */
		public function __construct(){
			$this->_config = require APP_PATH.DIRECTORY_SEPARATOR.'__config'.DIRECTORY_SEPARATOR.'config.session.php';
		}


		//------------------------------------------------------------------
        // CONTROLLER METHOD
		//------------------------------------------------------------------
		


		/**
		 * 
		 * Checks the expire that a specific
		 * session has left.
		 * 
		 * @return void
		 * 
		 */
		private function _controller() : void{
			// Skip the expire session.
			$sessions = $_SESSION;
			if (isset($sessions['expire'])) unset($sessions['expire']);

			// Remove all active sessions.
			else if (count($sessions) > 0) {
				$this->clear();
				return;
			}

			// Performing session control.
			foreach ($sessions as $group => $sessions) {
				if (is_array($sessions)) {
					foreach ($sessions as $id => $data) {
						if ($_SESSION['expire'][$group.'.'.$id] < time()) {
							unset($_SESSION['expire'][$group.'.'.$id]);
							unset($_SESSION[$group][$id]);
							
							// Removal of an empty session 
							// group and session expire.
							if (count($_SESSION[$group]) == 0) {
								unset($_SESSION['expire'][$group.'.'.$id]);
								unset($_SESSION[$group]);
							}
						}
					}
				}
			}

			// Removes all session expires.
			if (isset($_SESSION['expire'])) {
				if (count($_SESSION['expire']) == 0) unset($_SESSION['expire']);
			}
		}


		/**
		 *
		 * Set basic session settings.
		 *
		 * @param bool $regenerate - Off session_regenerate_id
		 * @return void
		 * 
		 * @uses _controller()
		 *
		 */
		public function start(bool $regenerate = true){
			// Set session name
			session_name($this->config['SESSION_NAME']);

			// Sets the session cookie params.
			session_set_cookie_params($this->config['SESSION_LIFETIME'], $this->config['SESSION_PATH'], $this->config['SESSION_DOMAIN'], $this->config['SESSION_SECURE'], $this->config['SESSION_HTTPONLY']);

			// Session start
			if (session_status() == PHP_SESSION_NONE) session_start();

			// Session regenerate id
			if ($regenerate) session_regenerate_id();

			// Checks the expire session.
			if (isset($_SESSION)) $this->_controller();
		}


		//------------------------------------------------------------------
        // CHECK METHODS
		//------------------------------------------------------------------


		/**
		 * 
		 * Returns the name of the default group 
		 * in which the session is located.
		 * 
		 * @param string $session - Session ID or session + group
		 * @return string
		 * 
		 */
		private function _whatDefaultGroup(string $session) : string{
			$pos = strpos($session, '.');

			if ($pos !== false) return substr($session, 0, $pos);
			else {
				$groups = [ $this->config['SESSION_FLASH_GROUP'], $this->config['SESSION_GROUP'] ];

				foreach ($groups as $group) {
					if ($this->has($group.'.'.$session)) return $group;
				}
			}

			// He is not in the group.
			throw new Exception('The session about ID '.$session.' is not in one of the default session groups.');
		}


		/**
		 *
		 * Checks whether the method has not
		 * been called (group).
         * 
		 * @return bool
		 *
		 */
		private function _checkGroupMethod() : void{
			if (empty($this->group_name)) throw new Exception('The <a href="https://github.com/sopskirk/apflow/Session#group">Session::group</a> method must be called for proper operation this method!');
		}


		/**
		 * 
		 * Retrieves all sessions inside 
		 * groups and returns them without them.
		 * 
		 * @param string $session_id - Session(s) ID(s)
		 * @return bool
		 * 
		 */
		private function _sessionExists(string $session_id) : bool{
			if (isset($_SESSION)) {
				// Obtaining the name in which the ID 
				// of the given session is located.
				$this->_temp_group = strpos($session_id, '.') === false ? empty($this->group_name) ? $this->config['SESSION_GROUP'].'.' : $this->group_name.'.' : null;

				// Skip the expire session.
				$sessions = $_SESSION;
				unset($sessions['expire']);

				// search for the given session ID.
				foreach ($sessions as $group => $data) {
					foreach ($data as $id => $value) {
						$session = !empty($this->_temp_group) ? $this->_temp_group.$id : $session_id;
						if ($group.'.'.$id == $session) return true;
					}
				}
			}

			return false;
		}
		

		/**
		 *
		 * Checks whether a session exists.
		 * 
		 * @param mixed $session_id 	- Session(s) ID(s)
		 * @param bool $error 			- Display error
		 * @param bool $bool 			- Return only bool
		 *
		 * @return mixed
		 * @uses _sessionExists()
		 *
		 */
		public function has($session_id, bool $error = false, bool $bool = false){
			/* @call to string $session_id argument. */
			if (!empty($this->_group) || is_string($session_id)) {
				$exists = $this->_sessionExists($session_id);
				if (!$error) return $exists;
				
				/* @error: Session ID does not exists. */
				else if (!$exists) throw new Exception('Session with an <q>'.$this->_temp_group.$session_id.'</q> ID does not exist!');
				return true;
			}

			/* @call to array $session_id argument. */
			elseif (is_array($session_id)) {
				if (!$error) {
					$sessions = [];

					foreach ($session_id as $id) {
						$exists = $this->_sessionExists($id);

						if (!$bool) $sessions[$id] = !$exists;
						else if (!$exists) return false;
					}

					return !$bool ? $sessions : true;
				} else throw new Exception('If the first argument of the <a href="https://github.com/sopskirk/apflow/Sessions#has">Session::has</a> method has been called as an <uarray</u>, then the second argument <span>can not be</span> <b>true</b>');
			}

			/* @error: Diffrent type $session_id argument. */
			else throw new Exception('Incompatible type <b>'.gettype($session_id).'</b> of the <a href="https://github.com/sopskirk/apflow/Session#has">Session::has</a> method argument(s)!');
		}


		//------------------------------------------------------------------
        // VALID METHODS
        //------------------------------------------------------------------


		/**
		 *
         * Checks the entered session id.
		 *
		 * @param string $session_id - Session ID
		 * @return void
		 *
         */
		private function _validID(string $session_id) : void{
			if (!preg_match('/^[a-z0-9-_]{1,128}$/i', $session_id)) throw new Exception('Incorrect session ID: <q>'.$session_id.'</q>!');
		}


		/**
		 *
         * Checks the entered session life time.
		 *
		 * @param string $expire - Verbal time
		 * @return void
		 *
         */
		private function _validExpire(string $expire) : void{
			if (!preg_match('/^[0-9 ]*(second|seconds|minute|minutes|hour|hours|day|days|month|months|year|years)$/', $expire)) throw new Exception('Incorrect session life time: <q>'.$expire.'</q>!');
		}


		//------------------------------------------------------------------
        // CREATE METHODS
		//------------------------------------------------------------------


		/**
		 * 
		 * Creates a specific one session.
		 * 
		 * @param string $session_id 	- Session ID
		 * @param string $data 			- Session DATA
		 * @param string $group 		- The name of the group in which the session ID is located
		 * @param string $expire		- Session life time
		 * 
		 * @return bool
		 * @uses _validID(), _validExpire(), has()
		 * 
		 */
		private function _build(string $session_id, string $data = 'hello world', ?string $group = null, ?string $expire = null) : bool{
			// Session group
			if (is_null($group)) $group = $this->_flash ? $this->config['SESSION_FLASH_GROUP'] : $this->config['SESSION_GROUP'];
			$session = $group.'.'.$session_id;

			//Checks whether the file exists before it created.
			if ($this->has($session)) throw new Exception('The session about ID <q>'.$session.'</q> you want to create already exists! Session <span>will not be</span> created until you correct the problem!');

			// Validation session id
			$this->_validID($session_id);

			// Create a new session expire.
			$convert = $this->load('convert');

			if (!is_null($expire)) {
				$this->_validExpire($expire);
				$_SESSION['expire'][$session] = $convert->toTimestamp($expire);
			} else {
				$expire = $this->_flash ? $this->config['SESSION_FLASH_EXPIRE'] : $this->config['SESSION_EXPIRE'];
				$_SESSION['expire'][$session] = $convert->toTimestamp($expire);
			}
			
			// Create a new session.
			$_SESSION[$group][$session_id] = $data;
			return $this->has($session);
		}


		/**
		 * 
		 * Create a new session(s).
		 *
		 * @param mixed $data - Data of the created session(s)
		 * @return bool
		 * 
		 * @uses _build()
		 *
		 */
		public function create($data) : bool{
			$created = false;

			/* @call to string $data argument. */
			if (is_string($data)) return $this->_build($data);

			/* @call to array $data argument. */
			elseif (is_array($data)) {
				// Creating a session
				foreach ($data as $id => $_data) {
					/* @call to string $id var. */
					if (is_string($id)) {
						/* @call to string $_data var. */
						if (is_string($_data)) return $this->_build($id, $_data);

						/* @call to array $_data var. */
						elseif (is_array($_data)) {
							$value 		= $_data[0] ?? 'hello world';
							$group 		= $_data[1] ?? null;
							$expire		= $_data[2] ?? null;

							$created = $this->_build($id, $value, $group, $expire);
						}
					} 
					
					/* @call to int $id var. */
					else $created = $this->_build($id);

					// When it will not be created.
					if (!$created) return false;
				}

				return true;
			}

			/* @error: Diffrent type $data argument. */
			else throw new Exception('Incompatible type <b>'.gettype($data).'</b> of the <a href="https://github.com/sopskirk/apflow/Session#create">Session::create</a> method argument!');
		}


		/**
		 * 
		 * Session based flash messages.
		 * 
		 * @param mixed $data - Data of the created flash session(s)
		 * @return bool
		 * 
		 * @uses create()
		 * 
		 */
		public function flash($data) : bool{
			$this->_flash = true;

			/* @call to string or array $data argument. */
			if (is_string($data) || is_array($data)) return $this->create($data);

			/* @error: Diffrent type $data argument. */
			else throw new Exception('Incompatible type <b>'.gettype($data).'</b> of the <a href="https://github.com/sopskirk/apflow/Session#flash">Session::flash</a> method argument!');
		}


		//------------------------------------------------------------------
        // GET METHOD
		//------------------------------------------------------------------


		/**
		 * 
		 * Retrieves session data from 
         * a specific group.
		 * 
		 * @param string $group - Sessions group
		 * @return object
		 * 
		 */
		public function group(string $group) : object{
			// Group name
			$pos = strpos($group, '.');

			if (is_null($group)) $this->group_name = $this->config['SESSION_GROUP'];
			else $this->group_name = $pos !== false ? substr($group, 0, $pos) : $group;

			// Value assignment to input.
            foreach ($_SESSION[$this->group_name] as $id => $data) {
                if (!is_array($data)) $this->_group[$id] = $data;
			}

			return $this;
		}


		/**
         * 
         * Returns the filtered data
         * of a specific session.
         * 
         * @return string
         * 
         */
        private function _data(string $data) : string{
			if (strpos($data, '.') !== false) {
				$values = [];

				if (preg_match_all('[(\w+).(\w+)]', $data, $matches)) {
					array_shift($matches);

					$values = $this->load('security')->filter($matches[1], true);
					$values = array_combine($matches[0], $values);
				}

				return $values;
			}

			return $this->load('security')->filter($data, true);
		}
		

		/**
         * 
         * It retrieves all of the session
         * and their data and returns them in 
         * the form of an associative array.
         * 
         * @param bool $all - Checking all session data
         * @return mixed
		 * 
		 * @uses _checkGroupMethod()
         * 
         */
        public function all(bool $all = false){
			// Has the group method been called?
			$this->_checkGroupMethod();

            if (!$all) {
                $data = [];

                foreach ($this->_group as $id => $value) {
					$data[$id]['data'] = $this->_data($value);
					if (!empty($this->_validation) && array_key_exists($id, $this->_validation)) $data[$id]['valid'] = $this->_validation[$id];
                }

                return $data;
            }

            // Check all data, if somewhere is false, 
            // it breaks the loops.
            foreach ($this->_validation as $id => $valid) {
                $this->valid = $valid;
                if (!$valid) break;
            }
            
            return $this;
        }


		/**
         * 
         * Returns the validated data of
         * a specific session.
         * 
         * @param mixed $data       - A set of patterns or a collective pattern
		 * @param bool $all 		- Validation for all session data
         * @param string $default   - Collective pattern
         *
         * @return void
         * @uses _data()
         * 
         */
        public function validation($data = 'http', bool $all = false, string $default = 'http') : void{
			// Skip the expire session.
			$sessions = $_SESSION;
			if (isset($sessions['expire'])) unset($sessions['expire']);

			if (isset($sessions)) {
				$validator = $this->load('security');
				
				/* @call to string $data argument. */
				if (is_string($data)) {
					foreach ($this->_group as $id => $value) {
						$validation = $validator->validation($value, $data);
						$this->_validation[$id] = $validation;
					}
				}

				/* @call to array $data argument. */
				elseif (is_array($data)) {
					foreach ($data as $id => $pattern) {
						if (array_key_exists($id, $this->_group)) {
							$value = $this->_data($this->_group[$id]);
							$this->_validation[$id] = $validator->validation($value, $pattern);
						}
					}
					
					// When the session ID was not provided in this method.
					if (count($this->_group) > count($this->_validation)) {
						foreach ($this->_group as $id => $value) {
							if (array_key_exists($id, $this->_validation) === false) {
								$value = $this->_data($this->_group[$id]);
								$this->_validation[$id] = $validator->validation($value, $default);
							}
						}
					}
				}

				/* @error: Diffrent type of argument. */
				else throw new Exception('Incompatible type </b>'.gettype($data).'<b> of the <a href="https://github.com/sopskirk/apflow/Session#validation">Session::validation</a> method argument!');

				// The overall result of validation for session data.
				if ($all) {
					foreach ($this->_validation as $id => $valid) {
						$this->valid = $valid;
						if (!$valid) break;
					}
				}
			}
		}
		

		//------------------------------------------------------------------
        // PREVIEW METHOD
		//------------------------------------------------------------------

		
		/**
		 * 
		 * Displays a preview of all active sessions.
		 * 
		 * @param string $group_name - Sessions group name
		 * @return array
		 * 
		 * @uses _checkGroupMethod()
		 * 
		 */
		public function preview(?string $group_name = null) : array{
			$sessions = [];

			if ($this->_checkGroupMethod(true)) {
				foreach ($this->_group as $id => $data) $sessions['GROUP_'.$this->group_name]['SESSION_'.$id] = $data;
			} else {
				foreach ($_SESSION as $group => $items) {
					foreach ($items as $id => $data) $sessions['GROUP_'.$group]['SESSION_'.$id] = $data;
				}
			}

			return empty($group_name) ? $sessions : $sessions['GROUP_'.$group_name];
		}


		//------------------------------------------------------------------
        // DELETE METHODS
		//------------------------------------------------------------------


		/**
		 *
		 * Deletes a specific session.
		 *
		 * @return bool
		 * @uses  _checkGroupMethod(), has()
		 *
		 */
		public function delete() : bool{
			// Has the group method been called?
			$this->_checkGroupMethod();

			/* @call to array $id var. */
			if (!empty($this->id)) {
				unset($_SESSION[$this->group_name][$this->id]);
				unset($_SESSION['expire'][$this->group_name.'.'.$this->id]);

				return true;
			} 
			
			/* @call to null $id var. */
			else {
				foreach ($_SESSION[$this->group_name] as $id => $data) {
					unset($_SESSION[$this->group_name][$id]);
					unset($_SESSION['expire'][$this->group_name.'.'.$id]);
				}

				return true;
			}

			return false;
		}


		/**
		 * 
		 * Removes all active sessions 
		 * and session cookies.
		 * 
		 * @param bool $logout - After logging out
		 * @return bool
		 * 
		 */
		public function clear(bool $logout = false) : bool{
			// If a cookie is used to pass the session id.
			if (ini_get('session.use_cookies')) $this->load('cookie')->name(session_name())->delete();

			// Destroy the session.
			if (isset($_SESSION)) {
				session_unset();
				session_destroy();

				// After logging out.
				if ($logout) {
					session_write_close();
					session_regenerate_id(true);
				}
			} else return false;

			return !isset($_SESSION);
		}


		//------------------------------------------------------------------
        // ID METHOD
		//------------------------------------------------------------------


		/**
		 * 
		 * Gets data about a specific session ID
         * given as a parameter.
		 * 
		 * @param mixed $sessions - Session(s) ID(s)
		 * @return object
		 * 
		 * @uses _whatDefaultGroup(), _data()
		 * 
		 */
		public function id($sessions) : object{
			// When the group method is not used.
			if (empty($this->group_name)) {
				/* @call to string $sessions argument. */
				if (is_string($sessions)) {
					$this->group_name 			= $this->_whatDefaultGroup($sessions);
					$this->_group[$sessions] 	= $_SESSION[$this->group_name][$sessions];
				}

				/* @call to array $sessions argument. */
				elseif (is_array($sessions)) {
					foreach ($sessions as $id) {
						$this->group_name 	= $this->_whatDefaultGroup($id);
						$this->_group[$id] 	= $_SESSION[$this->group_name][$id];
					}
				}

				/* @error: Diffrent type of $sessions argument. */
				else throw new Exception('Incompatible type <b>'.gettype($sessions).'</b> of the <a href="https://github.com/sopskirk/apflow/Session#id">Session::id</a> method argument!');
			}

			// Assignment of session data to variables.
			if (array_key_exists($sessions, $this->_group) !== false) {
				$this->id			= $sessions;
				$this->countdown	= $this->load('date')->countdown(date('Y:m:d H:i:s', $_SESSION['expire'][$this->group_name.'.'.$sessions]));
				$this->data 		= $this->_data($this->_group[$sessions]);
				$this->valid    	= $this->_validation[$sessions] ?? $this->valid;
			}

			// The session is not in the group.
			else throw new Exception('The session about ID <q>'.$sessions.'</q> is not in the <q>'.$this->group_name.'</q> group.');
			return $this;
		}
	}