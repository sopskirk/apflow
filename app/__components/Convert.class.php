<?php
/**
 * 
 * This class is used to convert:
 * - to URL,
 * - to server path,
 * - to timestamp,
 * - to date
 * 
 * @package apflow
 * @version 1.0
 * 
 */


    namespace apflow\Components;
    use apflow\Bootstrap;

    class Convert extends Bootstrap{
        //------------------------------------------------------------------
        // PATH METHODS
        //------------------------------------------------------------------


        /**
		 *
		 * Converts the server path to a file 
         * into the URL path.
		 *
         * @param string $path  - Path to file
         * @param bool $check   - Is he supposed to check the path?
         * 
		 * @return string
		 *
		 */
        public function toURL(string $path, bool $check = true) : string{
            if (!filter_var($path, FILTER_VALIDATE_URL)) {
                $root = $this->load('detect')->ip() != '127.0.0.1' && !empty(ROOT_PATH) ? ROOT_PATH : '';
                $path = str_replace($this->filterPath(ROOT_PATH), $this->load('request')->location()->protocol.'://'.$_SERVER['HTTP_HOST'].$root, $this->filterPath($path));
            }
            
            return $this->load('security')->filterURL($path);
        }


        /**
		 *
		 * Converts the URL path to a file 
         * into the server path.
		 *
         * @param string $path - URL address to the file
		 * @return string
		 *
		 */
        public function toServerPath(string $url) : string{
            $path = str_replace($this->load('request')->location()->protocol.'://'.$_SERVER['HTTP_HOST'].'/', $this->filterPath(ROOT_PATH), $url);
            return $this->filterPath($path);
        }


        //------------------------------------------------------------------
        // DATA METHODS
        //------------------------------------------------------------------


        /**
         *
         * Convert date to timestamp.
         * 
         * @param string $expire 		- Verbal time
         * @param string $custom_date 	- Custom date (other than current)
         * @param string $operator      - Operator adding or subtracting from date
         * @param string $format        - The format of the outputted date string
         * 
         * @return int
         *
         */
        public function toTimestamp(string $expire, ?string $custom_date = null, string $operator = '+', string $format = 'Y:m:d H:i:s') : int{
            return !empty($custom_date) ? strtotime($custom_date.$operator.$expire) : strtotime(date($format).$operator.$expire);
        }


        /**
         *
         * Convert timestamp to date.
         * 
         * @param mixed $expire 		- Verbal time or timestamp time
         * @param string $format        - The format of the outputted date string
         * 
         * @return string
         * @uses toTimestamp()
         *
         */
        function toDate($expire, string $format = 'Y:m:d H:i:s') : string{            
            return date($format, $expire);
        }
    }