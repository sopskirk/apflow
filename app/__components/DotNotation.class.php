<?php
/**
 * 
 * A PHP class to access a PHP 
 * array via dot notation.
 * 
 * @package apflow
 * @version 1.0
 * 
 * @see https://selvinortiz.com/blog/traversing-arrays-using-dot-notation
 * 
 */


    namespace apflow\Components;

    class DotNotation{
        /**
         * 
         * Converts dot notation to an 
         * array and then returns the array value.
         * 
         * @param string $key 		- Dot notation
         * @param array $data 		- Main array with data
         * @param string $default 	- Default value
         * @param bool $ret_array	- Is he to return the array
         * 
         * @return mixed
         * 
         */
        public function set(string $key, array $data, ?string $default = null, bool $array = false){
            if (empty($key) || !is_string($key) || empty($data)) return $default;

            // @assert $key contains a dot notated string
            if (strpos($key, '.') !== false) {
                $keys = explode('.', $key);
                
                foreach ($keys as $innerKey) {
                    // @assert $data[$innerKey] is available to continue
                    // @otherwise return $default value
                    if (!array_key_exists($innerKey, $data)) return $default;
                    $data = $data[$innerKey];
                    
                    // The last key gets.
                    if ($array) $key = $innerKey;
                }

                return !$array ? $data : array($key => $data);
            }

            // @fallback returning value of $key in $data or $default value
            if (array_key_exists($key, $data)){
                $data = $data[$key];
                return !$array ? $data : array($key => $data);
            }

            return $default;
        }
    }