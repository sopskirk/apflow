<?php
/**
 *
 * Features:
 * - Add/remove and manage headers easily.
 * - Build a Content Security Policy, or combine multiple together.
 * - Content Security Policy analysis
 * - Safe mode prevents accidental long-term self-DOS
 * - When using HSTS or HPKP.
 * - Receive warnings about missing, or misconfigured security headers.
 *
 * @package apflow
 * @subpackage Security
 * @version 1.0
 * 
 * @uses https://github.com/aidantwoods/SecureHeaders
 *
 */


	namespace apflow\Components\Security;
	use Aidantwoods\SecureHeaders\SecureHeaders;

	if (class_exists('Aidantwoods\SecureHeaders\SecureHeaders')) {
		class CSP extends SecureHeaders{
			/**
			 * 
			 * Stores config
			 * 
			 * @var array
			 * 
			 */
			private $_config = [];


			//------------------------------------------------------------------
       	 	// CSP METHODS
        	//------------------------------------------------------------------


			/**
			 *
			 * Constructor load key methods related to CSP and HPKP.
			 *
			 * @subpackage SecureHeaders
			 * @return void
			 *
			 */
			public function __construct(){
				$this->_config = require APP_PATH.DIRECTORY_SEPARATOR.'__config'.DIRECTORY_SEPARATOR.'config.csp.php';

				// Implicitly call $this->apply() on first byte of output.
				$this->applyOnOutput();

				// Strict mode -> docs strictMode.md
				$this->strictMode();

				// Generate a hash of the provided value -> docs cspHash.md
				$this->cspHash('script', $this->_config['CSPHash']);

				// Generate a nonce value.
				$this->nonce();

				// Protected cookies
				$protectedCOOKIE = $this->_config['ProtectedCOOKIE'];
				if (!empty($protectedCOOKIE)) $this->protectedCookie($protectedCOOKIE, self::COOKIE_SUBSTR | self::COOKIE_REMOVE);

				// HTTP Public Key Pinning pins
				$this->setHPKP();

				// Use regular PHP function to add strict transport security.
				$this->hsts(31536000, true, true);

				// Content Security Policy directives
				$this->setCSP();

				// HSTS in safe mode
				$this->safeModeException('Strict-Transport-Security');

				// list of headers configured in SecureHeaders
				$this->apply();
			}


			/**
			 *
			 * Generate a nonce value.
			 *
			 * @return string
			 * @see docs cspNonce.md
			 *
			 */
			public function nonce() : string{
				return $this->cspNonce('script');
			}


			/**
			 *
			 * HTTP Public Key Pinning pins.
			 *
			 * @return void
			 * @see docs hpkp.md
			 *
			 */
			private function setHPKP() : void{
				$this->hpkp($this->_config['HPKP']['PINS'], $this->_config['HPKP']['MAXAGE'], $this->_config['HPKP']['SUBDOMAINS'], $this->_config['HPKP']['REPORT-URI'], $this->_config['HPKP']['REPORT-ONLY']);
			}


			/**
			 *
			 * Content Security Policy directives.
			 * Combine various CSP directives.
			 *
			 * @return void
			 * @see docs csp.md
			 *
			 */
			private function setCSP() : void{
				$this->csp($this->_config['CSP']);
			}
		}
	}