<?php
/**
 *
 * This class is responsible for the overall validation of
 * the data and their protection respectively cleans the
 * field, protects against XSS attack, encodes and verifies
 * the password, and also checks the fields in terms of patterns.
 *
 * @package apflow
 * @subpackage Security
 * @version 1.0
 *
 */


	namespace apflow\Components\Security;
	use apflow\Components\Import;

	class Security extends XSSClean{
		/**
		 *
         * Sets the settings from 
		 * the security file.
		 * 
		 * @return array
		 *
         */
		private function _config(){
			return require APP_PATH.DIRECTORY_SEPARATOR.'__config'.DIRECTORY_SEPARATOR.'config.security.php';
		}


		//------------------------------------------------------------------
        // FILTER METHODS
		//------------------------------------------------------------------
		

		/**
		 * 
		 * Trait for the filtering method.
		 * 
		 * @param mixed $values - Value(s) to filter
		 * @param bool $trim 	- Is space to be allowed? 
		 * 
		 * @return string
		 * 
		 */
		private function __traitFilterMethod(string $value, bool $trim = true) : string{
			$filter = stripslashes(htmlspecialchars($value, ENT_QUOTES, 'utf-8'));
			return $trim ? $this->XSSClean(trim(preg_replace('/ {2,}/', ' ', $filter))) : $this->XSSClean(trim(preg_replace('/ {1,}/', '', $filter)));
		}


		/**
		 *
		 * Filtrates (clears) the field 
		 * from the wrong characters.
		 *
		 * @param mixed $values - Value(s) to filter
		 * @param bool $trim 	- Is space to be allowed? 
		 * 
		 * @return mixed
		 * @uses __traitFilterMethod()
		 *
		 */
		public function filter($values, bool $trim = true){
			/* @call to string $values argument. */
			if (is_string($values)) return $this->__traitFilterMethod($values, $trim);

			/* @call to null $values argument. */
			if (is_null($values)) return $values;

			/* @call to array $values argument. */
			if (is_array($values)) {
				$result = [];

				foreach ($values as $v) $result[] = $this->__traitFilterMethod($v, $trim);
				return $result;
			}

			/* @error: Diffrent type $name argument. */
			else throw new Exception('Incompatible type <b>'.gettype($values).'</b> of the <a href="https://github.com/sopskirk/apflow/Security#filter">Security::filter</a> method argument(s)!');
		}


		/**
		 * 
		 * Filters the URL address.
		 * 
		 * @param string $input - Input data
		 * @param bool $strip 	- Strip HTML and PHP tags from a string?
		 * 
		 * @return string
		 * 
		 */
		public function filterURL(string $input, bool $strip = true) : string{
			$input = str_replace(array('\\' , ' '), array('/', ''),trim($input));
			
			if ($input == '/') return $input;

			// add more chars if needed
			$input = str_ireplace(["\0", '%00', "\x0a", '%0a', "\x1a", '%1a'], '', rawurldecode($input));

			// remove markup stuff
			if ($strip) $input = strip_tags($input);

			// or any encoding you use instead of utf-8
			$input = htmlspecialchars($input, ENT_QUOTES, 'utf-8');

			// removes duplicates '/'
			return preg_replace('/([^:])(\/{2,})/', '$1/', $input);
		}


		//------------------------------------------------------------------
        // PASSWORD METHODS
        //------------------------------------------------------------------


		/**
		 *
		 * Encrypts the given password using 
		 * the PASSWORD_ARGON2I algorithm.
		 *
		 * @param string $password - The user password
		 * @return string
		 * 
		 * @uses _config()
		 *
		 */
		public function encrypted(string $password) : string{
			$config = $this->_config()['PASSWORD'];

			return password_hash($password, $config['ALGORITHM'], [
				'cost' => $config['COST'],
				'salt' => $config['SALT']
			]);
		}


		/**
		 *
		 * Verifies encrypted input.
		 *
		 * @param string $password 	- The user's password
		 * @param string $hash 		- Password HASH
		 * 
		 * @return bool
		 * @uses _config()
		 *
		 */
		public function verify(string $password, string $hash) : bool{
			return password_verify($password, $hash);
		}


		//------------------------------------------------------------------
        // VALIDATION METHOD
        //------------------------------------------------------------------


		/**
		 *
		 * Data validation patterns from the form.
		 *
		 * @param string $value 	- Value to check
		 * @param string $pattern 	- Pattern name
		 * 
		 * @return bool
		 * @uses _config()
		 *
		 */
		public function validation(string $value, string $pattern) : bool{
			if ($pattern == 'email') return filter_var($value, FILTER_VALIDATE_EMAIL);
			else {
				$pattern = preg_match('/^[\w]+$/i', $pattern) !== false ? $this->_config()[$pattern] : $pattern;
				return preg_match($pattern, $value);
			}
		}
	}
