<?php
/**
 * 
 * This file is used to initialize the connection 
 * to the database and operations on it using MysqliDb.
 * 
 * @package apflow
 * @version 1.0
 * 
 * @see https://github.com/ThingEngineer/PHP-MySQLi-Database-Class
 * 
 */


    namespace apflow\Components;
    use MysqliDb;
    use Exception;

    if (class_exists('MysqliDb') && RUN_SQL == true) {
        /**
         * 
         * This class is used to perform database 
         * operations using MysqliDb.
         * 
         */
        class SQL{
            /**
             * 
             * Handler to object MysqliDb.
             * 
             * @var object
             * 
             */
            public $db;

            /**
             * 
             * Stores data to connect
             * to the database.
             * 
             * @var array
             * 
             */
            private $_config;


            /**
             * 
             * Initialization of Mysqlidb class methods.
             * 
             * @return void
             * 
             */
            public function __construct(){
                // Configuration data
                $this->_config = include APP_PATH.DIRECTORY_SEPARATOR.'__config'.DIRECTORY_SEPARATOR.'config.db.php';
                $config = $this->_config['default'];

                // DB staying private here.
                $this->db = new MysqliDb([
                    'host'      => $config['HOST'],
                    'username'  => $config['USER'],
                    'password'  => $config['PASS'],
                    'db'        => $config['DBNAME'],
                    'port'      => $config['PORT'],
                    'charset'   => $config['CHARSET']
                ]);

                $this->db->setPrefix($config['PREFIX']);
            }


            /**
             * 
             * This method is used to connect to a different 
             * base than the default one.
             * 
             * @param string $connectName - The name of the db connection
             * @return object
             * 
             */
            public function db(string $connectName = 'default'){
                $default    = $this->_config['default'];
                $multiple   = $this->_config[$connectName] ?? false;

                if ($multiple !== false) {
                    $params = [];
                    $prefix = $default['PREFIX'];

                    if ($connectName != 'default') {
                        $params = [
                            'host'      => $multiple['HOST']    ?? $default['HOST'],
                            'username'  => $multiple['USER']    ?? $default['USER'],
                            'password'  => $multiple['PASS']    ?? $default['PASS'],
                            'db'        => $multiple['DBNAME']  ?? $default['DBNAME'],
                            'port'      => $multiple['PORT']    ?? $default['PORT'],
                            'charset'   => $multiple['CHARSET'] ?? $default['CHARSET']
                        ];

                        $prefix = $multiple['PREFIX'] ?? $default['PREFIX'];
                    }

                    else {
                        $params = [
                            'host'      => $default['HOST'],
                            'username'  => $default['USER'],
                            'password'  => $default['PASS'],
                            'db'        => $default['DBNAME'],
                            'port'      => $default['PORT'],
                            'charset'   => $default['CHARSET']
                        ];
                    }
                    
                    $db = $this->db->addConnection($connectName, $params);
                    $db->setPrefix($prefix);
                    
                    return $db->connection($connectName);
                }

                else throw new Exception('The database connection <q>'.$connectName.'</q> name has not been declared!');
            }


            /**
             * 
             * Set Error SQL
             * 
             * @param mixed $data   - DB handle
             * @param array $params - Error data
             * 
             * @return void
             * 
             */
            public function errorSQL($data, ?array $params = null) : void{
                $db = is_object($data) ? $data : $this->db;
                $params = $params ?? $data;
                
                if ($db->getLastErrno() !== 0) report('ERROR SQL!', $params['file'], $params['line'], $db->getLastQuery(), $db->getLastError());
            }
        }
    }