<?php
/**
 * 
 * This class is used to check 
 * the type of variables.
 * 
 * @package apflow
 * @version 1.0
 * 
 * @author Keos Media
 * @copyright Keos Media
 * 
 */


	namespace apflow\Components;

	class IsType {
		/**
		 * 
		 * 
		 * 
		 * @param mixed $var 		- 
		 * @param int $digits 		- 
		 * @param int $precision	- 
		 * 
		 * @return bool
		 * 
		 */
		public function isNumber($var, int $digits = 10, int $precision = 2) : bool{
			$pattern = '#^[\d]{1,'.$digits.'}([.|,][\d]{1,'.$precision.'})?$#';
			return preg_match($pattern, $var) ? true : false;
		}		
		

		/**
		 * 
		 * 
		 * 
		 * @param string $file - 
		 * @return bool
		 * 
		 */
		public function isFileName(string $file) : bool {
			return preg_match('/^([-\.\w]+)$/', $file) > 0;
		}	
		

		/**
		 * 
		 * 
		 * 
		 * @param string $email - 
		 * @return bool
		 * 
		 */
		public function isEmail(string $email) : bool {
			return preg_match("/^([_a-z0-9-\.]+)\@([_a-z0-9-\.]+)(\.[a-z]{2,3})$/is", $email) > 0 ? true : false;
		}
		

		/**
		 * 
		 * 
		 * 
		 * @param string $md5 - 
		 * @return bool
		 * 
		 */
		public function isMd5(string $md5) : bool{
			return !empty($md5) && preg_match('/^[a-f0-9]{32}$/', $md5);
		}


		/**
		 * 
		 * 
		 * 
		 * @param string $ip - 
		 * @return bool
		 * 
		 */
		public function isIP(string $ip) : bool{
			return preg_match("/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/", $ip) > 0 ? true : false;
		}
		

		/**
		 * 
		 * 
		 * 
		 * @param string $url - 
		 * @return bool
		 * 
		 */
		public static function isUrl($url) : bool{
			return !empty($url) && preg_match('#([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&\'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)|[\xE000-\xF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\x00A0-\xD7FF\xF900-\xFDCF\xFDF0-\xFFEF])|(%[\da-f]{2})|[!\$&\'\(\)\*\+,;=]|:|@)|\/|\?)*)?#iS', $url);
		}	


		/**
		 * 
		 * 
		 * 
		 * @param string $regon - 
		 * @return bool
		 * 
		 */
		public function isREGON(string $regon) : bool{
			$regon = preg_replace("/[^0-9]/","", $regon);
			if (strlen($regon) == 7) $regon = '00'.$regon;
		
			if (strlen($regon) == 9) {
				$funProductArrays = function ($a,$b){
					return($a * $b);
				};		  
					
				$regon = str_split($regon);
				$controlDigit = array_pop($regon);
				$weights = [ 8,9,2,3,4,5,6,7 ];
			
				$b = array_map($funProductArrays,$weights,$regon);
				$sum = array_sum($b);
				$modulo = $sum%11;
				$modulo = ($modulo==10) ? 0 : $modulo;
			
				return $modulo == $controlDigit ? true : false;
			}

			return false;
		}
		

		/**
		 * 
		 * 
		 * 
		 * @param int $areaCode - 
		 * @return bool
		 * 
		 */
		public function isAreaCode(int $areaCode) : bool{
			return preg_match("/(\+([0-9]{2}))|([0-9]{3})/", $areaCode, $matches) ? true : false;
		}
		

		/**
		 * 
		 * 
		 * 
		 * @param string $nip - 
		 * @return bool
		 * 
		 */
		public function isNIP(string $nip) : bool{
			$nip = preg_replace("/[^0-9]/","", $nip);
			
			if (strlen($nip) == 10){
				$funProductArrays = function ($a,$b){
					return($a * $b);
				};	
				
				$nip = str_split($nip);
				$controlDigit = array_pop($nip);
				$weights = [ 6,5,7,2,3,4,5,6,7 ];
				
				$b = array_map($funProductArrays,$weights,$nip);
				$sum = array_sum($b);
				$modulo = $sum%11;
			
				return $modulo == $controlDigit ? true : false;
			}
			
			return false;
		}
		

		/**
		 * 
		 * 
		 * 
		 * @param string $iban - 
		 * @return bool
		 * 
		 */
		public function isIBAN(string $iban) : bool{
			$Codes = Array (
				"AL","DZ","AD","AO","AT","AZ","BH","BY","BE","BJ",
				"BA","BR","VG","BG","BF","BI","CM","CV","FR","CG",
				"CR","HR","CY","CZ","DK","DO","EG","EE","FO","FI",
				"FR","FR","FR","GA","GE","DE","GI","GR","GL","FR",
				"GT","GG","HU","IS","IR","IQ","IE","IM","IL","IT",
				"CI","JE","JO","KZ","XK","KW","LV","LB","LI","LT",
				"LU","MK","MG","ML","MT","FR","MR","MU","MD","MC",
				"ME","MZ","NL","FR","NO","PK","PS","PL","PT","QA",
				"FR","RO","LC","FR","SM","ST","SA","SN","RS","SC",
				"SK","SI","ES","SE","CH","TL","TN","TR","UA","AE","GB","FR"
			);
			
			$LengthAccount = Array (
				28,24,24,25,20,28,22,28,16,28,
				20,29,24,22,27,16,27,25,27,27,
				21,21,28,24,18,28,27,20,18,18,
				27,27,27,27,22,22,23,27,18,27,
				28,22,28,26,26,23,22,22,23,27,
				28,22,30,20,20,30,21,28,21,20,
				20,19,27,28,31,27,27,30,24,27,
				22,25,18,27,15,24,29,28,25,29,
				27,24,32,27,27,25,24,28,22,31,
				24,19,24,24,21,23,24,26,29,23,22,27
			);	 
		
			// Normalize input (remove spaces and make upcase)
			$iban = strtoupper(preg_replace("/[^0-9A-Za-z]/","", $iban));
		
			if (preg_match('/^[A-Z]{2}[0-9]{2}[A-Z0-9]{1,30}$/', $iban)) {
				$country 	= substr($iban, 0, 2);
				$check 		= intval(substr($iban, 2, 2));
				$account 	= substr($iban, 4);
				
				$key = array_search($country, $Codes);

				//Jeśli nie mamy wybranego kodu
				if (is_null($key) || $key === false) return false;
	
				//Jeśli dlugoś numeru konta jest niezgodna z określonym kodem państwa
				if (strlen($iban) != $LengthAccount[$key]) return false;		
			
				// To numeric representation
				$search = range('A','Z');
				foreach (range(10, 35) as $tmp) $replace[] = strval($tmp);
				
				$numstr = str_replace($search, $replace, $account.$country.'00');
		
				// Calculate checksum
				$checksum = intval(substr($numstr, 0, 1));
				
				for ($pos = 1; $pos < strlen($numstr); $pos++) {
					$checksum *= 10;
					$checksum += intval(substr($numstr, $pos,1));
					$checksum %= 97;
				}	
				
				return ((98-$checksum) == $check);
			}
			
			return false;
		}	
		

		/**
		 * 
		 * 
		 * 
		 * @param string $barcode - 
		 * @return bool
		 * 
		 */
		public function isEAN(string $barcode) {
			return (clsLibGTIN::GTINCheck($barcode) !== false) ? true : false;
		}
		

		/**
		 * 
		 * 
		 * 
		 * @param string $date - 
		 * @return bool
		 * 
		 */
		public function isDateFormat(string $date) : bool{
			if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts)) return checkdate($parts[2], $parts[3], $parts[1]) ? true : false;
			else return false;
		}
		
		/**
		 * 
		 * 
		 * 
		 * @param string $time - 
		 * @return bool
		 * 
		 */
		public function isTimeFormat(string $time){
			return preg_match("/^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/", $time) ? true : false;
		}


		/**
		 * 
		 * 
		 * 
		 * @param string $date_time - 
		 * @return bool
		 * 
		 */
		public static function isDateTimeFormat(string $date_time){
			return preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})\s([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/", $date_time) ? true : false;	   
		}
	}