<?php
/**
 * 
 * This class is used for date
 * and time operations
 * 
 * @package apflow
 * @version 1.0
 * 
 */


   namespace apflow\Components;
   use apflow\Bootstrap;
	use Exception;

   class DateTime{
      /**
       * 
       * Time countdown
       * 
       * @param string $time - Date in formate Y:m:d H:i:s
       * @param array $units - Units of time
       * 
       * @return string
       * 
       */
		public function countdown(string $time, array $units = []) : string{
         $time          = strtotime($time);
         $current_time  = strtotime(date('Y:m:d H:i:s'));

         $substr = $time - $current_time;

         $hour    = floor(($substr) / 3600);
         $min     = (($substr) / 60) % 60;
         $second  = $substr % 60;

         // Units of time
         if (!$units) $units = ['hour' => 'hour', 'minute' => 'min', 'second' => 'sec'];

         $hour = $hour == 0 ? '' : $hour.' '.$units['hour'];

         if ($second != 0) {
            $min = $min == 0 && $hour == 0 ? $second.' '.$units['second'] : $min.' '.$units['minute'].' '.$second.' '.$units['second'];
         } else
            $min != 0 ? $min = $min.' '.$units['minute'] : $min = '';

         return $hour.$min;
      }


      /**
       * 
       * Converting date to time ago.
       * 
       * @param string $timestamp   - Timestamp time
       * @param int $precision      - Precision to which time should be given
       * @param bool $substract     - Is he supposed to do the action?
       * 
       * @return string
       * @author https://gist.github.com/james2doyle/4739742
       * 
       */
      public function elapsedTime(string $timestamp, int $precision = 2, bool $substract = true, array $suffix = []) : string{
         $time = $substract ? time() - $timestamp : $timestamp;
         $elapsed = [
            'decade'    => 315576000, 
            'year'      => 31557600, 
            'month'     => 2629800, 
            'week'      => 604800, 
            'day'       => 86400, 
            'hour'      => 3600, 
            'min'       => 60, 
            'sec'       => 1
         ];

         // Suffix to time ago.
         if (!$suffix) $suffix = ['ago' => 'ago', 'just' => '1 sec to go'];

         $i = 0;
         
         foreach ($elapsed as $unit => $value) {
            $count = floor($time/$value);
            if ($count) $i++;

            $time = $i >= $precision ? 0 : $time - $count * $value;

            $s       = $count > 1 ? 's' : '';
            $count   = $count ? $count.' '.$count.$s.' ' : '';
            $result .= $count;
         }
         return $result ? $result.$suffix['ago'] : $suffix['just'];
      }
   }