<?php
/**
 *
 * This class is used to translate 
 * the content of the page.
 *
 * @package apflow
 * @version 1.0.0
 *
 */


    namespace apflow\Components;
    use apflow\Bootstrap;

    class Localization extends Bootstrap{
        /**
		 * 
		 * Stores configurations
		 * @var array
		 * 
		 */
		private $_config = [];


		/**
		 *
         * Sets the settings from the localizations file.
		 *
         */
		public function __construct(){
			$this->_config = include APP_PATH.DS.'__config'.DS.'config.localization.php';
        }


        //------------------------------------------------------------------
        // TRANSLATE METHODS
		//------------------------------------------------------------------


        /**
		 *
		 * Checks the browser language of the user who
		 * is currently on the page.
		 *
		 * @return string
		 *
		 */
		private function _lang() : string{
            $config = $this->_config;
            $test   = $config['TEST_LANGUAGE'];

            if (!$test){
                $browser    = $this->load('detect')->lang();
                $exists     = false;

                foreach ($config['LANGUAGES'] as $lang) {
                    if ($browser == $lang) {
                        $browser    = $lang;
                        $exists     = true;
                        
                        break;
                    }
                }
                
                return $exists ? $browser : $config['DEFAULT_LANGUAGE'];
            }

            return $config['DEFAULT_LANGUAGE'];
        }
        

        /**
		 *
		 * Translates the content of the page.
		 *
         * @param string $filename - The name of the file with the translation
		 * @return array
         * 
         * @uses _lang()
		 *
		 */
        public function translate(?string $filename = null) : array{
            // Create path to file localization.
            $path = empty($filename) ? '.ini' : DS.$filename.'.ini';
            $path = $this->load('file')->name(PROJECT_PATH.'/localizations/'.$this->_lang().$path)->files();

            return parse_ini_file($path, true);
        }
    }