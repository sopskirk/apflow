<?php
/**
 * 
 * This file contains all additional functions.
 * 
 * @package apflow
 * @subpackage funcs
 * @version 1.0
 * 
 */


    //------------------------------------------------------------------
    // STRING FUNCTIONS
    //------------------------------------------------------------------


    /** 
     * 
     * This function cuts the text to the set limit.
     * 
     * @param string $str   - Input string
     * @param int $limit    - Limit letters
     * @param string $pad   - Suffix
     * 
     * @return string
     * 
     */
    function truncateText(string $str, int $limit, string $pad = '...') : string{
        $tail   = max(0, $limit-10);
        $trunk  = substr($str, 0, $tail);
        $trunk .= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $limit-$tail))));
        
        if (substr($trunk, -3, 3) == '...') $trunk = substr($trunk, 0, -3).$pad;
        return $trunk;
    }


    /**
     * 
     * Checks whether the given word(s) 
     * is in the string.
     * 
     * @param string $haystack  - The string to search in
     * @param mixed $needle     - Search character
     * @param int $offset       - Search will start this number of characters
     * 
     * @return mixed
     * @see http://php.net/manual/en/function.strpos.php
     * 
     */
    function is(string $haystack, $needle, int $offset = 0){
        if (is_array($needle)) {
            foreach ($needle as $search) {
                $pos = strpos($haystack, $search, $offset);
                if ($pos !== false) return $pos;
            }

            return false;
        }

        return strpos($haystack, $needle, $offset);
    }


    /**
     * 
     * It checks the correctness of the given 
     * subject automatically for an array or string.
     * 
     * @param string $pattern   - The pattern to search for, as a string
     * @param mixed $subject    - The input string
     * 
     * @return bool
     * 
     * @see http://php.net/manual/en/function.preg-grep.php
     * @see http://php.net/manual/en/function.preg-match.php
     * 
     */
    function match(string $pattern, $subject) : bool{
        /* Call to string $subject. */
        if (is_string($subject)) return (bool)preg_match($pattern, $subject);

        /* Call to array $subject. */
        elseif (is_array($subject)) {
            foreach($subject as $key => $value) {
                if (preg_match($pattern, $key)) $exists = true;
                else {
                    $exists = false;
                    break;
                }
            }
    
            return $exists;
        }

        /* Error: Diffrent type $subject argument. */
        else throw new Exception('Incompatible type <b>'.gettype($subject).'</b> of the <a href="https://github.com/sopskirk/apflow/xString:#match">xString:match</a> method argument!');
    }


    //------------------------------------------------------------------
    // ARRAY FUNCTIONS
    //------------------------------------------------------------------


    /**
     * 
     * Checks if the array is multidimensional and 
     * returns its dimension.
     *
     * @param array $array - Checked array
     * @return bool
     *
     */
    function isMulti(array $array) : bool{
        rsort($array);
        return (bool)(isset($array[0]) && is_array($array[0]));
    }


    /**
     *
     * Check if specific array key exists
     * in multidimensional array.
     *
     * @param mixed $key	- Search key
     * @param array $array 	- Searched array
     * 
     * @return bool
     * @author https://stackoverflow.com/a/19421079
     *
     */
    function searchKey($key, array $array) : bool{
        foreach ($array as $_key => $item){
            if ($_key == $key) return true;
            elseif (is_array($item) && searchKey($item, $key)) return true;
        }

        return false;
    }


    //------------------------------------------------------------------
    // RESOURCES FUNCTIONS
    //------------------------------------------------------------------


    /**
     * 
     * Creates a URL to the upload dir.
     * 
     * @param string $path - Pathname to file
     * @return string
     * 
     */
    function upload(string $path) : string{
        $path = 'upload'.DS.$path;
        return load('convert')->toUrl($path);
    }


    /**
     * 
     * Creates a URL to the file.
     * 
     * @param string $path      - Pathname to file
     * @param bool $filemtime   - Add to end URL file time?
     * 
     * @return string
     * 
     */
    function url(string $path, bool $filemtime = true) : string{
        $path   = PROJECT_PATH.DS.$path;
        $url    = load('convert')->toUrl($path);

        return $filemtime ? $url.'?v='.filemtime($path) : $url;
    }


    /**
     * 
     * Load styles form styles dir.
     * 
     * @param string $path  - Path to file
     * @param string $dir   - Default dir
     * 
     * @return void
     * 
     */
    function load_css($path, string $dir = 'styles') : void{
        if (is_string($path)) {
            $pather = filter_var($path, FILTER_VALIDATE_URL) === false ? url('assets/'.$dir.'/'.$path) : $path;
            echo '<link rel="stylesheet" href="'.$pather.'">';
        }
        
        else {
            $controller = load('controller');

            foreach ($path as $key => $files) {
                if (is_array($files)) {
                    foreach ($files as $file) {
                        if ($controller->groupView($key)) load_css($key.'/'.$file);
                    }
                }

                else load_css($files);
            }
        }
    }


    /**
     * 
     * Load scripts from scripts dir.
     * 
     * @param string $path  - Path to file
     * @param string $nonce - CSP Nonce
     * @param string $dir   - Default dir
     * 
     * @return void
     * 
     */
    function load_js($path, string $nonce = null, string $dir = 'scripts') : void{
        if (is_string($path)) {
            $nonce  = !is_null($nonce) ? ' nonce="'.$nonce.'"' : '';
            $pather = filter_var($path, FILTER_VALIDATE_URL) === false ? url('assets/'.$dir.'/'.$path) : $path;

            echo '<script src="'.$pather.'" '.$nonce.'></script>';
        }
        else {
            $controller = load('controller');
            
            foreach ($path as $key => $files) {
                if (is_array($files)) {
                    foreach ($files as $file) {
                        if ($controller->groupView($key)) load_js($key.'/'.$file);
                    }
                }

                else load_js($files, $nonce);
            }
        }
    }


    /**
     * 
     * Load files from plugins dir.
     * 
     * @param string $path  - Path to file
     * @param string $nonce - CSP Nonce
     * 
     * @return void
     * 
     */
    function load_plugins($path, string $nonce = null) : void{
        if (is_string($path)) {
            $js = pathinfo($path)['extension'] == 'js' ? true : false;
            $js ? load_js($path, $nonce, 'plugins') : load_css($path, 'plugins');
        }
        else {
            foreach ($path as $file) load_plugins($file, $nonce);
        }
    }


    /**
     * 
     * Load images from images dir.
     * 
     * @param string $path     - Path to file
     * @param array $options   - Options image
     * 
     * @return mixed
     * 
     */
    function load_image(string $path, array $options = []){
        $file   = pathinfo($path);
        $pather = filter_var($path, FILTER_VALIDATE_URL) === false ? url('assets/images/'.$path, false) : $path;

        if ($file['extension'] != 'svg') {
            $type = $options['type'] ?? 'img';

            switch ($type) {
                case 'url'  : return $pather;
                case 'bg'   : return 'background-image:url('.$pather.')';
                case 'img'  : default : 
                    $class  = isset($options['class']) ? 'class="'.$options['class'].'"' : '';
                    $id     = isset($options['id']) ? 'id="'.$options['id'].'"' : '';
                    $alt    = isset($options['alt']) ?? $file['filename'];
                    $data   = $options['data'] ?? false;

                    $datas = '';
                    if ($data != false) {
                        foreach ($data as $key => $value) $datas .= ' data-'.$key.'="'.$value.'"';
                    }

                    return '<img src="'.$pather.'" alt="'.$alt.'" '.$class.' '.$id.''.$datas.'>';
                break;
            }
        }

        else require PROJECT_PATH.DS.filterPath('assets/images/'.$path);
    }


    //------------------------------------------------------------------
    // REQUEST METHODS FUNCTIONS
    //------------------------------------------------------------------


    /**
     * 
     * Clears values ​​from the global $_GET array
     * 
     * @param string $key   - Key global array key $_GET
     * @param bool $int     - Param is int?
     * 
     * @return mixed
     * 
     */
    function _GET(string $key, bool $int = true){
        if (isset($_GET[$key])) {
            $GET = load('security')->filter($_GET[$key], false);
            return $int ? (int)$GET : $GET;
        }
        
        return null;
    }
    

    /**
     * 
     * Clears values ​​from the global $_POST array
     * 
     * @param string $key   - Key global array key $_POST
     * @param bool $trim    - Is space to be allowed? 
     * 
     * @return mixed
     * 
     */
    function _POST(string $key, bool $trim = true){
        if (isset($_POST[$key])) {
            $security = load('security');

            $POST = $_POST[$key];
            $retPOST = [];
    
            if (is_string($POST)) return $security->filter($_POST[$key], $trim);
            else {
                foreach ($POST as $fields => $values) {
                    if (is_string($values)) $retPOST[$fields] = $security->filter($values, $trim);
                    else {
                        foreach ($values as $key => $value) {
                            $field = $POST[$fields];
                            $retPOST[$field['name']] = $security->filter($field['value'], $trim);
                        }
                    }
                }
                
                return $retPOST;
            }
        }
        
        return null;
    }


    /**
     * 
     * checks the method and cleans 
     * the appropriate global method array.
     * 
     * @param string $key   - Key global array key $_POST|GET
     * @param bool $option  - Option form methods functions
     * 
     * @return mixed
     * 
     */
    function _REQUEST(string $key, bool $option = false){
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET' 	: return _GET($key, $option);break;
            case 'POST'	: return _POST($key, $option);break;
        }
        
        return null;
    }


    //------------------------------------------------------------------
    // OTHERS FUNCTIONS
    //------------------------------------------------------------------


    /**
     * 
     * 
     * 
     * @param int $total_score  - 
     * @param int $max_score    -
     * 
     * @return bool
     * 
     */
    function progress(int $total_score, int $max_score) : int{
        $progress = $total_score > 0 && $max_score > 0 ? number_format(($total_score / $max_score) * 100, 0) : 0;
        return $progress >= 100 ? 100 : (int)$progress;
    }