<?php
/**
 *
 * This file is a central file that 
 * loads all resources.
 * 
 * @package apflow
 * @version 1.0
 * 
 * @author Kamil Staniec
 * @link https://www.sopskirk.pl
 * @source https://www.github/sopskirk/apflow/
 * 
 */


	// Checking the current PHP version.
	if (version_compare(phpversion(), '7.2.0', '<')) {
		@ob_end_clean();
		die("PHP version isn't high enough! The minimum PHP version is 7.2.0");
	}

	// Current Frmamework version.
	define('VERSION', '1.0');

	// Autoload classes
	require APP_PATH.DS.'__autoload'.DS.'__autoload.php';

	// Create a apflow Bootstrap object.
	$boot = new apflow\Bootstrap;

	// Included a apflow bootstrap functions.
	$boot->import(APP_PATH.'/__bootstrap/Bootstrap.php');

	// If there are no cookies, the storage file is cleared.
	if (LOAD_COOKIE_STORAGE == true) load('cookie')->storage()->controller();

	// Included composer component
	if (LOAD_COMPOSER == true) import(APP_PATH.'/__composer/vendor/autoload.php');

	// Init Access to system
	if (RUN_SQL == true) load('access')->init();

	// Attaches functions.
	import(APP_PATH.'/__funcs/functions.php');


	/** @package PROJECT */

	// Load custom project config.
	if (LOAD_CUSTOM_CONFIG == true) import(PROJECT_PATH.'/config.php');

	// Project functions
	import(LAYOUT_PATH.'/functions.php');
