<?php
/**
 * 
 * The class is used to support 
 * access to the site.
 * 
 * @package apflow
 * @subpackage Tools
 * @version 1.0
 * 
 */


    namespace apflow\Tools;
    use apflow\Bootstrap;
    use Exception;

    class Access extends Bootstrap{
        /**
         * 
         * 
         * 
         * @var array
         * 
         */
        private static $_userData = [];


        //------------------------------------------------------------------
        // ACCESS METHODS
        //------------------------------------------------------------------

        
        /**
         * 
         * 
         * 
         * @return void
         * 
         */
        public function init() : void{
            $current = date('Y-m-d H:i:s');
        }


        /**
         * 
         * 
         * 
         * @return bool
         * 
         */
        public function isLogged() : bool{
            return (bool)self::$_userData['logged'];
        }


        /**
         * 
         * 
         * 
         * @return bool
         * 
         */
        public function isLocked() : bool{
            return (bool)self::$_userData['locked'];
        }


        /**
         * 
         * 
         * 
         * @param array $data - 
         * @return bool
         * 
         */
        public function login(array $data) : bool{
            $sql    = $this->load('sql');
            $data   = null;

            self::$_userData = $data;
            return false;
        }


        /**
         * 
         * 
         * 
         * @param int $id_user      - 
         * @param string $token     -
         * 
         * @return bool
         * 
         */
        public function logout(int $id_user, string $token) : bool{
            $sql = $this->load('sql');
            return false;
        }


        /**
         * 
         * 
         * 
         * @return array
         * 
         */
        public function getActivateUser() : array{
            return self::$_userData;
        }
    }