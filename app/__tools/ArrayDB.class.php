<?php
/**
 * 
 * This file contains methods that imitate 
 * action on arrays, just like on a database.
 * 
 * @package apflow
 * @subpackage Tools
 * @version 1.0
 * 
 */


    namespace apflow\Tools;

    class ArrayDB{
        /**
         * 
         * 
         * 
         * @var string
         * 
         */
        private $_column        = null;

        /**
         * 
         * 
         * 
         * @var string
         * 
         */
        private $_value         = null;

        /**
         * 
         * 
         * 
         * @var array
         * 
         */
        private $_conditions    = [];

        /**
         * 
         * 
         * 
         * @var string
         * 
         */
        private $_orderColumn   = null;

        /**
         * 
         * 
         * 
         * @var array
         * 
         */
        private $_targetArray   = [];

        /**
         * 
         * 
         * 
         * @var bool
         * 
         */
        private $_where = false;


        //------------------------------------------------------------------
        // RESET METHOD
        //------------------------------------------------------------------


        /**
         * 
         * 
         * 
         * @return void
         * 
         */
        private function _reset(){
            $this->_column      = null;
            $this->_value       = null;
            $this->_conditions  = [];

            $this->_orderColumn = null;
            $this->_targetArray = [];

            $this->_where = false;
        }


        //------------------------------------------------------------------
        // OPERATORS WHERE METHODS
        //------------------------------------------------------------------

        /**
         * 
         * 
         * 
         * @param string $value - 
         * @return mixed
         * 
         */
        private function _getOperator(?string $value){
            if (strpos($value, '!=') !== false) return '!=';
            if (strpos($value, '>') !== false) return strpos($value, '>=') !== false ? '>=' : '>';
            if (strpos($value, '<') !== false) return strpos($value, '<=') !== false ? '<=' : '<';

            return false;
        }


        /**
         * 
         * 
         * 
         * @param string $value - 
         * @return bool
         * 
         */
        private function _whichOperator(?string $value) : bool{
            $operator = $this->_getOperator($value);
            return strpos($value, $operator) !== false ? true : false;
        }


        /**
         * 
         * 
         * 
         * @param array $source_array   - 
         * @param string $column        - 
         * @param string $value         -
         * 
         * @return array
         * 
         */
        private function _operatorsWhere(array $source_array, string $column, ?string $value) : array{
            $operator = $this->_getOperator($value);

            if ($operator !== false) {
                $value = $this->_resetValue($value);

                foreach ($source_array as $key => $cols) {
                    if (
                        ($cols[$column] == $value && $operator == '!=') ||
                        ($cols[$column] > $value && $operator == '<=') ||
                        ($cols[$column] < $value && $operator == '>=') ||
                        ($cols[$column] >= $value && $operator == '<') ||
                        ($cols[$column] <= $value && $operator == '>')
                    ) unset($source_array[$key]);
                }

                $source_array = array_values($source_array);
            }
            
            // Target array
            return $source_array;
        }


        //------------------------------------------------------------------
        // WHERE BUILD METHODS
        //------------------------------------------------------------------

        
        /**
         * 
         * 
         * 
         * @param string $value - 
         * @return string
         * 
         */
        private function _resetValue(?string $value) : string{
            $value = str_replace([ '!=', '<>' ], '', $value);
            return trim($value);
        }


        /**
         * 
         * 
         * 
         * @param array $source_array   - 
         * @param string $column        - 
         * @param string $value         - 
         * 
         * @return array
         * 
         */
        private function _traitBuildWhere(array $source_array, string $column, ?string $value) : array{
            $value = $this->_resetValue($value);

            $keys = array_keys(array_column($source_array, $column), $value);
            $source_array = array_map(function($k) use ($source_array){return $source_array[$k];}, $keys);

            // Target array
            return $source_array;
        }


        /**
         * 
         * 
         * 
         * @param array $source_array - 
         * @return array
         * 
         */
        private function _buildWhere(array $source_array) : array{
            // When in where is != or <>.
            if (is_null($this->_column)) {
                foreach ($this->_conditions as $column => $value) {
                    if ($this->_whichOperator($value)) $source_array = $this->_operatorsWhere($source_array, $column, $value);
                    else $source_array = $this->_traitBuildWhere($source_array, $column, $value);
                }
            } else {
                if ($this->_whichOperator($this->_value)) $source_array = $this->_operatorsWhere($source_array, $this->_column, $this->_value);
                else $source_array = $this->_traitBuildWhere($source_array, $this->_column, $this->_value);
            }

            // Reset vars
            $this->_reset();

            // Target array
            return $source_array;
        }

        /**
         * 
         * 
         * 
         * @param mixed $columns    - 
         * @param string $value     - 
         * 
         * @return array
         * 
         */
        public function where($columns = null, ?string $value = null){
            if (is_string($columns)) {
                $this->_column  = $columns;
                $this->_value   = $value;

                $this->_where   = true;
            }
            
            elseif (is_array($columns)) {
                $this->_conditions  = $columns;
                $this->_where       = true;
            }

            return $this;
        }


        //------------------------------------------------------------------
        // ORDER BY METHODS
        //------------------------------------------------------------------


        /**
         * 
         * 
         * 
         * @param array $a - 
         * @param array $b - 
         * 
         * @return array
         * 
         */
        private function _buildOrderBy(array $a, array $b) : array{
            if ($this->_ordering == 'asc') return $a[$this->_orderColumn] > $b[$this->_orderColumn];
            elseif ($this->_ordering == 'desc') return $a[$this->_orderColumn] < $b[$this->_orderColumn];
        }

        /**
         * 
         * 
         * 
         * @param string $column    - 
         * @param string $ordering  -
         * 
         * @return object
         * 
         */
        public function orderBy(string $column, ?string $ordering = null) : object{
            $this->_orderColumn = $column;
            $this->_ordering    = $ordering;

            return $this;
        }


        //------------------------------------------------------------------
        // GET METHODS
        //------------------------------------------------------------------


        /**
         * 
         * 
         * 
         * @param array $source_array       - 
         * @param int $limit                -
         * @param string $source_columns    -
         * 
         * @return mixed
         * 
         */
        public function get(array $source_array, ?int $limit, string $source_columns){
            // Array
            if ($this->_where === true) $source_array = $this->_buildWhere($source_array);
            $target_array   = [];
            
            // Columns
            $temp_columns   = [];
            $target_columns = [];

            // Ordering
            if (!is_null($this->_orderColumn)) usort($source_array, [$this, '_buildOrderBy']);

            // Searched records in the array.
            if (strpos($source_columns, ',') !== false || count($source_array) > 1) {
                $temp_columns = explode(',', $source_columns);
                foreach ($temp_columns as $key => $column) $target_columns[] = array_column($source_array, trim($column));

                foreach ($target_columns as $key => $columns) {
                    foreach ($columns as $_key => $value) $target_array[$_key][trim($temp_columns[$key])] = $value;
                }
            } else {
                $columns = array_column($source_array, $source_columns)[0] ?? null;
                if (!is_null($columns)) $target_array[$source_columns] = array_column($source_array, $source_columns)[0] ?? $columns;
            }

            // Limit
            if (!is_null($limit) && $limit > 0 && isset($target_array)) {
                $limit = count($source_array) < $limit ? count($source_array) : $limit;
                $target_array = array_slice($target_array, 0, $limit);
            }

            // Reset vars
            $this->_reset();

            // Target array
            return $target_array ?? false;
        }


        /**
         * 
         * 
         * 
         * @param array $source_array       -
         * @param string $source_columns    -
         * 
         * @return mixed
         * 
         */
        public function getOne(array $source_array, string $source_columns){
            // Array
            if ($this->_where === true) $source_array = $this->_buildWhere($source_array);
            $target_array   = [];
            
            // Columns
            $temp_columns   = [];
            $target_columns = [];

            // Ordering
            if (!is_null($this->_orderColumn)) usort($source_array, [$this, '_buildOrderBy']);

            // Searched records in the array.
            if (strpos($source_columns, ',') !== false) {
                $temp_columns = explode(',', $source_columns);
                foreach ($temp_columns as $key => $column) $target_columns[] = array_column($source_array, trim($column));

                foreach ($target_columns as $key => $columns) {
                    foreach ($columns as $_key => $value) $target_array[$_key][trim($temp_columns[$key])] = $value;
                }

                $target_array = $target_array[0] ?? null;
            } else {
                $columns = array_column($source_array, $source_columns)[0] ?? null;
                if (!is_null($columns)) $target_array[$source_columns] = array_column($source_array, $source_columns)[0] ?? $columns;
            }

            // Reset vars
            $this->_reset();

            // Target array
            return $target_array ?? false;
        }


        /**
         * 
         * 
         * 
         * @param array $source_array   -
         * @param string $column        -
         * 
         * @return mixed
         * 
         */
        public function getValue(array $source_array, string $column){
            if ($this->_where === true) $source_array = $this->_buildWhere($source_array);

            // Ordering
            if (!is_null($this->_orderColumn)) usort($source_array, [$this, '_buildOrderBy']);

            // Reset vars
            $this->_reset();

            // Target array
            $target_array = array_column($source_array, trim($column));
            return $target_array[0] ?? false;
        }
    }