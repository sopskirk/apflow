<?php
/**
 * Developed by Kevin Warren, https://twitter.com/KevinTWarren
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 * Detect Device 1.0.5
 *
 * Last Modification Date: 26/01/2019
 * 
 * @package apflow
 * @subpackage Tools
 * @version 1.0
 * 
 */


	namespace apflow\Tools;
	use Exception;

	class Detect {
		private $ipAddress = null;
		private $ipUrl = null;
		private $detect = null;

		public function init() {
			$this->detect = new Mobile_Detect();
			$this->detect->setDetectionType(Mobile_Detect::DETECTION_TYPE_EXTENDED);
			$this->getIp();
		}

		private function getIp() {
			if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) { $this->ipAddress = $_SERVER['HTTP_CLIENT_IP']; }
			elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { $this->ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR']; }
			else { $this->ipAddress = $_SERVER['REMOTE_ADDR']; }
			if (in_array($this->ipAddress, array('::1', '127.0.0.1', 'localhost'))) {
				$this->ipAddress = 'localhost';
				$this->ipUrl = '';
			} else {
				$this->ipUrl = '/' . $this->ipAddress;
			}
		}

		public function isMobile() {
			return $this->detect->isMobile();
		}

		public function isTablet() {
			return $this->detect->isTablet();
		}

		public function isPhone() {
			return ($this->detect->isMobile() ? ($this->detect->isTablet() ? false : true) : false);
		}

		public function isComputer() {
			return ($this->detect->isMobile() ? false : true);
		}

		public function deviceType() {
			return ($this->detect->isMobile() ? ($this->detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer');
		}

		public function version($var) {
			return $this->detect->version($var);
		}

		public function isEdge() {
			$agent = $_SERVER['HTTP_USER_AGENT'];
			if (preg_match('/Edge\/\d+/', $agent)) {
				return true;
			} else {
				return false;
			}
		}

		public function __call($name, $arguments) {
			if (substr($name, 0, 2) != 'is') {
				$trace = current(debug_backtrace());
				throw new Exception('No such method exists: '.$name);
			} else {
				return $this->detect->{$name}();
			}
		}

		public function brand() {
			$brand = 'Unknown Brand';
			switch ($this->deviceType()) {
			case 'Phone':
				foreach($this->detect->getPhoneDevices() as $name => $regex) {
					$check = $this->detect->{'is'.$name}();
					if ($check !== false) { $brand = $name; }
				}
				return $brand;
			case 'Tablet':
				foreach($this->detect->getTabletDevices() as $name => $regex) {
					$check = $this->detect->{'is'.$name}();
					if ($check !== false) { $brand = str_replace('Tablet', '', $name); }
				}
				return $brand;
				break;
			case 'Computer':
				return $brand;
				break;
			}
		}

		public function os() {
			$agent = $_SERVER['HTTP_USER_AGENT'];
			$version = '';
			$codeName = '';
			$os = 'Unknown OS';
			foreach($this->detect->getOperatingSystems() as $name => $regex) {
				$check = $this->detect->version($name);
				if ($check !== false) { $os = $name . ' ' . $check; }
				break;
			}
			if ($this->detect->isAndroidOS()) {
				if ($this->detect->version('Android') !== false) {
					$version = ' ' . $this->detect->version('Android');
					switch (true) {
			case $this->detect->version('Android') >= 9.0: $codeName = ' (Pie)'; break;
			case $this->detect->version('Android') >= 8.0: $codeName = ' (Oreo)'; break;
						case $this->detect->version('Android') >= 7.0: $codeName = ' (Nougat)'; break;
			case $this->detect->version('Android') >= 6.0: $codeName = ' (Marshmallow)'; break;
			case $this->detect->version('Android') >= 5.0: $codeName = ' (Lollipop)'; break;
						case $this->detect->version('Android') >= 4.4: $codeName = ' (KitKat)'; break;
						case $this->detect->version('Android') >= 4.1: $codeName = ' (Jelly Bean)'; break;
						case $this->detect->version('Android') >= 4.0: $codeName = ' (Ice Cream Sandwich)'; break;
						case $this->detect->version('Android') >= 3.0: $codeName = ' (Honeycomb)'; break;
						case $this->detect->version('Android') >= 2.3: $codeName = ' (Gingerbread)'; break;
						case $this->detect->version('Android') >= 2.2: $codeName = ' (Froyo)'; break;
						case $this->detect->version('Android') >= 2.0: $codeName = ' (Eclair)'; break;
						case $this->detect->version('Android') >= 1.6: $codeName = ' (Donut)'; break;
						case $this->detect->version('Android') >= 1.5: $codeName = ' (Cupcake)'; break;
						default: $codeName = ''; break;
					}
				}
				$os = 'Android' . $version . $codeName;
			} elseif (preg_match('/Linux/', $agent)) {
				$os = 'Linux';
			} elseif (preg_match('/Mac OS X/', $agent)) {
		if (preg_match('/Mac OS X 10_14/', $agent) || preg_match('/Mac OS X 10.14/', $agent)) {
					$os = 'OS X (Mojave)';
				} elseif (preg_match('/Mac OS X 10_13/', $agent) || preg_match('/Mac OS X 10.13/', $agent)) {
					$os = 'OS X (High Sierra)';
				} elseif (preg_match('/Mac OS X 10_12/', $agent) || preg_match('/Mac OS X 10.12/', $agent)) {
					$os = 'OS X (Sierra)';
				} elseif (preg_match('/Mac OS X 10_11/', $agent) || preg_match('/Mac OS X 10.11/', $agent)) {
					$os = 'OS X (El Capitan)';
				} elseif (preg_match('/Mac OS X 10_10/', $agent) || preg_match('/Mac OS X 10.10/', $agent)) {
					$os = 'OS X (Yosemite)';
				} elseif (preg_match('/Mac OS X 10_9/', $agent) || preg_match('/Mac OS X 10.9/', $agent)) {
					$os = 'OS X (Mavericks)';
				} elseif (preg_match('/Mac OS X 10_8/', $agent) || preg_match('/Mac OS X 10.8/', $agent)) {
					$os = 'OS X (Mountain Lion)';
				} elseif (preg_match('/Mac OS X 10_7/', $agent) || preg_match('/Mac OS X 10.7/', $agent)) {
					$os = 'Mac OS X (Lion)';
				} elseif (preg_match('/Mac OS X 10_6/', $agent) || preg_match('/Mac OS X 10.6/', $agent)) {
					$os = 'Mac OS X (Snow Leopard)';
				} elseif (preg_match('/Mac OS X 10_5/', $agent) || preg_match('/Mac OS X 10.5/', $agent)) {
					$os = 'Mac OS X (Leopard)';
				} elseif (preg_match('/Mac OS X 10_4/', $agent) || preg_match('/Mac OS X 10.4/', $agent)) {
					$os = 'Mac OS X (Tiger)';
				} elseif (preg_match('/Mac OS X 10_3/', $agent) || preg_match('/Mac OS X 10.3/', $agent)) {
					$os = 'Mac OS X (Panther)';
				} elseif (preg_match('/Mac OS X 10_2/', $agent) || preg_match('/Mac OS X 10.2/', $agent)) {
					$os = 'Mac OS X (Jaguar)';
				} elseif (preg_match('/Mac OS X 10_1/', $agent) || preg_match('/Mac OS X 10.1/', $agent)) {
					$os = 'Mac OS X (Puma)';
				} elseif (preg_match('/Mac OS X 10/', $agent)) {
					$os = 'Mac OS X (Cheetah)';
				}
			} elseif ($this->detect->isWindowsPhoneOS()) {
				if ($this->detect->version('WindowsPhone') !== false) {
					$version = ' ' . $this->detect->version('WindowsPhoneOS');
				}
				$os = 'Windows Phone' . $version;
			} elseif ($this->detect->version('Windows NT') !== false) {
				switch ($this->detect->version('Windows NT')) {
					case 10.0: $codeName = ' 10'; break;
					case 6.3: $codeName = ' 8.1'; break;
					case 6.2: $codeName = ' 8'; break;
					case 6.1: $codeName = ' 7'; break;
					case 6.0: $codeName = ' Vista'; break;
					case 5.2: $codeName = ' Server 2003; Windows XP x64 Edition'; break;
					case 5.1: $codeName = ' XP'; break;
					case 5.01: $codeName = ' 2000, Service Pack 1 (SP1)'; break;
					case 5.0: $codeName = ' 2000'; break;
					case 4.0: $codeName = ' NT 4.0'; break;
					default: $codeName = ' NT v' . $this->detect->version('Windows NT'); break;
				}
				$os = 'Windows' . $codeName;
			} elseif ($this->detect->isiOS()) {
				if ($this->detect->isTablet()) {
					$version = ' ' . $this->detect->version('iPad');
				} else {
					$version = ' ' . $this->detect->version('iPhone');
				}
				$os = 'iOS' . $version;
			}
			return $os;

		}

		public function browser() {
			$agent = $_SERVER['HTTP_USER_AGENT'];
			$browser = 'Unknown Browser';
			if (preg_match('/Edge\/\d+/', $agent)) {
				$browser = 'Microsoft Edge ' . str_replace('12', '20', $this->detect->version('Edge'));
			} elseif ($this->detect->version('Trident') !== false && preg_match('/rv:11.0/', $agent)) {
				$browser = 'Internet Explorer 11';
			} else {
				$found = false;
				foreach($this->detect->getBrowsers() as $name => $regex) {
					$check = $this->detect->version($name);
					if ($check !== false && !$found) {
						$browser = $name . ' ' . $check;
						$found = true;
					}
				}
			}
			return $browser;
		}

		public function isIE() {
			return $this->detect->version('IE') !== false ? true : false;
		}

		public function ip() {
			$ip = '';
			
			if (getenv('HTTP_CLIENT_IP'))
				$ip = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
				$ip = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
				$ip = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
				$ip = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
				$ip = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
				$ip = getenv('REMOTE_ADDR');
			else
				$ip = 'UNKNOWN';
			
			return $ip == '::1' ? '127.0.0.1' : $ip;
		}

		/**
         * 
         * Gets the guest lang browser.
         * 
         * @param bool $full - Full code language
         * @return string
         * 
         */
        public function lang(bool $full = false) : string{
            $server = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            $server = substr($server, 0, strpos($server, ','));

            // Extracts the language short code.
            $lang = !$full ? substr($server, 0, 2) : $server;
            return $lang;
        }

		private $phoneModels = [
			'Samsung' => [
				'GT-I7500' => 'Samsung Galaxy',
				'GT-I5700' => 'Samsung Galaxy Spica',
				'GT-I9000' => 'Samsung Galaxy S',
				'GT-S5830' => 'Samsung Galaxy Ace',
				'GT-S5830I' => 'Samsung Galaxy Ace',
				'GT-S5670' => 'Samsung Galaxy Fit',
				'GT-I9003' => 'Samsung Galaxy SL',
				'GT-S5660' => 'Samsung Galaxy Gio',
				'GT-S5570' => 'Samsung Galaxy Mini',
				'SPH-M820' => 'Samsung Galaxy Prevail',
				'GT-I9100' => 'Samsung Galaxy S II',
				'SGH-T759' => 'Samsung Exhibit 4G',
				'GT-I9001' => 'Samsung Galaxy S Plus',
				'GT-I9103' => 'Samsung Galaxy R',
				'GT-I8150' => 'Samsung Galaxy W',
				'SGH-T679' => 'Samsung Exhibit II 4G',
				'GT-S5360' => 'Samsung Galaxy Y',
				'GT-S5690' => 'Samsung Galaxy Xcover',
				'GT-I9250' => 'Samsung Galaxy Nexus',
				'GT-B5510' => 'Samsung Galaxy Y Pro Duos',
				'GT-B5512' => 'Samsung Galaxy Y Pro Duos',
				'GT-S7500' => 'Samsung Galaxy Ace Plus',
				'GT-I8160' => 'Samsung Galaxy Ace 2',
				'GT-S7560M' => 'Samsung Galaxy Ace 2 x',
				'GT-S6500' => 'Samsung Galaxy Mini 2',
				'GT-S6102' => 'Samsung Galaxy Y DUOS',
				'GT-I8520' => 'Samsung Galaxy Beam',
				'SGH-i847' => 'Samsung Galaxy Rugby Smart',
				'GT-S5300' => 'Samsung Galaxy Pocket',
				'GT-S5690M' => 'Samsung Galaxy Rugby',
				'GT-I9300' => 'Samsung Galaxy S III',
				'GT-I9305' => 'Samsung Galaxy S III LTE',
				'SGH-I827' => 'Samsung Galaxy Appeal',
				'GT-B5330' => 'Samsung Galaxy Ch@t',
				'SCH-I200' => 'Samsung Galaxy Stellar',
				'GT-S7562' => 'Samsung Galaxy S Duos',
				'GT-S7568' => 'Samsung Galaxy S Duos',
				'TD-SCDMA' => 'Samsung Galaxy S Duos',
				'GT-S7560M' => 'Samsung Galaxy Trend',
				'GT-S7572' => 'Samsung Galaxy Trend II Duos',
				'GT-S5302' => 'Samsung Galaxy Pocket Duos',
				'SPH-L300' => 'Samsung Galaxy Victory 4G LTE',
				'GT-N7100' => 'Samsung Galaxy Note II (3G)',
				'GT-N7102' => 'Samsung Galaxy Note II (Dual-SIM)',
				'GT-N7105' => 'Samsung Galaxy Note II (4G)',
				'SGH-I437' => 'Samsung Galaxy Express',
				'SGH-I547' => 'Samsung Galaxy Rugby Pro',
				'SGH-I547C' => 'Samsung Galaxy Rugby LTE',
				'GT-I8190' => 'Samsung Galaxy S III Mini'
			]
		];
	}