<?php
/**
 *
 * Database configuration
 *
 * @package apflow
 * @subpackage Config
 * @version 1.0
 *
 */

    return [
        /**
         * 
         * Default database connection.
         * 
         */
        'default' => [
            /**
             * 
             * Accepts the host name on which 
             * the database resides.
             * 
             */
            'HOST'      => 'sql.serwer1998858.devveb.pl',


            /**
             * 
             * The name of the database on which the query
             * structure is based.
             * 
             */
            'DBNAME'    => '31118536_pai',


            /**
             * 
             * Database login information.
             * 
             */
            'USER'      => '31118536_pai',
            'PASS'      => 'olcw5lQR',


            /**
             * 
             * Database port access.
             * 
             */
            'PORT'      => 3306,


            /**
             * 
             * Tables prefix
             * 
             */
            'PREFIX'    => 'tpai_',

            
            /**
             * 
             * Table charset
             * 
             */
            'CHARSET'   => 'utf8'
        ],


        /**
         * 
         * Others data to connect 
         * to the database.
         * 
         */

         // 'connect_name' => [
         //     ...
         // ],
    ];