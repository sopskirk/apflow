<?php
/**
 *
 * Settings for connecting to the server email.
 *
 * @package apflow
 * @subpackage Config
 * @version 1.0
 *
 */


    return [
        /**
         * 
         * TCP port to connect to.
         * 
         */
        'PORT'      => '',


        /**
         * 
         * Specify main and backup SMTP servers.
         * 
         */
        'HOST'      => '',


        /**
         * 
         * SMTP username
         * 
         */
        'USER'      => '',


        /**
         * 
         * SMTP password
         * 
         */
        'PASS'      => '',


        /**
         * 
         * Sender email
         * 
         */
        'SENDER'    => '',


        /**
         * 
         * Recipient email
         * 
         */
        'RECIPIENT' => '',


        /**
         * 
         * Signature of the admin.
         * 
         */
        'SIGNATURE' => '',

        /**
         * 
         * Default Template
         * 
         */
        'DEFAULT_TEMPLATE' => 'mailer_example'
    ];