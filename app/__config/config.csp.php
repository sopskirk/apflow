<?php
/**
 *
 * Settings for CSP and HSTS.
 *
 * @package apflow
 * @subpackage Config
 * @version 1.0
 *
 */


    return [
        /**
         * 
         * Generate a hash of the provided $string value, 
         * and have it added to the $friendlyDirective 
         * directive in CSP.
         * 
         */
        'CSPHash' => 'X9jvuevtEd90vUhqFAGJR2oimejjFUthl54vpicY',

        /**
         * 
         * Protected cookies.
         * 
         */
        'ProtectedCOOKIE' => [],

        /**
         * 
         * Add and configure the HTTP
         * Public Key Pins header.
         * 
         */
        'HPKP'  => [
            /**
             * 
             * The above will add pin1, pin2, and pin3 with the associated 
             * hash label sha256. This is the only valid * HPKP 
             * hashing algorithm at time of writing.
             * 
             */
            'PINS'          => [
                ['sha256', 'pin1'],
                ['pin2'],
                ['pin3', 'sha256']
            ],

            /**
             * 
             * The length, in seconds that a browser should 
             * enforce the policy after last receiving it.
             * 
             */
            'MAXAGE'        => 1500,

            /**
             * 
             * Loosely casted to a boolean, whether to include 
             * the includeSubDomains flag to deploy the policy 
             * across the entire domain. true enables this flag.
             * 
             */
            'SUBDOMAINS'     => true,

            /**
             * 
             * A reporting address to send violation reports to.
             * 
             */
            'REPORT-URI'    => '',

            /**
             * 
             * Loosely cased to a boolean.
             * 
             */
            'REPORT-ONLY'   => false
        ],


        /**
         * 
         * The exact interpretation of csp is type dependent.
         * 
         */
        'CSP' => [
            'default-src'			=> 'none',
            'script-src' 			=> ['self', 'https://stackpath.bootstrapcdn.com/bootstrap/', 'https://ajax.googleapis.com/ajax/libs/webfont/', 'https://cdnjs.cloudflare.com/ajax/libs/'],
            'style-src'				=> ['self', 'unsafe-inline', 'https://fonts.googleapis.com/', 'https://cdnjs.cloudflare.com/', 'https://cdn.materialdesignicons.com/3.0.39/', 'https://use.fontawesome.com/', 'https://cdn.jsdelivr.net/npm/'],
            'font-src'				=> ['self', 'https://fonts.googleapis.com/', 'https://fonts.gstatic.com/', 'https://cdn.materialdesignicons.com/3.0.39/', 'https://use.fontawesome.com/'],
            'img-src' 				=> ['self', 'data:'],
            'connect-src'			=> ['self', 'https://www.google-analytics.com/'],
            'media-src'				=> 'self',
            'child-src'				=> 'self',
            'frame-src'				=> 'self',
            'base-uri'				=> 'self',
            'frame-ancestors'		=> 'self',
            'object-src'			=> 'none',
            'manifest-src'			=> 'self',
            'report-uri'			=> '',
            'block-all-mixed-content' => null
        ]
    ];