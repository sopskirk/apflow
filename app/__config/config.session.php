<?php
/**
 *
 * Settings for creating session.
 *
 * @package apflow
 * @subpackage Config
 * @version 1.0
 *
 */

    $secure = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? true : false;

	return [
		/**
         * 
         * Lifetime of the session cookie, defined
         * in seconds (recommended by 0).
         * 
         */
		'SESSION_LIFETIME'	=> 0,


		/**
         * 
         * The path on the server in which the
         * cookie will be available on.
         * 
         */
		'SESSION_PATH' 		=> '/',


		/**
         * 
         * The (sub)domain that the cookie
         * is available to (recommended).
         * 
         */
		'SESSION_DOMAIN'	=> $_SERVER['SERVER_NAME'],


		/**
         * 
         * Indicates that the cookie should only be transmitted
         * over a secure HTTPS connection from the client.
         * 
         */
		'SESSION_SECURE'	=> $secure,


		/**
         * 
         * When TRUE the cookie will be made accessible
         * only through the HTTP protocol.
         * 
         */
        'SESSION_HTTPONLY'	=> true,
        

        /**
         * 
         * Sets the name 'session_name'.
         * 
         */
		'SESSION_NAME'		=> 'SSID',


		/**
         * 
         * Additional group of the session being created.
         * 
         */
        'SESSION_GROUP'	    => 'apflow',
        

        /**
         * 
         * The default expire of the session.
         * 
         */
        'SESSION_EXPIRE'    => '15 minutes',


        //------------------------------------------------------------------
        // FLASH CONFIG
        //------------------------------------------------------------------


        /**
         * 
         * Additional group of the flash session being created.
         * 
         */
        'SESSION_FLASH_GROUP'   => 'flash',


        /**
         * 
         * The default expire of the flash session.
         * 
         */
        'SESSION_FLASH_EXPIRE'  => '1 seconds'
    ];
