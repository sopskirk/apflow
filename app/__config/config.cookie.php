<?php
/**
 *
 * Settings for creating cookies.
 *
 * @package apflow
 * @subpackage Config
 * @version 1.0
 *
 */

    $secure = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? true : false;

    return [
        /**
         *
         * The path on the server in which the
         * cookie will be available on.
         *
         */
        'PATH' 		=> '/',


        /**
         *
         * The (sub)domain that the cookie is
         * available to (recommended).
         *
         */
        'DOMAIN'	=> $_SERVER['SERVER_NAME'],


        /**
         *
         * Indicates that the cookie should only be transmitted
         * over a secure HTTPS connection from the client.
         *
         */
        'SECURE'	=> $secure,

        /**
         *
         * When TRUE the cookie will be made accessible
         * only through the HTTP protocol.
         *
         */
        'HTTPONLY'	=> true
    ];
