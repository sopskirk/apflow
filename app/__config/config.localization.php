<?php
/**
 *
 * Settings for Localization.
 *
 * @package apflow
 * @subpackage Config
 *
 */


    return [
        /**
         * 
         * The language in which the page will be tested. 
         * If it is false, the tests will be turned off.
         * 
         */
        'TEST_LANGUAGE' => false,


        /**
         * 
         * The default language in which the page 
         * should be displayed in case the user's 
         * language is not declared.
         * 
         */
        'DEFAULT_LANGUAGE' => 'pl',


        /**
         * 
         * Set the languages ​​in which 
         * the page will be available.
         * 
         */
        'LANGUAGES' => [ 'pl', 'en', 'de' ]
    ];