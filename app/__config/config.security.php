<?php
/**
 *
 * Settings for valid pattern.
 *
 * @package apflow
 * @subpackage Config
 * @version 1.0
 *
 */


    return [
        /**
         * 
         * Patterns for the preg_match function.
         * 
         */
        'PATTERNS' => [
            'text'  	=> '/^[a-z0-9ąążśźęćńółĄŻŚŹĘĆŃÓŁ\W\s]+$/i',
            'http'      => '/^[\w\d \.]+$/i',
            'cookie'    => '/^[a-z ]+$/i'
        ],


        /**
         * 
         * Configuration for encoding 
         * user password.
         * 
         */
        'PASSWORD' => [
            /**
             * 
             * Password encoding algorithm
             * 
             */
            'ALGORITHM' => PASSWORD_BCRYPT,


            /**
             * 
             * The cost of the algorithm
             * 
             */
            'COST' => 11,


            /**
             * 
             * SALT for user password
             * 
             */
            'SALT' => '5d8f95dba0767ec67c26f93938e16026abbe0990f23c5b419da51fd4f1'
        ]
    ];