<?php
/**
 * 
 * Example Template for PHPMailer.
 * 
 * @package apflow
 * @subpackage Templates
 * @version 1.0
 * 
 */


    function mailer_example(array $data = []) : array{
        $content = $data['message'];

        $message = '
            <div style="padding:40px;background:#fafafa;border:1px solid #e5e5e5;border-radius:10px;width:fit-content;margin:auto">
                <p style="margin:0 0 10px;padding:0"><b>Zleceniodawca</b>: '.$content['name'].'</p>
                <p style="margin:0 0 10px;padding:0"><b>Czas realizacji projektu</b>: '.$content['timeframe'].'</p>
                <p style="margin:0 0 10px;padding:0"><b>Budżet</b>: '.$content['budget'].'</p>
                <p style="margin:0 0 10px;padding:0 0 10px;border-bottom:1px solid #e5e5e5"><b>Email zleceniodawcy</b>: '.$content['email'].'</p>
                <p style="margin:0;padding:0"><b>Wiadomość</b>: '.$content['message'].'</p>
                <p style="margin: 15px 0 0;padding:0;max-width:400px"><span style="color:#999;font-style:italic;font-size:11px">Ta wiadomość została wygenerowana automatycznie i proszę na nią nie odpowiadać. Wiadomość jest potwierdzeniem otrzymania zlecenia. Wkrótce się z Państwem skontaktuję.</span></p>
            </div>'
        ;

        return [
            'subject' => $content['title'],
            'message' => $message
        ];
    }