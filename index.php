<?php
/**
 * 
 * Project init
 * 
 * @package apflow
 * @subpackage Project
 * @version 1.0
 * 
 */


	// Thoroughly checks the type of function argument.
 	declare(strict_types = 1);

	
	/** @package BASIC PATHES **/
	define('DS', DIRECTORY_SEPARATOR);
	define('ROOT_PATH', dirname(__FILE__));
	define('APP_PATH', ROOT_PATH.DS.'app');
	define('PROJECT_PATH', ROOT_PATH.DS.'project');
	define('LAYOUT_PATH', PROJECT_PATH.DS.'layout');
	

	/** @package MVC PATHES **/
	define('MVC_PATH', LAYOUT_PATH.DS.'MVC');
	define('MVC_VIEWS_PATH', MVC_PATH.DS.'views');
	define('MVC_MODELS_PATH', MVC_PATH.DS.'models');
	define('MVC_AJAX_PATH', MVC_PATH.DS.'ajax');


	/** @package SQL **/
	define('RUN_SQL', true);


	/** @package LOGIN/REGISTRY **/
	define('LOGIN_EXPIRE', '7 hours');


	/** @package AJAX **/
	define('AJAX_PERMITTED_METHOD', 'POST');


	/** @package LOADERS **/
	define('LOAD_CUSTOM_CONFIG', true);
	define('LOAD_COOKIE_STORAGE', false);
	define('LOAD_COMPOSER', true);
	define('LOAD_CSP', false);

	
	/** @package ERRORS **/

	/**
	 * 
	 * Sets the appropriate version of error reports.
	 * Versions to choose from:
	 * 
	 * [private] - All error information is displayed
	 * in the report. In addition, the private
	 * version is the default version (test phase),
	 * 
	 * [public] - Displays only the Unique Report 
	 * Identifier (final phase).
	 * 
	 * #!Each error goes to the reports_[date].log file
	 * irrelevant to the report display versions!#
	 * 
	 */
	define('ERROR_VISIBLE', 'private');

	// Do you want to see errors regarding 
	// the resource you are looking for?
	define('DISPLAY_HAS_ERROR', false);

	// Whether errors should be displayed as a bool type?
	define('DISPLAY_AS_BOOL', false);


	/** @package SLACK WEBHOOKS **/

	// WEBHOOK RUN
	define('SLACK_WEBHOOK_RUN', false);

	// WEBHOOKS URL
	define('SLACK_WEBHOOK_URL', '');

	// WEBHOOK DEFAULT TEMPLATE
	define('SLACK_WEBHOOK_TEMPLATE', 'slack_example');

	// Load init
	@ob_start();
	require 'init.php';