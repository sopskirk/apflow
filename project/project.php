<?php
/**
 * 
 * In this file you can put references 
 * to all of your page files.
 * 
 * @package apflow
 * @subpackage Project
 * 
 */


    $controller = load('controller');
    
    // Init controller to the project.
    $controller->init();

    // HTTP response code 200
    if ($controller->viewFound()) {
        // Included header theme
        $controller->header();

        // Included views
        $controller->loadViews(false, false);

        // Included footer theme
        $controller->footer();
    } 
    
    // HTTP response code 404
    else $controller->view404();