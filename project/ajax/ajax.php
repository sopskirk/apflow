<?php
/**
 * 
 * In this file you can put references 
 * to all of your page files.
 * 
 * @package apflow
 * @subpackage Project - AJAX
 * 
 */


	if (defined('AJAX')) {
		$request = load('request');

		if ($request->isMethod() == AJAX_PERMITTED_METHOD || AJAX_PERMITTED_METHOD == false) {
			$token = _REQUEST('token');

			// Tokens of array
			$tokens = [
				'21c0d331afab3e14540c22b256' => 'CheckAnswer',
				'769184c78e486b0bf5b1188427' => 'GetQuestion',
				'349184c78e43s1af5b11884212' => 'Test'
			];

			// Good token!
			if (isset($tokens[$token])) import(MVC_AJAX_PATH.DS.$tokens[$token].'.php');

			// Wrong token!
			else $request->ajax()->response('Invalid token! Access denied!', 403);
		}

		// Wrong Request Method!
		else $request->ajax()->response('Method not allowed! Access denied!', 403);
	}

	// Wrong Request!
	else {
		ob_start();
		header('Location:/');
		exit;
	}