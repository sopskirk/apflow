<?php
/**
 * 
 * Test
 * 
 * @package apflow
 * @subpackage Project - Ajax
 * 
 */

    function ajaxTest() {
        $sql = load('sql');
        $test = $sql->db->getOne('Test', 'name, content');

        return [
            'status'    => true,
            'data'      => $test,
            'token'     => '349184c78e43s1af5b11884212'
        ];
    }

    load('request')->ajax()->response(ajaxTest());