<?php
/**
 * 
 * This file sends the drawn question by AJAX.
 * 
 * @package apflow
 * @subpackage Project - Ajax
 * 
 */

    function ajaxGetQuestion() {
        $security = load('security');

        $id_alert = load('randval')->generate(16);
        $question_number = $security->filter($_REQUEST['question_number']);
        $method = $security->filter($_SERVER['REQUEST_METHOD']);

        try {
            if ($method == 'POST') {
                return [
                    'token'     => '769184c78e486b0bf5b1188427',
                    'status'    => true,
                    'data'      => getQuestion($question_number),
                    'qn'        => $question_number
                ];
            } else {
                load('api')->Slack([
                    'attachments' => [
                        'author_name'   => 'Serwer',
                        'title'         => 'Zgłoszono Alert!',
                        'text'          => 'Dane nie zostały wysłane za pomocą metody POST, ale za pomocą metody '.$method.'.'
                    ]
                ]);

                return [ 'status' => false ];
            }
        } catch(Throwable $t){
            $error = report($t, null, null, null, null, $id_alert);

            return [
                'status'        => false,
                'isErrorServer' => true,
                'ID_Alert'      => $error->errid ?? $id_alert
            ];
        }
    }

    load('request')->ajax()->response(ajaxGetQuestion());