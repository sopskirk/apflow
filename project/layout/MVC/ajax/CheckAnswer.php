<?php
/**
 * 
 * Tthis file checks the response sent by AJAX 
 * and sends back a specific status.
 * 
 * @package apflow
 * @subpackage Project - Ajax
 * 
 */

    function ajaxCheckAnswer() {
        $security = load('security');

        $id_alert       = load('randval')->generate(16);
        $method         = $security->filter($_SERVER['REQUEST_METHOD']);
        $id_question    = $security->filter($_REQUEST['id_question']);
        
        try {
            if ($method == 'POST') {
                $sql = load('sql');

                $question_number = $security->filter($_POST['question_number']) - 1;
                $answer = $security->filter($_POST['answer']);

                $good_answer = $sql->db->where('id_question', $id_question)->getValue('questions_answers', 'good_answer');
                $good_answer = mb_convert_case($security->filter($good_answer), MB_CASE_LOWER);

                $status = mb_strpos($good_answer, mb_convert_case($answer, MB_CASE_LOWER)) !== false ? 1 : 0;

                // Update `questions_answers` table.
                $params = [ 'where' => [ 'id_question' => $id_question ] ];
                $update = $status == 1 ? [ 'good' => '+1' ] : [ 'bad' => '+1' ];

                $sql->db->where('id_question', $id_question)->update('questions_answers', $update);

                return [
                    'status'    => true,
                    'data'      => [
                        'status' => $status
                    ]
                ];
            } else {
                load('api')->Slack([
                    'attachments' => [
                        'author_name'   => 'Serwer',
                        'title'         => 'Zgłoszono Alert!',
                        'text'          => 'Dane nie zostały wysłane za pomocą metody POST, ale za pomocą metody '.$method.'.'
                    ]
                ]);

                return [ 'status' => false ];
            }
        } catch(Throwable $t){
            $error = report($t, null, null, null, null, $id_alert);
            
            return [
                'status'        => false,
                'isErrorServer' => true,
                'ID_Alert'      => $error->errid ?? $id_alert
            ];
        }
    }

    load('request')->ajax()->response(ajaxCheckAnswer());