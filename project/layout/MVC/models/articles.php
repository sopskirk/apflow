<?php
/**
 *
 * Model definition for Articles module.
 *
 * @package MVC
 * @subpackage Articles
 *
 */


    use apflow\Components\Controller;

    class Articles extends Controller{
        public $getTestData = [];

        public function lazyLoadingSQL($methods){
            return $this->useLazyLoadingSQL($methods, $this);
        }

        protected function getTestData(){
            $db = $this->load('sql')->db;
            return $db->getOne('Test', 'name, content');
        }
    }