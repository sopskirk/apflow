<?php
/**
 * 
 * The template for displaying the footer.
 *
 * @package apflow
 * @subpackage Project
 * 
 */

    global $nonce;

    load_plugins([
        'jquery-3.4.1.min.js',
        'sweetalert2/sweetalert2.min.js',
        'sweetalert2/init.js'
    ], $nonce);
    
    load_js([
        'index.min.js',
        'https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js'
    ], $nonce);

?>

    </body>
</html>