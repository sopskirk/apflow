<?php
/**
 *
 * This template for displaying the header,
 *
 * @package apflow
 * @subpackage Project
 *
 */


	global $nonce;
	//$nonce = load('csp')->nonce();
	$nonce = 'nonce';

	$controller = load('controller');

?>
<!doctype html>
<html lang="<?=load('detect')->lang(true);?>">
    <head>
		<title><?=$controller->title();?></title>

		<!-- METS -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- FONTS -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" nonce="<?=$nonce?>"></script>
    	<script nonce="<?=$nonce?>">
			WebFont.load({
				google : {
					'families' : [ 'Poppins: 300,400,500,600,700', 'Roboto:300,400,500,700' ]
				},
				
				active : function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!-- # -->

		<!-- ASSETS -->
		<?php
			load_css([
				'https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css',
				'style.min.css'
			]);

			load_plugins([
				'animate.min.css',
				'sweetalert2/sweetalert2.css',
			]);
		?>
		<!-- # -->
	</head>
	<body>